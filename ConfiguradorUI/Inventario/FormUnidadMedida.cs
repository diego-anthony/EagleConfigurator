﻿using ConfigBusinessEntity;
using ConfigBusinessLogic;
using ConfiguradorUI.Buscadores;
using ConfiguradorUI.FormUtil;
using ConfigUtilitarios;
using ConfigUtilitarios.HelperControl;
using MetroFramework.Controls;
using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace ConfiguradorUI.Inventario
{
    public partial class FormUnidadMedida : MetroForm
    {
        #region Variables
        private decimal _factorUniMed;
        private bool _isSelected = false;
        private bool _isChangedRow = false;
        private bool _isPending = false;
        private bool _preguntar = true;
        private int _tipoOperacion = TipoOperacionABM.No_Action;
        private string _codSelected;
        private ulong _idUnidad;
        private bool _ignoreReloadCbo;
        #endregion

        public FormUnidadMedida()
        {
            InitializeComponent();
        }

        #region Métodos de ventana
        private void addHandlers()
        {
            //Agregando Handlers que se disparan al cambiar el contenido, estado o selección
            var txts = new[] { txtNombre, txtCantidadBase, txtFactor, txtCodigo, txtAbrev };
            foreach (var txt in txts)
            {
                txt.TextChanged += OnContentChanged;
            }

            var cbos = new[] { cboUnidad,cboUndMedidaSNT };
            foreach (var cbo in cbos)
            {
                cbo.SelectedIndexChanged += OnContentChanged;
                cbo.IntegralHeight = false;
                cbo.MaxDropDownItems = ControlHelper.maxDropDownItems;
                cbo.DropDownWidth = ControlHelper.DropDownWidth(cbo);
            }

            var chks = new[] { chkActivo, chkUMCompra, chkUMVenta};

            foreach (var chk in chks)
            {
                chk.CheckedChanged += OnContentChanged;
            }

            txtFactor.KeyPress += ValidarTxtDecimal;
        }

        protected void OnContentChanged(object sender, EventArgs e)
        {
            if (_isSelected && _isChangedRow == false && _tipoOperacion != TipoOperacionABM.Cambio)
            {
                _tipoOperacion = TipoOperacionABM.Cambio;
                ControlarEventosABM();
            }
        }
        private void CambioEnControl(object sender, EventArgs e)
        {
            _isChangedRow = false;
        }
        private void ValidarTxtDecimal(object sender, KeyPressEventArgs e)
        {
            MetroTextBox txt = (MetroTextBox)sender;
            if (e.KeyChar >= '0' && e.KeyChar <= '9' || char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                if (e.KeyChar == '.')
                {
                    e.Handled = txt.Text.Contains(".") || txt.Text.Equals("");
                }
                else
                {
                    e.Handled = true;
                }
            }
        }

        private void Commit()
        {
            try
            {
                if (_tipoOperacion == TipoOperacionABM.Insertar)
                {
                    if (esValido())
                    {
                        var model = GetObject();
                        new ProdUnidadMedidaBL().Add(model);
                        ControlarEventosABM(model.id_um);
                        _ignoreReloadCbo = true;
                        _isSelected = false;
                        CargarCombos();
                        if (model.id_um_base > 0)
                        {
                            cboUnidad.SelectedValue = model.id_um_base;
                        }
                        _ignoreReloadCbo = false;
                    }
                }
                else
                {
                    Actualizar();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(this, "Ocurrió un error en commit. " + e.Message, "MENSAJE", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        private void Eliminar()
        {
            if (_tipoOperacion == TipoOperacionABM.Eliminar)
            {
                if (dgvUnidadMedida.RowCount > 0)
                {
                    if (dgvUnidadMedida.SelectedRows.Count > 0)
                    {
                        try
                        {
                            if (ulong.TryParse(lblIdUnidad.Text, out ulong id) && id > 0)
                            {
                                DialogResult rp = MessageBox.Show("¿Seguro de eliminar?", "CONFIRMACIÓN", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                if (rp == DialogResult.Yes)
                                {
                                    // Evalúa la cantidad de hijos de unidad de medida. 0 => no tiene hijo y puede eliminarse, -1   
                                    int UndsBase = new ProdUnidadMedidaBL().FindByUndbase(id);
                                    if (UndsBase == 0)
                                    {
                                        new ProdUnidadMedidaBL().Eliminar(id);
                                        ControlarEventosABM();
                                        _isSelected = false;
                                        CargarCombos();
                                        _isSelected = true;
                                    }
                                    else if (UndsBase > 0)
                                    {
                                        _tipoOperacion = TipoOperacionABM.No_Action;
                                        ControlarEventosABM();
                                        Msg.Ok_Info("Este registro no se puede eliminar porque se usa en otro lado.", "MENSAJE EAGLE");
                                    }
                                    else
                                    {
                                        _tipoOperacion = TipoOperacionABM.No_Action;
                                        ControlarEventosABM();
                                        Msg.Ok_Info("No se pudo eliminar el registro", "MENSAJE EAGLE");
                                    }
                                }
                            }
                            else
                            {
                                Msg.Ok_Wng("El ID de la unidad de medida es incorrecto.", "MENSAJE");
                            }
                        }
                        catch (Exception e)
                        {
                            MessageBox.Show(this, "Ocurrió una excepción en el intento de eliminación: " + e.Message, "MENSAJE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show(this, "No se ha seleccinado un producto.", "MENSAJE", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    MessageBox.Show(this, "No hay registros", "MENSAJE", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }

        }
        private bool Actualizar()
        {
            bool isValid = false;
            try
            {
                if (_tipoOperacion == TipoOperacionABM.Modificar && _isSelected && _isPending)
                {
                    if (esValido())
                    {
                        var model = new INVt10_unidad_medida();
                        model = GetObject();
                        if (ulong.TryParse(lblIdUnidad.Text, out ulong id))
                        {
                            model.id_um = (long)id;
                            bool success = new ProdUnidadMedidaBL().Edit(model);
                            ControlarEventosABM(model.id_um);
                            if (success)
                            {
                                _isSelected = false;
                                //CargarCombos();
                                _isSelected = true;
                            }
                            else
                            {
                                MessageBox.Show("No se pudo actualizar el producto.", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        isValid = true;
                    }
                    else { isValid = false; }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Ocurrió una excepción en Actualizar Producto: " + e.Message);
            }
            return isValid;
        }
        private bool ActualizarProductoEnCheck()
        {
            bool isValid = false;
            try
            {
                if (_tipoOperacion == TipoOperacionABM.Modificar && _isSelected && _isPending)
                {
                    if (esValido())
                    {
                        var model = new INVt10_unidad_medida();
                        model = GetObject();
                        if (ulong.TryParse(lblIdUnidad.Text, out ulong id))
                        {
                            // Quitar el cas cuando se corriga el modelo a long
                            model.id_um = (long) id;
                            bool success = new ProdUnidadMedidaBL().Edit(model);
                            if (!success)
                            {
                                MessageBox.Show("No se pudo actualizar el producto.", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            else
                            {
                                _isSelected = false;
                                CargarCombos();
                                _isSelected = true;
                            }
                        }
                        isValid = true;
                    }
                    else { isValid = false; }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Ocurrió una excepción en Actualizar Producto en Check: " + e.Message);
            }
            return isValid;
        }

        private INVt10_unidad_medida GetObject()
        {
            var model = new INVt10_unidad_medida();
            try
            {
                model.txt_desc = txtNombre.Text.Trim();
                model.id_estado = chkActivo.Checked ? Estado.IdActivo : Estado.IdInactivo;
                model.txt_estado = chkActivo.Checked ? Estado.TxtActivo : Estado.TxtInactivo;
                model.cod_um = txtCodigo.Text.Trim();
                model.txt_abrv = txtAbrev.Text.Trim();
                model.txt_unid_base = txtUnidadMedidaBase.Text.Trim();
                model.sn_compra = chkUMCompra.Checked;
                model.sn_venta = chkUMVenta.Checked;

                if (decimal.TryParse(txtCantidadBase.Text, out decimal qty_base))
                    model.qty_base = qty_base;
                else
                    model.qty_base = 1;

                if (decimal.TryParse(txtFactor.Text, out decimal factor))
                    model.dec_factor = factor;
                else
                    model.dec_factor = 1;
                if (cboUnidad.SelectedValue != null)
                {
                    ulong idBase = ulong.Parse(cboUnidad.SelectedValue.ToString());
                    if (new ProdUnidadMedidaBL().Get(idBase).cod_um != TopList.UnidadMedida)
                    {
                        model.id_um_base = (long) idBase;
                    }
                }
                if (cboUndMedidaSNT.SelectedValue != null)
                {
                    int idUMSNT = int.Parse(cboUndMedidaSNT.SelectedValue.ToString());
                    if (new UnidadMedidaBL().Get(idUMSNT).cod_um != TopList.UnidadMedida)
                    {
                        model.id_um_snt = idUMSNT;
                    }
                }

            }
            catch (Exception e)
            {
                Msg.Ok_Err("Error en la asignación de datos. " + e.Message);
            }

            return model;
        }
        private void SetUnidad(INVt10_unidad_medida obj)
        {
            _isChangedRow = true;
            LimpiarForm(false);
            chkActivo.Checked = (obj.id_estado == Estado.IdActivo) ? true : false;

            lblIdUnidad.Text = obj.id_um.ToString();
            txtNombre.Text = obj.txt_desc;
            txtCodigo.Text = obj.cod_um;
            _codSelected = obj.cod_um;
            txtAbrev.Text = obj.txt_abrv;
            txtUnidadMedidaBase.Text = obj.txt_unid_base;
            txtFactor.Text = obj.dec_factor?.RoundOut();
            txtCantidadBase.Text = obj.qty_base?.RoundOut();
            
            chkUMCompra.Checked = obj.sn_compra;
            chkUMVenta.Checked = obj.sn_venta;

            if (obj.id_um_base != null)
                cboUnidad.SelectedValue = obj.id_um_base;
            else
                cboUnidad.SelectedIndex = -1;

            if (obj.id_um_snt != null)
                cboUndMedidaSNT.SelectedValue = obj.id_um_snt;
            else
                cboUndMedidaSNT.SelectedIndex = -1;

            _idUnidad = (ulong) obj.id_um;
        }

        private bool esValido()
        {
            errorProv.Clear();

            bool no_error = true;

            if (txtNombre.Text.Trim().Length == 0)
            {
                errorProv.SetError(txtNombre, "Este campo es requerido.");
                txtNombre.Focus();
                no_error = false;
            }

            if (cboUnidad.SelectedValue != null && cboUnidad.SelectedIndex > -1 && string.IsNullOrEmpty(txtFactor.Text.Trim()))
            {
                errorProv.SetError(txtFactor, "Debe ingresar el factor asociado a la unidad");
                txtCantidadBase.Focus();
                no_error = false;
            }
            if (string.IsNullOrEmpty(txtFactor.Text.Trim()))
            {
                errorProv.SetError(txtFactor, "Este campo es requerido.");
                txtFactor.Focus();
                no_error = false;
            }
            else if (txtFactor.Text.Trim() == "0")
            {
                errorProv.SetError(txtFactor, "El factor debe ser mayor a cero");
                txtFactor.Focus();
                no_error = false;
            }


            #region Code
            if (no_error)
            {
                string cod = txtCodigo.Text.Trim();
                if (cod.Length > 0)
                {
                    var obj = new ProdUnidadMedidaBL().UnitByCode(cod);
                    if (_tipoOperacion == TipoOperacionABM.Insertar)
                    {
                        if (obj != null && obj.id_um > 0)
                        {
                            tabMain.SelectedTab = tabPagGeneral;
                            Msg.Ok_Err("El código ya está en uso.");
                            errorProv.SetError(txtCodigo, "El código ya está en uso.");
                            txtCodigo.Focus();
                            no_error = false;
                        }
                    }
                    else if (_tipoOperacion == TipoOperacionABM.Modificar)
                    {
                        if (cod != _codSelected && obj != null && obj.id_um > 0)
                        {
                            Msg.Ok_Err("El código ya está en uso.");
                            errorProv.SetError(txtCodigo, "El código ya está en uso.");
                            txtCodigo.Focus();
                            no_error = false;
                        }
                    }
                }
            }
            #endregion

            #region Validar Eliminar
            if (no_error && !chkActivo.Checked && _tipoOperacion == TipoOperacionABM.Modificar)
            {
                if (_idUnidad > 0)
                {
                    // Evalúa la cantidad de hijos de unidad de medida. 0 => no tiene hijo y puede eliminarse, -1   
                    int UndsBase = new ProdUnidadMedidaBL().FindByUndbase(_idUnidad);
                    if (UndsBase > 0)
                    {
                        Msg.Ok_Info("Este registro no se puede desactivar porque se usa en otro lado.", "MENSAJE EAGLE");
                        errorProv.SetError(chkActivo, "No puede desactivarlo, está usándose en otro lado.");
                        chkActivo.Focus();
                        no_error = false;
                    }
                    else if (UndsBase < 0)
                    {
                        Msg.Ok_Err("Error al puede desactivar.", "MENSAJE EAGLE");
                        no_error = false;
                    }
                }
                else
                {
                    MessageBox.Show(this, "No se pudo obtener el id para verificar la validación.", "MENSAJE EAGLE", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    no_error = false;
                }
            }
            #endregion
            return no_error;
        }
        private void Filtrar(int criterio, string filtro)
        {
            int index = 0;
            try
            {

                if (criterio == Filtro.Nombre)
                {
                    DataGridViewRow row = dgvUnidadMedida.Rows
                    .Cast<DataGridViewRow>()
                    .Where(r => r.Cells["NOMBRE"].Value.ToString().ToUpper().Contains(filtro.ToUpper()))
                    .FirstOrDefault();
                    if (row != null)
                    {
                        index = row.Index;
                        if (dgvUnidadMedida.Rows.Count > 0)
                        {
                            dgvUnidadMedida.Rows[index].Selected = true;
                            dgvUnidadMedida.FirstDisplayedScrollingRowIndex = index;
                        }
                    }
                }
                else if (criterio == Filtro.Codigo)
                {
                    DataGridViewRow row = dgvUnidadMedida.Rows
                    .Cast<DataGridViewRow>()
                    .Where(r => r.Cells["CODIGO"].Value.ToString().ToUpper().Contains(filtro.ToUpper()))
                    .FirstOrDefault();
                    if (row != null)
                    {
                        index = row.Index;
                        if (dgvUnidadMedida.Rows.Count > 0)
                        {
                            dgvUnidadMedida.Rows[index].Selected = true;
                            dgvUnidadMedida.FirstDisplayedScrollingRowIndex = index;

                        }
                    }
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(this, "Ocurrió una excepción al seleccionar el producto. " + e.Message, "MENSAJE", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void SeleccionarPorId(long id)
        {
            //Deberá ser capaz de buscar de posicionarse  en ese producto
            //si es que existe para los datos actuales de grilla
            // en caso no exista sencillamente se posicionará 
            //por defecto en el 1er registro si lo hubiera.
            int index = 0;
            try
            {
                //si no haya alguna fila con el id enviado, signfica que no está el id
                DataGridViewRow row = dgvUnidadMedida.Rows
                .Cast<DataGridViewRow>()
                .Where(r => r.Cells["id_um"].Value.ToString().Equals(id.ToString()))
                .FirstOrDefault();
                if (row != null)
                {
                    index = row.Index;
                    if (dgvUnidadMedida.Rows.Count > 0)
                    {
                        dgvUnidadMedida.Rows[index].Selected = true;
                        dgvUnidadMedida.FirstDisplayedScrollingRowIndex = index;

                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(this, "Ocurrió una excepción al seleccionar el producto. " + e.Message, "MENSAJE", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void SeleccionarRegistro()
        {
            _isPending = false;
            if (dgvUnidadMedida.RowCount > 0 && dgvUnidadMedida.SelectedRows.Count > 0 && dgvUnidadMedida.CurrentRow.Index != -1)
            {
                if (ulong.TryParse(GetIdSelected(), out ulong id))
                {
                    if (id > 0)
                    {
                        // Quitar el cast cuando se corriga el modelo
                        var model = new ProdUnidadMedidaBL().Get(id);
                        if (model != null)
                        {
                            _isSelected = false;
                            SetUnidad(model);
                            dgvUnidadMedida.Focus();
                            _isChangedRow = true;
                            _isSelected = true;
                        }
                    }
                }
                else
                {
                    MessageBox.Show(this, "No se pudo capturar el id en la grilla", "MENSAJE", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }
        private string GetIdSelected()
        {
            string id = "-1";
            try
            {
                if (dgvUnidadMedida.SelectedRows.Count > 0 && dgvUnidadMedida.Rows.Count > 0)
                {
                    id = dgvUnidadMedida.SelectedRows[0].Cells[0].Value.ToString();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Excepción al capturar el id seleccionado: " + e.Message);
            }
            return id;
        }

        private void CargarComboFiltro()
        {
            try
            {
                var listFiltro = new ComboFiltro().ListarFiltros();
                cboFiltro.DisplayMember = "TxtCampo";
                cboFiltro.ValueMember = "IdCampo";
                cboFiltro.DataSource = listFiltro;

            }
            catch (Exception e)
            {
                MessageBox.Show(this, "Ocurrió una excepción al cargar el combo de Filtro: " + e.Message, "MENSAJE", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }
        private void LimpiarForm(bool cleanCbo = true)
        {
            _isSelected = false;

            if (_tipoOperacion == TipoOperacionABM.Nuevo)
                chkActivo.Enabled = false;
            else
                chkActivo.Enabled = true;

            chkActivo.Checked = true;

            lblIdUnidad.Text = 0 + "";

            txtNombre.Clear();
            txtFactor.Clear();
            txtCantidadBase.Clear();
            txtUnidadMedidaBase.Clear();
            txtCodigo.Clear();
            txtAbrev.Clear();
            cboUnidad.SelectedValue = (cboUnidad.Items.Count > 0) ? 0 : -1;
            cboUndMedidaSNT.SelectedValue = (cboUndMedidaSNT.Items.Count > 0) ? 0 : -1;
            _factorUniMed = 1;
        }
        private void ControlarBotones(bool eNuevo, bool eDelete, bool eCommit, bool eRollback, bool eSearch, bool eFilter)
        {
            btnNuevo.Enabled = eNuevo;
            btnDelete.Enabled = eDelete;
            btnCommit.Enabled = eCommit;
            btnRollback.Enabled = eRollback;
            btnSearch.Enabled = eSearch;
            btnFilter.Enabled = eFilter;
        }
        private void ControlarEventosABM(long? id = null)
        {

            if (_tipoOperacion == TipoOperacionABM.No_Action)
            {
                _isPending = false;
                ControlarBotones(true, true, false, false, true, true);
                errorProv.Clear();
            }
            else
            {
                if (_tipoOperacion == TipoOperacionABM.Nuevo)
                {
                    ControlarBotones(false, false, true, true, false, false);
                    errorProv.Clear();
                    LimpiarForm();
                    tabMain.SelectedTab = tabPagGeneral;
                    txtNombre.Focus();
                }
                else
                {
                    //Después de hacer el commit-insertar
                    if (_tipoOperacion == TipoOperacionABM.Insertar)
                    {
                        ControlarBotones(true, true, false, false, true, true);
                        LimpiarForm();
                        ActualizarGrilla();
                        long idInsertado = (long)id;
                        SeleccionarPorId(idInsertado);
                        tabMain.SelectedTab = tabPagGeneral;
                        btnNuevo.Focus();
                    }
                    else
                    {
                        if (_tipoOperacion == TipoOperacionABM.Eliminar)
                        {
                            errorProv.Clear();
                            ControlarBotones(true, true, false, false, true, true);
                            LimpiarForm();
                            ActualizarGrilla();
                            tabMain.SelectedTab = tabPagGeneral;
                            btnNuevo.Focus();
                        }
                        else
                        {
                            if (_tipoOperacion == TipoOperacionABM.Rollback)
                            {
                                ControlarBotones(true, true, false, false, true, true);
                                _isPending = false;
                                errorProv.Clear();
                                LimpiarForm();
                                SeleccionarRegistro();
                                tabMain.SelectedTab = tabPagGeneral;
                                btnNuevo.Focus();
                            }
                            else
                            {
                                if (_tipoOperacion == TipoOperacionABM.Cambio)
                                {
                                    ControlarBotones(false, false, true, true, false, false);
                                    _isPending = true;
                                }
                                else
                                {
                                    if (_tipoOperacion == TipoOperacionABM.Modificar)
                                    {
                                        errorProv.Clear();
                                        LimpiarForm();
                                        ControlarBotones(true, true, false, false, true, true);
                                        _isSelected = false;
                                        _isPending = false;
                                        _isChangedRow = false;
                                        ActualizarGrilla();
                                        tabMain.SelectedTab = tabPagGeneral;
                                        if (id != null)
                                        {
                                            long idAct = (long)id;
                                            SeleccionarPorId(idAct);
                                        }
                                        btnNuevo.Focus();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        private void MantenerEstadoABM()
        {
            if (_tipoOperacion == TipoOperacionABM.Nuevo)
            {
                ControlarBotones(false, false, true, true, false, false);
            }
            else if (_tipoOperacion == TipoOperacionABM.Cambio)
            {
                ControlarBotones(false, false, true, true, false, false);
                _isPending = true;
            }
            else if (_tipoOperacion == TipoOperacionABM.No_Action)
            {
                _isPending = false;
                ControlarBotones(true, true, false, false, true, true);
            }
            else
            {
                _isPending = false;
                ControlarBotones(true, true, false, false, true, true);
            }
        }
        private void CargarCombos()
        {
            try
            {

                cboUnidad.DataSource = null;
                cboUnidad.DisplayMember = "txt_desc";
                cboUnidad.ValueMember = "id_um";
                cboUnidad.DataSource = new ProdUnidadMedidaBL().ListaUndMed(Estado.IdActivo, true);
                cboUnidad.DropDownWidth = ControlHelper.DropDownWidth(cboUnidad);

                cboUndMedidaSNT.DataSource = null;
                cboUndMedidaSNT.DisplayMember = "txt_desc";
                cboUndMedidaSNT.ValueMember = "id_um";
                cboUndMedidaSNT.DataSource = new UnidadMedidaBL().ListaUnidadMed(Estado.IdActivo, true);
                cboUndMedidaSNT.DropDownWidth = ControlHelper.DropDownWidth(cboUndMedidaSNT);
                
            }
            catch (Exception e)
            {
                MessageBox.Show(this, "Ocurrió una excepción al cargar los combos aquí: " + e.Message, "MENSAJE", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void CargarGrilla(int? id_estado = null)
        {
            try
            {
                var lista = new ProdUnidadMedidaBL().ListaUndMed(id_estado, enableTopList:true,ocultarBlankReg:true);
                var listaView = lista
                    .Select(x => new { x.id_um, CODIGO = x.cod_um, NOMBRE = x.txt_desc })
                    .OrderBy(x => string.IsNullOrEmpty(x.CODIGO))
                    .ThenBy(x => x.CODIGO, new AlphaNumericComparer())
                    .ThenBy(x => x.NOMBRE).ToList();

                if (lista != null)
                {
                    ContarEstados(lista);
                    dgvUnidadMedida.DataSource = listaView;
                    dgvUnidadMedida.Columns["id_um"].Visible = false;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(this, $"Excepción en cargar la grilla: {e.Message}", "MENSAJE", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }
        private void ActualizarGrilla()
        {
            if (tglListarInactivos.Checked)
                CargarGrilla();
            else
                CargarGrilla(Estado.IdActivo);
        }
        private void ConfigurarGrilla()
        {
            dgvUnidadMedida.RowsDefaultCellStyle.BackColor = ColorTranslator.FromHtml("#ecf0f1");
            dgvUnidadMedida.AlternatingRowsDefaultCellStyle.BackColor = ColorTranslator.FromHtml("#FAFAFA");


            dgvUnidadMedida.ColumnHeadersDefaultCellStyle.BackColor = ColorTranslator.FromHtml("#00B2EE");
            dgvUnidadMedida.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;

            dgvUnidadMedida.DefaultCellStyle.SelectionBackColor = Color.DeepSkyBlue;

            dgvUnidadMedida.Columns["CODIGO"].Width = 70;
            dgvUnidadMedida.Columns["NOMBRE"].Width = 260;

            dgvUnidadMedida.EnableHeadersVisualStyles = false;
        }
        private void ConfigurarControles()
        {
            //txtCostoProd.TextAlign = HorizontalAlignment.Right;
            txtAbrev.MaxLength = 10;
            txtFactor.MaxLength = 50;
            txtCantidadBase.MaxLength = 20;
            txtNombre.MaxLength = 250;
            txtCodigo.MaxLength = 10;
            //txtPvPuConImpto.MaxLength = 19;

        }
        private void ContarEstados(List<INVt10_unidad_medida> lista)
        {
            try
            {
                int numReg = lista.Count;
                int numAct = lista.Where(x => x.id_estado == Estado.IdActivo).ToList().Count;
                int numInac = lista.Where(x => x.id_estado == Estado.IdInactivo).ToList().Count;

                lblNumReg.Text = "Total: " + numReg;
                lblNumActivo.Text = "Activos: " + numAct;
                lblNumInactivo.Text = "Inactivos: " + numInac;
            }
            catch (Exception e)
            {
                MessageBox.Show(this, $"Excepción el contar los estados: {e.Message}");
            }
        }
        private void SetInit()
        {
            lblIdUnidad.Visible = false;
            ConfigurarControles();
            ControlarEventosABM();
            CargarCombos();
            LimpiarForm();
            CargarGrilla(Estado.IdActivo);
            CargarComboFiltro();
            panelFiltro.Visible = false;
            addHandlers();
            tglListarInactivos.AutoCheck = false;
            ConfigurarGrilla();
        }
        #endregion

        #region Eventos de ventana
        private void FormProducto_Load(object sender, EventArgs e)
        {
            SetInit();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            _tipoOperacion = TipoOperacionABM.Nuevo;
            ControlarEventosABM();
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            _tipoOperacion = TipoOperacionABM.Eliminar;
            Eliminar();
        }
        private void btnCommit_Click(object sender, EventArgs e)
        {
            if (_tipoOperacion == TipoOperacionABM.Nuevo)
            {
                _tipoOperacion = TipoOperacionABM.Insertar;
            }
            else
            {
                if (_tipoOperacion == TipoOperacionABM.Cambio)
                {
                    _tipoOperacion = TipoOperacionABM.Modificar;
                }
            }
            Commit();
        }
        private void btnRollback_Click(object sender, EventArgs e)
        {
            _tipoOperacion = TipoOperacionABM.Rollback;
            ControlarEventosABM();
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (panelFiltro.Visible)
            {
                panelFiltro.Visible = false;
            }
            else
            {
                panelFiltro.Visible = true;
            }
        }
        private void btnFilter_Click(object sender, EventArgs e)
        {
            try
            {
                string filtro = txtFiltro.Text.Trim();
                string criterio = "";
                if (cboFiltro.SelectedValue != null)
                {
                    criterio = cboFiltro.SelectedValue.ToString();

                    if (!string.IsNullOrEmpty(criterio) && !filtro.Equals(""))
                    {

                        int idCriterio = int.Parse(criterio);
                        if (idCriterio == Filtro.Nombre)
                        {
                            Filtrar(Filtro.Nombre, filtro);
                        }
                        else if (idCriterio == Filtro.Codigo)
                        {
                            Filtrar(Filtro.Codigo, filtro);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Ocurrió una excepción al filtrar: " + ex.Message, "MENSAJE", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void cboFiltro_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtFiltro.Clear();
            txtFiltro.Focus();
        }

        private void dgvUnidadMedida_SelectionChanged(object sender, EventArgs e)
        {
            errorProv.Clear();
            if (_isPending)
            {
                if (_preguntar)
                {
                    var checkDialog = new CheckBoxDialog();
                    DialogResult rp = checkDialog.ShowDialog();
                    if (rp == DialogResult.Yes)
                    {
                        _tipoOperacion = TipoOperacionABM.Modificar;
                        //al intentar cambiar la fila si no es válido
                        //la actualización, no pasará hasta que sea válido
                        //o se dea rollback.
                        bool isValid = false;
                        string idSelect = GetIdSelected();

                        //Indica que está seleccionado otro registro
                        //que el que se quiere modificar
                        if (idSelect != lblIdUnidad.Text && idSelect != "-1")
                        {
                            isValid = Actualizar();
                            if (isValid)
                            {
                                //Sobreescribe el indice indicado
                                //por el indice que corresponde al seleccionado
                                //que es diferente respecto quién está en el proceso.
                                //manejar 
                                SeleccionarPorId(long.Parse(idSelect));
                            }
                        }
                        else
                        {
                            Actualizar();
                        }


                        _preguntar = !checkDialog.check;
                    }
                    else if (rp == DialogResult.No)
                    {
                        SeleccionarRegistro();
                        _tipoOperacion = TipoOperacionABM.No_Action;
                        ControlarEventosABM();
                    }

                }
                else
                {
                    _tipoOperacion = TipoOperacionABM.Modificar;
                    //al intentar cambiar la fila si no es válido
                    //la actualización, no pasará hasta que sea válido
                    //o se dea rollback.
                    bool isValid = false;
                    string idSelect = GetIdSelected();

                    //Indica que está seleccionado otro registro
                    //que el que se quiere modificar
                    if (idSelect != lblIdUnidad.Text && idSelect != "-1")
                    {
                        isValid = Actualizar();
                        if (isValid)
                        {
                            SeleccionarPorId(long.Parse(idSelect));
                        }
                    }
                    else
                    {
                        Actualizar();
                    }
                }
            }
            else
            {
                SeleccionarRegistro();
                _tipoOperacion = TipoOperacionABM.No_Action;
                ControlarEventosABM();
            }

        }
        private void tglListarInactivos_Click(object sender, EventArgs e)
        {
            //Controla que no se quede pegado la data del registro en caso 
            //no haya registro cambiando de estado del tgl
            // var check = sender as MetroToggle;

            if (_isPending)
            {
                if (_preguntar)
                {
                    var checkDialog = new CheckBoxDialog();
                    DialogResult rp = checkDialog.ShowDialog();

                    if (rp == DialogResult.Yes)
                    {
                        bool isValid = false;
                        _tipoOperacion = TipoOperacionABM.Modificar;
                        isValid = ActualizarProductoEnCheck();
                        //Ya se validó y actualizó pero aún no recarga la grilla
                        if (isValid)
                        {

                            tglListarInactivos.Checked = !tglListarInactivos.Checked;
                            ControlarEventosABM();
                        }
                        _preguntar = !checkDialog.check;
                    }
                    else if (rp == DialogResult.No)
                    {
                        _isPending = false;
                        LimpiarForm();
                        ActualizarGrilla();
                    }

                }
                else if (_preguntar == false)
                {
                    bool isValid = false;
                    _tipoOperacion = TipoOperacionABM.Modificar;
                    isValid = ActualizarProductoEnCheck();
                    //Ya se validó y actualizó pero aún no recarga la grilla
                    if (isValid)
                    {
                        tglListarInactivos.Checked = !tglListarInactivos.Checked;
                        ControlarEventosABM();
                    }
                }
            }

            else
            {
                LimpiarForm();
                ActualizarGrilla();
                if (tglListarInactivos.Checked)
                    CargarGrilla(Estado.IdActivo);
                else
                    CargarGrilla();
                tglListarInactivos.Checked = !tglListarInactivos.Checked;

            }
        }
        private void txtFiltro_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Convert.ToInt32(Keys.Enter))
            {
                btnFilter_Click(null, null);
            }
        }
        private void btnCerrar_Click(object sender, EventArgs e)
        {
            if (_isPending)
            {
                if (_preguntar)
                {
                    var checkDialog = new CheckBoxDialog();
                    DialogResult rp = checkDialog.ShowDialog();
                    if (rp == DialogResult.Yes)
                    {
                        _preguntar = !checkDialog.check;
                        bool isValid = false;
                        _tipoOperacion = TipoOperacionABM.Modificar;
                        isValid = Actualizar();
                        if (isValid)
                        {
                            _isPending = false;
                            Dispose();
                            Hide();
                            Close();
                        }

                    }
                    else if (rp == DialogResult.No)
                    {
                        _isPending = false;
                        Dispose();
                        Hide();
                        Close();
                    }

                }
                else if (_preguntar == false)
                {
                    bool isValid = false;
                    _tipoOperacion = TipoOperacionABM.Modificar;
                    isValid = Actualizar();
                    if (isValid)
                    {
                        _isPending = false;
                        Dispose();
                        Hide();
                        Close();
                    }
                }
            }
            else
            {
                Dispose();
                Hide();
                Close();
            }
        }
        private void cboUnidadMedida_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cboUnidad.SelectedValue?.ToString() is string idUnidadMedida)
            {
                // Corregir a long cuando corriga la bd
                var unidadMedida = new ProdUnidadMedidaBL().Get(ulong.Parse(idUnidadMedida));

                txtUnidadMedidaBase.Text = string.IsNullOrEmpty(unidadMedida.txt_unid_base) ?
                                            unidadMedida.txt_desc : unidadMedida.txt_unid_base;
                if (unidadMedida != null)
                {
                    if (decimal.TryParse(unidadMedida.dec_factor.ToString(), out decimal factor))
                    {
                        _factorUniMed = factor;
                    }

                    if (unidadMedida.id_um_base != null)
                    {
                        // corregir a long
                        var unidadBase = new ProdUnidadMedidaBL().Get((ulong)unidadMedida.id_um_base);
                        if (unidadBase.qty_base != null)
                        {
                            _factorUniMed *= (decimal)unidadBase.qty_base;
                        }
                    }
                    AsingCantidadBase(txtFactor);
                } 
            }
            cboUnidad.DropDownWidth = ControlHelper.DropDownWidth(cboUnidad);

            _isChangedRow = false;
        }
        private void txtCantidad_TextChanged(object sender, EventArgs e)
        {
            if (sender is MetroTextBox TxtCantidad)
            {
                AsingCantidadBase(TxtCantidad);
            }
        }
        private void btnBuscarItem_Click(object sender, EventArgs e)
        {
            try
            {
                int oldValue = 0;

                if (cboUnidad.SelectedValue != null)
                    oldValue = int.Parse(cboUnidad.SelectedValue.ToString());

                var frm = new FormBuscarUnidadMedida();
                frm.ShowDialog();
                if (frm.UnidadMedida != null)
                {
                    SetItem(frm.UnidadMedida);
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(this, $"Excepción cuando se intentaba actualizar el combo. {exc.Message}", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion
        
        private void AsingCantidadBase(MetroTextBox TxtCantidad)
        {
            if (!string.IsNullOrEmpty(TxtCantidad.Text))
            {
                var unitsCant = TxtCantidad.Text;
                var unitsCantbase = decimal.Parse(unitsCant) * _factorUniMed;
                txtCantidadBase.Text = unitsCantbase.RoundOut();
            }
            else
            {
                txtCantidadBase.Text = string.Empty;
            }
        }
        private void SetItem(INVt10_unidad_medida unidadMedida)
        {
            try
            {
                if (int.TryParse(unidadMedida?.id_um.ToString(), out int id))
                {

                    cboUnidad.SelectedValue = long.Parse(id.ToString());
                                       
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al setear: " + ex.Message);
            }
        }

        
    }
}
