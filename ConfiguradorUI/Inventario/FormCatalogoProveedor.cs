﻿using ConfigBusinessEntity;
using ConfigBusinessLogic;
using ConfigBusinessLogic.Inventario;
using ConfigBusinessLogic.Persona;
using ConfigBusinessLogic.Producto;
using ConfigBusinessLogic.Utiles;
using ConfiguradorUI.Buscadores;
using ConfiguradorUI.FormUtil;
using ConfigUtilitarios;
using ConfigUtilitarios.HelperControl;
using ConfigUtilitarios.KeyValues;
using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace ConfiguradorUI.Inventario
{
    public partial class FormCatalogoProveedor : MetroForm
    {
        #region Variables
        bool isSelected = false;
        bool isChangedRow = false;
        bool isPending = false;
        bool preguntar = true;
        public bool actualizar = false;
        private int TipoOperacion = TipoOperacionABM.No_Action;

        string codSelected = "";

        INVt09_item item = null;
        List<INVt07_catalogo_proveedor_dtl> details = null;
        int maxNumItems = 10;

        enum DeleteDtlAction { Remove, ActiveDesactive };

        private PERt03_proveedor prov = null;
        private long idProv;

        DateTime fechaActual = DateTime.Now;
        #endregion
        public FormCatalogoProveedor()
        {
            InitializeComponent();
        }
        #region Metodos

        #region Detalle
        private void SetItem(INVt09_item producto)
        {
            try
            {
                item = new INVt09_item();
                item = producto;

                txtItemCod.Text = producto.cod_item;
                txtItemDesc.Text = producto.txt_desc;
                
            }
            catch (Exception e)
            {
                MessageBox.Show("No se pudo setear el producto. Excepción: " + e.Message, "Mensaje Eagle", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private INVt07_catalogo_proveedor_dtl GetItem()
        {
            try
            {
                var detailItem = new INVt07_catalogo_proveedor_dtl();
                detailItem.INVt09_item = new INVt09_item();

                detailItem.id_item = item.id_item;
                
                detailItem.id_estado = Estado.IdActivo;
                detailItem.txt_estado = Estado.TxtActivo;

                detailItem.INVt09_item.txt_desc = txtItemDesc.Text.Trim();
                return detailItem;
            }
            catch (Exception e)
            {
                MessageBox.Show("No se pudo obtener el producto. Excepción: " + e.Message, "Mensaje Eagle", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }

        private void CleanItem(bool includeItem = false)
        {
            txtItemCod.Clear();
            txtItemDesc.Clear();
            
            if (includeItem) item = null;
        }
        private void CleanDetail(bool includeDetails = false)
        {
            SetCabeceraGridDetail();
            DefinirCabeceraGridDetail();
            if (includeDetails) details = null;
        }

        private bool EditItem(INVt07_catalogo_proveedor_dtl item, bool accumulateQuantity = false)
        {
            try
            {
                var index = details.FindIndex(x => x.id_item == item.id_item);
                if (index != -1)
                {
                    var oldItem = details[index];
                    //Actualizamos los valores
                    
                    oldItem.id_estado = item.id_estado;
                    oldItem.txt_estado = item.txt_estado;
                    details[index] = oldItem;
                    return true;
                }
                return false;
            }
            catch (Exception e )
            {
                MessageBox.Show("No se pudo editar. Excepción en EditItem: " + e.Message, "Mensaje Eagle", MessageBoxButtons.OK, MessageBoxIcon.Error);
                
                return false;
            }
        }
        private void AddItem()
        {
            if (ValidItem())
            {
                var itemObtained = GetItem();
                if (itemObtained != null)
                {
                    if (details == null) details = new List<INVt07_catalogo_proveedor_dtl>();
                    try
                    {
                        if (EditItem(itemObtained, true))
                        {
                            CargarGridProd(details, chkMostrarInactivos.Checked);
                        }
                        //Si el producto no existe en el detalle -> agrega item
                        else if (details.Count < maxNumItems)
                        {
                            long idMaster = 0;
                            if (TipoOperacion != TipoOperacionABM.Nuevo)
                            {
                                long.TryParse(lblIdCatalogo.Text, out idMaster);
                            }
                            itemObtained.id_catalogo = idMaster;
                            details.Add(itemObtained);
                            CargarGridProd(details, chkMostrarInactivos.Checked);
                        }
                        else
                            Msg.Ok_Info($"No puede agregar más items. Ha alcanzado el número máximo de items({maxNumItems}).", "Mensaje Eagle");

                    }
                    catch (Exception e)
                    {
                        MessageBox.Show($"No se pudo agregar el item. Excepción: {e.Message}", "Mensaje Eagle", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        CleanItem(true);
                        errorProvDtl.Clear();
                        txtItemCod.Focus();

                    }
                }
            }
        }
        
        private void RemoveItem(DeleteDtlAction actionDeleteDtl)
        {
            try
            {
                //Item1 : item selected
                //Item2 : index

                var itemSelected_index = GetItemSelected();
                if (details != null && itemSelected_index.Item1 != null)
                {
                    var oldItem = itemSelected_index.Item1;
                    var itemDesc = oldItem.INVt09_item?.txt_desc;

                    if (actionDeleteDtl == DeleteDtlAction.Remove)
                    {
                        if (oldItem.id_catalogo_proveedor_dtl <= 0)
                        {
                            if (Msg.YesNo_Ques($"¿Está seguro de QUITAR el item '{itemDesc}'?") == DialogResult.Yes)
                            {
                                details.RemoveAt(itemSelected_index.Item2);
                                isChangedRow = false;
                                if (details.Count == 0) details = null;
                                CargarGridProd(details, chkMostrarInactivos.Checked);
                            }
                        }
                        else
                        {
                            Msg.Ok_Info($"No puede QUITAR un item que ha guardado. Puede activar/desactivar según sea el caso.");
                        }
                    }
                    else if ((actionDeleteDtl == DeleteDtlAction.ActiveDesactive))
                    {
                        //Para activar
                        if (oldItem.id_estado != Estado.IdActivo)
                        {
                            if (Msg.YesNo_Ques($"¿Está seguro de ACTIVAR el item '{itemDesc}'?") == DialogResult.Yes)
                            {
                                oldItem.id_estado = Estado.IdActivo;
                                oldItem.txt_estado = Estado.TxtActivo;
                                details[itemSelected_index.Item2] = oldItem;
                                CargarGridProd(details, chkMostrarInactivos.Checked);
                            }
                        }
                        //Para desactivar
                        else
                        {
                            if (Msg.YesNo_Ques($"¿Está seguro de DESACTIVAR el item '{itemDesc}'?") == DialogResult.Yes)
                            {
                                oldItem.id_estado = Estado.IdInactivo;
                                oldItem.txt_estado = Estado.TxtInactivo;
                                details[itemSelected_index.Item2] = oldItem;
                                CargarGridProd(details, chkMostrarInactivos.Checked);
                            }
                        }
                    }
                }
                else
                    Msg.Ok_Info($"No hay ningún item .");
            }
            catch (Exception e)
            {
                Msg.Ok_Err($"No se pudo eliminar el item correctamente. Excepción: {e.Message}", "Excepción encontrada");
            }
            finally
            {
                dgvDetail.Focus();
            }
        }
        private void SearchAndSetItem()
        {
            if (!BuscarItem())
            {
                var form = new FormBuscarItem();
                form.ShowDialog();
                if (form.item != null)
                {
                    CleanItem();
                    errorProvDtl.Clear();
                    SetItem(form.item);
                }
            }
        }
        private Tuple<INVt07_catalogo_proveedor_dtl, int> GetItemSelected()
        {
            if (long.TryParse(ControlHelper.DgvGetCellValueSelected(dgvDetail, 0), out long id))
            {
                int index = details.FindIndex(x => x.id_item == id);
                if (index != -1 && details[index] != null)
                {
                    return new Tuple<INVt07_catalogo_proveedor_dtl, int>(details[index], index);
                }
            }
            return new Tuple<INVt07_catalogo_proveedor_dtl, int>(null, -1);
        }
        private bool BuscarItem()
        {
            string codProd = txtItemCod.Text.Trim();
            //Si está vació el txt -> abre el form
            if (codProd == "")
                return false;

            //De lo contrario hace la búsqueda del producto
            var list = new ProductoItemBL().BuscarItem(codProd, "", "",Estado.Ignorar, Estado.Ignorar, Estado.Ignorar, Estado.Ignorar, Estado.Ignorar,  Estado.IdActivo);
            //Si solo hay un producto con ese filtro
            if (list != null && list.Count() == 1)
            {
                foreach (var i in list)
                {
                    //Si el prouducto encontrado es distinto al prouducto ya cargado -> carga prod y no abre form
                    if (item == null || i.id_item != item.id_item)
                    {
                        CleanItem();
                        errorProvDtl.Clear();
                        SetItem(i);
                        return true;
                    }
                }
                //Si el producto encontrado es el mismo que el que ya estaba cargado -> abre from
                return false;
            }
            //si no hay ningún producto con esa característica o hay más de uno -> abre form
            else
                return false;
        }
        private bool ValidItem()
        {
            errorProvDtl.Clear();
            var valid = true;

            if (!(item != null && item.id_item > 0))
            {
                valid = false;
                Msg.Ok_Info("No se ha seleccionado a ningún producto. Presione Enter y seleccione.");
                txtItemCod.Focus();
            }
            else
            {
                
                if (txtItemDesc.Text.Trim() == "")
                {
                    valid = false;
                    errorProvDtl.SetError(lblProducto, ValidationMsg.Required);
                    txtItemDesc.Focus();
                }
            }
            return valid;
        }

        private void CargarGridProd(IEnumerable<INVt07_catalogo_proveedor_dtl> list, bool showInactive = false)
        {
            if (list != null)
            {
                var finalList = showInactive ? list : list.Where(x => x.id_estado == Estado.IdActivo);

                dgvDetail.DataSource = finalList.Select(x => new
                {
                    ID_PROD = x.id_item,
                    
                    PRODUCTO = x.INVt09_item != null ? x.INVt09_item.txt_desc : "NO SE PUEDE MOSTRAR",
                    
                    ACTIVO = x.id_estado == Estado.IdActivo
                }).OrderBy(x => x.PRODUCTO).ToList();

            }
            else
            {
                SetCabeceraGridDetail();
            }
            DefinirCabeceraGridDetail();
        }

        #region útiles

        private void SetCabeceraGridDetail()
        {
            var detailHeader = new List<INVt07_catalogo_proveedor_dtl>();
            dgvDetail.DataSource = detailHeader.Select(x => new
            {
                ID_PROD = "",
                
                PRODUCTO = "",
                
                ACTIVO = true
            }).ToList();
        }

        private void DefinirCabeceraGridDetail()
        {
            try
            {
                dgvDetail.Columns["ID_PROD"].Visible = false;

                

                dgvDetail.Columns["PRODUCTO"].Width = 602;
                
                dgvDetail.Columns["ACTIVO"].Width = 55;

            }
            catch (Exception e)
            {
                MessageBox.Show($"No se pudo definir la cabecera de la grilla de productos. Excepción: {e.Message}", "Excepción encontrada", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region Métodos para eventos

        private void SearchAndSetItem_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Convert.ToInt32(Keys.Enter))
            {
                SearchAndSetItem();
            }
        }

        #endregion

        #endregion

        #region Maestro
        private void AddHandlers()
        {
            //Form
            KeyPreview = true;
            KeyDown += ControlHelper.FormCloseShiftEsc_KeyDown;

            //Agregando Handlers que se disparan al cambiar el contenido, estado o selección
            
            txtItemCod.KeyPress += SearchAndSetItem_KeyPress;
            txtItemDesc.KeyPress += SearchAndSetItem_KeyPress;

            var txts = new[] {  txtCodigo,txtProveedor};
            foreach (var txt in txts)
            {
                txt.TextChanged += new EventHandler(OnContentChanged);

            }

            var chks = new[] { chkActivo };

            foreach (var chk in chks)
            {
                chk.CheckedChanged += new EventHandler(OnContentChanged);
            }

            

            dgvDetail.DataSourceChanged += new EventHandler(OnContentChanged);

        }
        protected void OnContentChanged(object sender, EventArgs e)
        {
            if (isSelected && isChangedRow == false && TipoOperacion != TipoOperacionABM.Cambio)
            {
                TipoOperacion = TipoOperacionABM.Cambio;
                ControlarEventosABM();
            }
        }
        private void CambioEnControl(object sender, EventArgs e)
        {
            //invocado con el IDE (por repetición).
            isChangedRow = false;
        }
        private void Commit()
        {
            try
            {
                if (TipoOperacion == TipoOperacionABM.Insertar)
                {
                    if (EsValido())
                    {
                        var obj = new INVt06_catalogo_proveedor();
                        obj = GetObjeto();
                        long id = new CatalogoProveedorBL().InsertarCatalogoProveedor(obj);
                        if (id > 0)
                            actualizar = true;
                        ControlarEventosABM(id);
                    }
                }
                else
                {
                    Actualizar();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(this, "Ocurrió una excepción en el commit:" + e.Message, "MENSAJE");
            }

        }
        private void Eliminar()
        {
            if (TipoOperacion == TipoOperacionABM.Eliminar)
            {
                if (dgvCatalogo.RowCount > 0)
                {
                    if (dgvCatalogo.SelectedRows.Count > 0)
                    {
                        try
                        {
                            long id = 0;
                            if (long.TryParse(lblIdCatalogo.Text, out id) && id > 0)
                            {
                                DialogResult rp = MessageBox.Show("¿Seguro de eliminar el registro?", "CONFIRMACIÓN", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                if (rp == DialogResult.Yes)
                                {
                                    //bool validDelete = new UtilBL().ValidarDelete(id, CodValDelete.CatalogoProv_CatalogoProvDtl);
                                    //if (validDelete)
                                    //{
                                        new CatalogoProveedorBL().EliminarCatalogoProveedor(id);
                                        actualizar = true;
                                        ControlarEventosABM();
                                    //}
                                    //else
                                    //{
                                    //    TipoOperacion = TipoOperacionABM.No_Action;
                                    //    ControlarEventosABM();
                                    //    MessageBox.Show(this, "Este registro no se puede eliminar porque se usa en otro lado.", "MENSAJE EAGLE", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    //}

                                }
                            }
                            else
                            {
                                MessageBox.Show(this, "El ID es incorrecto.", "MENSAJE");
                            }
                        }
                        catch (Exception e)
                        {
                            MessageBox.Show(this, "Ocurrió una excepción en el intento de eliminación: " + e.Message, "MENSAJE");
                        }
                    }
                    else
                    {
                        MessageBox.Show(this, "No se ha seleccinado ningún registro.", "MENNSAJE");
                    }
                }
                else
                {
                    MessageBox.Show(this, "No hay registros", "MENSAJE", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }

        }

        private bool Actualizar()
        {
            bool isValid = false;
            try
            {
                if (TipoOperacion == TipoOperacionABM.Modificar && isSelected && isPending)
                {
                    if (EsValido())
                    {
                        INVt06_catalogo_proveedor obj = new INVt06_catalogo_proveedor();
                        obj = GetObjeto();
                        long id = 0;
                        if (long.TryParse(lblIdCatalogo.Text, out id))
                        {
                            obj.id_catalogo = id;
                            bool success = new CatalogoProveedorBL().ActualizarCatalogo(obj);
                            actualizar = true;
                            ControlarEventosABM(obj.id_catalogo);
                            if (!success)
                            {
                                MessageBox.Show("No se pudo actualizar el catalogo.", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        isValid = true;
                    }
                    else { isValid = false; }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Ocurrió una excepción en Actualizar Producto: " + e.Message);
            }
            return isValid;
        }
        private bool ActualizarEnCheck()
        {
            bool isValid = false;
            try
            {
                if (TipoOperacion == TipoOperacionABM.Modificar && isSelected && isPending)
                {
                    if (EsValido())
                    {
                        INVt06_catalogo_proveedor obj = new INVt06_catalogo_proveedor();
                        obj = GetObjeto();
                        long id = 0;
                        if (long.TryParse(lblIdCatalogo.Text, out id))
                        {
                            obj.id_catalogo = id;
                            bool success = new CatalogoProveedorBL().ActualizarCatalogo(obj);
                            actualizar = true;
                            if (!success)
                            {
                                MessageBox.Show("No se pudo actualizar el catalogo.", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        isValid = true;
                    }
                    else { isValid = false; }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Ocurrió una excepción en Actualizar en Check: " + e.Message);
            }
            return isValid;
        }

        private INVt06_catalogo_proveedor GetObjeto()
        {
            INVt06_catalogo_proveedor obj = new INVt06_catalogo_proveedor();
            try
            {
                obj.txt_proveedor = txtProveedor.Text.Trim();
                obj.id_proveedor = idProv;
                obj.fecha_creacion = fechaActual;
                obj.id_estado = chkActivo.Checked ? Estado.IdActivo : Estado.IdInactivo;
                obj.txt_estado = chkActivo.Checked ? Estado.TxtActivo : Estado.TxtInactivo;
                obj.cod_catalogo = txtCodigo.Text.Trim();
                

                if (details != null)
                    details.ForEach(x => x.INVt09_item = null);
                obj.INVt07_catalogo_proveedor_dtl = details;


                


            }
            catch (Exception e)
            {
                MessageBox.Show(this, "Excepción en el Get: " + e.Message);
            }

            return obj;
        }
        private void SetObjeto(INVt06_catalogo_proveedor obj)
        {
            try
            {
                
                isChangedRow = true;
                LimpiarForm();

                chkActivo.Checked = (obj.id_estado == Estado.IdActivo) ? true : false;

                lblIdCatalogo.Text = obj.id_catalogo.ToString();
                codSelected = obj.cod_catalogo;
                idProv = obj.id_proveedor;
                
                txtCodigo.Text = obj.cod_catalogo;

                if (obj.id_proveedor != 0)
                {
                    var nProv = new ProveedorBL().ProveedorXId(obj.id_proveedor);
                    SetProveedor(nProv);
                }


                details = obj.INVt07_catalogo_proveedor_dtl?.ToList();
                CargarGridProd(obj.INVt07_catalogo_proveedor_dtl, chkMostrarInactivos.Checked);

                dgvCatalogo.Focus();

            }
            catch (Exception e)
            {
                MessageBox.Show(this, "Excepción en el Set: " + e.Message);
            }

        }
        //
        private void SetProveedor(PERt03_proveedor proveedor)
        {
            try
            {
                prov = new PERt03_proveedor();
                prov = proveedor;
                txtProveedor.Text = proveedor.txt_nom_comercial;
                idProv = proveedor.id_proveedor;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al setear el Proveedor: " + ex.Message);
            }
        }
        //
        private bool EsValido()
        {
            bool no_error = true;
            errorProv.Clear();
            errorProvDtl.Clear();

           

            if (idProv == 0 )
            {
                tabMain.SelectedTab = tabPagGeneral;
                errorProv.SetError(txtProveedor, "Este campo es requerido.");
                txtProveedor.Focus();
                no_error = false;
            }

            #region código único

            if (no_error)
            {
                string cod = txtCodigo.Text.Trim();
                if (cod.Length > 0)
                {

                    if (int.TryParse(cod, out int numCod) && numCod == Reserved.Code)
                    {
                        tabMain.SelectedTab = tabPagGeneral;
                        string msg = $"El código '{Reserved.Code.ToString()}' es reservado para el sistema.";
                        MessageBox.Show(msg, "MENSAJE EAGLE", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        errorProv.SetError(txtCodigo, msg);
                        txtCodigo.Focus();
                        no_error = false;
                    }
                    else
                    {

                        var obj = new CatalogoProveedorBL().CatalogoXCod(cod);
                        if (TipoOperacion == TipoOperacionABM.Insertar)
                        {
                            if (obj != null && obj.id_catalogo > 0)
                            {
                                tabMain.SelectedTab = tabPagGeneral;
                                MessageBox.Show("El código ya está en uso.", "MENSAJE EAGLE", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                errorProv.SetError(txtCodigo, "El código ya está en uso.");
                                txtCodigo.Focus();
                                no_error = false;
                            }
                        }
                        else if (TipoOperacion == TipoOperacionABM.Modificar)
                        {
                            if (cod != codSelected && obj != null && obj.id_catalogo > 0)
                            {
                                tabMain.SelectedTab = tabPagGeneral;
                                MessageBox.Show("El código ya está en uso.", "MENSAJE EAGLE", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                errorProv.SetError(txtCodigo, "El código ya está en uso.");
                                txtCodigo.Focus();
                                no_error = false;
                            }
                        }
                    }

                }
            }

            #endregion

            #region Validación Details

            if (no_error)
            {
                if (details == null || details.Count == 0)
                {
                    errorProvDtl.SetError(dgvDetail, "Se requiere al menos de un item.");
                    txtItemCod.Focus();
                    no_error = false;
                }
                else
                {
                    var msjValid = "";
                    var numActivos = 0;
                    foreach (var item in details)
                    {
                        //if (!ValidAmountRange(item.cantidad))
                        
                        if (item.id_estado == Estado.IdActivo)
                        {
                            numActivos++;
                        }
                    }
                    if (numActivos == 0)
                    {
                        no_error = false;
                        msjValid = "- Al menos un producto del combo debe estar activo.\n" + msjValid;
                    }
                    if (msjValid != "")
                    {
                        Msg.Ok_Info(msjValid, "VALIDACIÓN DEL CATALOGO");
                    }
                }

            }

            #endregion

           

            #region validación delete

            if (no_error && !chkActivo.Checked && TipoOperacion == TipoOperacionABM.Modificar)
            {
                long id = 0;
                if (long.TryParse(lblIdCatalogo.Text, out id))
                {
                    //bool validDelete = false;
                    //validDelete = new UtilBL().ValidarDelete(id, CodValDelete.CatalogoProv_CatalogoProvDtl);
                    //if (!validDelete)
                    //{
                    //    MessageBox.Show(this, "Este registro no se puede desactivar porque se usa en otro lado.", "MENSAJE EAGLE", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //    tabMain.SelectedTab = tabPagGeneral;
                    //    errorProv.SetError(chkActivo, "No puede desactivarlo, está usándose en otro lado.");
                    //    chkActivo.Focus();
                        no_error = true;
                    //}
                }
                else
                {
                    MessageBox.Show(this, "No se pudo obtener el id para verificar la validación.", "MENSAJE EAGLE", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    no_error = false;
                }
            }

            #endregion

            return no_error;
        }

        private void Filtrar(int criterio, string filtro)
        {
            int index = 0;
            try
            {
                //si no haya alguna fila con el id enviado, signfica que no está el id

                if (criterio == Filtro.Nombre)
                {
                    DataGridViewRow row = dgvCatalogo.Rows
                    .Cast<DataGridViewRow>()
                    .Where(r => r.Cells["PROVEEDOR"].Value.ToString().ToUpper().Contains(filtro.ToUpper()))
                    .FirstOrDefault();
                    if (row != null)
                    {
                        index = row.Index;
                        if (dgvCatalogo.Rows.Count > 0)
                        {
                            dgvCatalogo.Rows[index].Selected = true;
                            dgvCatalogo.FirstDisplayedScrollingRowIndex = index;
                        }
                    }
                }
                else if (criterio == Filtro.Codigo)
                {
                    DataGridViewRow row = dgvCatalogo.Rows
                    .Cast<DataGridViewRow>()
                    .Where(r => r.Cells["CODIGO"].Value.ToString().ToUpper().Contains(filtro.ToUpper()))
                    .FirstOrDefault();
                    if (row != null)
                    {
                        index = row.Index;
                        if (dgvCatalogo.Rows.Count > 0)
                        {
                            dgvCatalogo.Rows[index].Selected = true;
                            dgvCatalogo.FirstDisplayedScrollingRowIndex = index;

                        }
                    }
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(this, "Ocurrió una excepción al seleccionar el producto: " + e.Message, "MENSAJE EAGLE", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void SeleccionarPorId(long id)
        {
            //Deberá ser capaz de buscar de posicionarse  en ese producto
            //si es que existe para los datos actuales de grilla
            // en caso no exista sencillamente se posicionará 
            //por defecto en el 1er registro si lo hubiera.
            int index = 0;
            try
            {
                //si no haya alguna fila con el id enviado, signfica que no está el id
                DataGridViewRow row = dgvCatalogo.Rows
                .Cast<DataGridViewRow>()
                .Where(r => r.Cells["id_catalogo"].Value.ToString().Equals(id.ToString()))
                .FirstOrDefault();
                if (row != null)
                {
                    index = row.Index;
                    if (dgvCatalogo.Rows.Count > 0)
                    {
                        dgvCatalogo.Rows[index].Selected = true;
                        dgvCatalogo.FirstDisplayedScrollingRowIndex = index;
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(this, "Ocurrió una excepción al seleccionar el producto por ID: " + e.Message, "MENSAJE EAGLE", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void SeleccionarRegistro()
        {
            isPending = false;
            if (dgvCatalogo.RowCount > 0 &&
                dgvCatalogo.SelectedRows.Count > 0)
            {
                long id = 0;
                if (long.TryParse(GetIdSelected(), out id))
                {
                    if (id > 0)
                    {
                        
                        var obj = new CatalogoProveedorBL().CatalogoXId(id);
                        if (obj != null)
                        {
                            //la primera vez no se ejecuta el evento enlazado.
                            isSelected = false;
                            SetObjeto(obj);
                            isChangedRow = true;
                            isSelected = true;
                        }
                    }
                }
                else
                {
                    MessageBox.Show(this, "No se pudo capturar el id en la grilla", "MENSAJE EAGLE", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }
        private string GetIdSelected()
        {
            string id = "-1";
            try
            {
                if (dgvCatalogo.SelectedRows.Count > 0 && dgvCatalogo.Rows.Count > 0)
                {
                    id = dgvCatalogo.SelectedRows[0].Cells[0].Value.ToString();
                    //var idProveedor = dgvCatalogo.SelectedRows[0].Cells[0].Value.ToString();
                    //var lista = new CatalogoProveedorBL().ObtenerIdCatalogo(idProveedor);

                    //if (lista != string.Empty)
                    //{
                    //    id = lista;
                    //    if (id == string.Empty || id == null)
                    //    {
                    //        idProv = 0;
                    //    }
                    //    else { idProv = Convert.ToInt64(id); }
                    //}
                    //else
                    //{
                    //    LimpiarForm();
                    //    idProv = Convert.ToInt64(idProveedor);
                    //    txtProveedor.Text = dgvCatalogo.SelectedRows[0].Cells[2].Value.ToString();
                    //}

                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Excepción al capturar el id seleccionado: " + e.Message);
            }
            return id;
        }

        private void CargarComboFiltro()
        {
            try
            {
                var listFiltro = new ComboFiltro().ListarFiltros();
                cboFiltro.DisplayMember = "TxtCampo";
                cboFiltro.ValueMember = "IdCampo";
                cboFiltro.DataSource = listFiltro;

            }
            catch (Exception e)
            {
                MessageBox.Show(this, "Ocurrió una excepción al cargar el combo de Filtro: " + e.Message, "MENSAJE EAGLE", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }
        private void LimpiarForm()
        {
            isSelected = false;
            
            chkActivo.Enabled = false;
            
            lblIdCatalogo.Text = 0 + "";
            idProv = 0;
            chkActivo.Checked = true;
            codSelected = "";
            
            txtCodigo.Clear();
            txtProveedor.Clear();
            errorProvDtl.Clear();
            CleanItem(true);
            CleanDetail(true);

            

        }

        private void ControlarBotones(bool eNuevo, bool eDelete, bool eCommit, bool eRollback, bool eSearch, bool eFilter)
        {
            
            btnDelete.Enabled = eDelete;
            btnCommit.Enabled = eCommit;
            btnRollback.Enabled = eRollback;
            btnSearch.Enabled = eSearch;
            btnFilter.Enabled = eFilter;
        }
        private void ControlarEventosABM(long? id = null)
        {

            if (TipoOperacion == TipoOperacionABM.No_Action)
            {
                isPending = false;
                ControlarBotones(true, true, false, false, true, true);
                errorProv.Clear();
                //tabProducto.SelectedTab = tabPagGeneral;
            }
            else
            {
                if (TipoOperacion == TipoOperacionABM.Nuevo)
                {
                    ControlarBotones(false, false, true, true, false, false);
                    errorProv.Clear();
                    LimpiarForm();
                    tabMain.SelectedTab = tabPagGeneral;
                    
                }
                else
                {
                    //Después de hacer el commit-insertar
                    if (TipoOperacion == TipoOperacionABM.Insertar)
                    {
                        ControlarBotones(true, true, false, false, true, true);
                        LimpiarForm();

                        if (tglListarInactivos.Checked) { ActualizarGrilla(); } else { ActualizarGrilla(Estado.IdActivo); }

                        long idInsertado = (long)id;
                        SeleccionarPorId(idInsertado);
                        tabMain.SelectedTab = tabPagGeneral;
                        btnCommit.Focus();
                    }
                    else
                    {
                        //Después de hacer commit-eliminar
                        if (TipoOperacion == TipoOperacionABM.Eliminar)
                        {
                            errorProv.Clear();
                            ControlarBotones(true, true, false, false, true, true);
                            LimpiarForm();
                            if (tglListarInactivos.Checked) { ActualizarGrilla(); } else { ActualizarGrilla(Estado.IdActivo); }
                            tabMain.SelectedTab = tabPagGeneral;
                            btnCommit.Focus();
                        }
                        else
                        {
                            if (TipoOperacion == TipoOperacionABM.Rollback)
                            {
                                ControlarBotones(true, true, false, false, true, true);
                                isPending = false;
                                errorProv.Clear();
                                LimpiarForm();
                                SeleccionarRegistro();
                                tabMain.SelectedTab = tabPagGeneral;
                                btnCommit.Focus();
                            }
                            else
                            {
                                //ver lo del error provider
                                if (TipoOperacion == TipoOperacionABM.Cambio)
                                {
                                    ControlarBotones(false, false, true, true, false, false);
                                    isPending = true;
                                }
                                else
                                {
                                    if (TipoOperacion == TipoOperacionABM.Modificar)
                                    {
                                        errorProv.Clear();
                                        LimpiarForm();
                                        ControlarBotones(true, true, false, false, true, true);
                                        isSelected = false;
                                        isPending = false;
                                        isChangedRow = false;

                                        if (tglListarInactivos.Checked) { ActualizarGrilla(); } else { ActualizarGrilla(Estado.IdActivo); }

                                        tabMain.SelectedTab = tabPagGeneral;

                                        if (id != null)
                                        {
                                            long idAct = (long)id;
                                            SeleccionarPorId(idAct);
                                        }
                                        btnCommit.Focus();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        private void MantenerEstadoABM()
        {
            if (TipoOperacion == TipoOperacionABM.Nuevo)
            {
                ControlarBotones(false, false, true, true, false, false);
            }
            else if (TipoOperacion == TipoOperacionABM.Cambio)
            {
                ControlarBotones(false, false, true, true, false, false);
                isPending = true;
            }
            else if (TipoOperacion == TipoOperacionABM.No_Action)
            {
                isPending = false;
                ControlarBotones(true, true, false, false, true, true);
            }
            else
            {
                isPending = false;
                ControlarBotones(true, true, false, false, true, true);
            }
        }
        private void CargarGrilla(int? id_estado = null)
        {
            try
            {
                var lista = new CatalogoProveedorBL().ListaCatalogoProveedor(id_estado);
                var listaView = lista.Select(x => new { x.id_catalogo, CODIGO = x.cod_catalogo,  PROVEEDOR = x.txt_proveedor })
                .OrderBy(x => string.IsNullOrEmpty(x.CODIGO)).ThenBy(x => x.CODIGO, new AlphaNumericComparer()).ThenBy(x => x.PROVEEDOR).ToList();

                if (lista != null)
                {
                    ContarEstados(lista);
                    dgvCatalogo.DataSource = listaView;
                    dgvCatalogo.Columns["id_catalogo"].Visible = false;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(this, $"Excepción en cargar la grilla: {e.Message}");

            }
        }
        private void ActualizarGrilla(int? id_estado = null)
        {
            CargarGrilla(id_estado);
        }
        private void ConfigurarGrilla()
        {
            dgvCatalogo.RowsDefaultCellStyle.BackColor = ColorTranslator.FromHtml("#ecf0f1");
            dgvCatalogo.AlternatingRowsDefaultCellStyle.BackColor = ColorTranslator.FromHtml("#FAFAFA");

            //Cabecera
            dgvCatalogo.ColumnHeadersDefaultCellStyle.BackColor = ColorTranslator.FromHtml("#00B2EE");
            dgvCatalogo.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            //Selección
            dgvCatalogo.DefaultCellStyle.SelectionBackColor = Color.DeepSkyBlue;

            //Para que no sobreescriba los estilos de cabecera
            dgvCatalogo.EnableHeadersVisualStyles = false;

            //Configurando columnas del grid
            dgvCatalogo.AllowUserToResizeColumns = true;
            dgvCatalogo.Columns["CODIGO"].HeaderText = "CÓDIGO";
            //dgvCatalogo.Columns["PROVEEDOR"].HeaderText = "PROVEEDOR";

            dgvCatalogo.Columns["CODIGO"].Width = 100;
            dgvCatalogo.Columns["PROVEEDOR"].Width = 248;
            //dgvCatalogo.Columns["PROVEEDOR"].Width = 300;
        }
        private void SetMaxLengthTxt()
        {
            txtCodigo.MaxLength = 20;
            txtItemCod.MaxLength = 50;
            
        }
        private void ContarEstados(List<INVt06_catalogo_proveedor> lista)
        {
            try
            {
                int numReg = lista.Count;
                int numAct = lista.Where(x =>  x.id_estado == Estado.IdActivo).ToList().Count;
                int numInac = lista.Where(x => x.id_estado == Estado.IdInactivo).ToList().Count;

                lblNumReg.Text = "Total: " + numReg;
                lblNumActivo.Text = "Activos: " + numAct;
                lblNumInactivo.Text = "Inactivos: " + numInac;
            }
            catch (Exception e)
            {
                MessageBox.Show(this, $"Excepción el contar los estados: {e.Message}");
            }
        }
        private void SetInit()
        {

            #region Controls
            lblIdCatalogo.Visible = false;
            txtItemDesc.ReadOnly = true;


            chkActivo.Enabled = false;
            #region Dgv

            dgvDetail.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedCells;
            dgvDetail.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            SetCabeceraGridDetail();
            DefinirCabeceraGridDetail();
            ControlHelper.DgvReadOnly(dgvDetail);
            ControlHelper.DgvLightStyle(dgvDetail);


            #endregion

            #endregion
            
            SetMaxLengthTxt();
            ControlarEventosABM();
            
            LimpiarForm();
            CargarGrilla(Estado.IdActivo);
            CargarComboFiltro();
            panelFiltro.Visible = false;
            AddHandlers();
            tglListarInactivos.AutoCheck = false;
            ConfigurarGrilla();
        }
        private void CerrarForm()
        {
            Dispose();
            Hide();
            Close();
        }

        #endregion
        
        #endregion

        #region Eventos
        

        private void FormCatalogoProveedor_Load(object sender, EventArgs e)
        {
            SetInit();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            TipoOperacion = TipoOperacionABM.Nuevo;
            ControlarEventosABM();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            TipoOperacion = TipoOperacionABM.Eliminar;
            Eliminar();
        }

        private void btnCommit_Click(object sender, EventArgs e)
        {
            
            if (Convert.ToInt16(lblIdCatalogo.Text) == 0)
            { TipoOperacion = TipoOperacionABM.Nuevo; }
            else { TipoOperacion = TipoOperacionABM.Cambio; }

            if (TipoOperacion == TipoOperacionABM.Nuevo)
            {
                TipoOperacion = TipoOperacionABM.Insertar;
            }
            else
            {
                if (TipoOperacion == TipoOperacionABM.Cambio)
                {
                    TipoOperacion = TipoOperacionABM.Modificar;
                }
            }
            Commit();
        }

        private void btnRollback_Click(object sender, EventArgs e)
        {
            TipoOperacion = TipoOperacionABM.Rollback;
            ControlarEventosABM();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            panelFiltro.Visible = !panelFiltro.Visible;
            txtFiltro.Focus();
        }

        private void btnFilter_Click(object sender, EventArgs e)
        {
            try
            {
                string filtro = txtFiltro.Text.Trim();
                string criterio = "";
                if (cboFiltro.SelectedValue != null)
                {
                    criterio = cboFiltro.SelectedValue.ToString();

                    if (!string.IsNullOrEmpty(criterio) && !filtro.Equals(""))
                    {

                        int idCriterio = int.Parse(criterio);
                        if (idCriterio == Filtro.Nombre)
                        {
                            Filtrar(Filtro.Nombre, filtro);
                        }
                        else if (idCriterio == Filtro.Codigo)
                        {
                            Filtrar(Filtro.Codigo, filtro);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Ocurrió una excepción al filtrar: " + ex.Message, "MENSAJE", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void cboFiltro_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtFiltro.Clear();
            txtFiltro.Focus();
        }

        private void dgvCatalogo_SelectionChanged(object sender, EventArgs e)
        {
            errorProv.Clear();
            if (isPending)
            {
                if (preguntar)
                {
                    var checkDialog = new CheckBoxDialog();
                    DialogResult rp = checkDialog.ShowDialog();
                    if (rp == DialogResult.Yes)
                    {
                        TipoOperacion = TipoOperacionABM.Modificar;
                        //al intentar cambiar la fila si no es válido
                        //la actualización, no pasará hasta que sea válido
                        //o se dea rollback.
                        bool isValid = false;
                        string idSelect = GetIdSelected();

                        //Indica que está seleccionado otro registro
                        //que el que se quiere modificar
                        if (idSelect != lblIdCatalogo.Text && idSelect != "-1")
                        {
                            isValid = Actualizar();
                            if (isValid)
                            {
                                //Sobreescribe el indice indicado
                                //por el indice que corresponde al seleccionado
                                //que es diferente respecto quién está en el proceso.
                                //manejar 
                                SeleccionarPorId(long.Parse(idSelect));
                            }
                        }
                        else
                        {
                            Actualizar();
                        }


                        preguntar = !checkDialog.check;
                    }
                    else if (rp == DialogResult.No)
                    {
                        SeleccionarRegistro();
                        TipoOperacion = TipoOperacionABM.No_Action;
                        ControlarEventosABM();
                    }

                }
                else if (preguntar == false)
                {
                    TipoOperacion = TipoOperacionABM.Modificar;
                    //al intentar cambiar la fila si no es válido
                    //la actualización, no pasará hasta que sea válido
                    //o se dea rollback.
                    bool isValid = false;
                    string idSelect = GetIdSelected();

                    //Indica que está seleccionado otro registro
                    //que el que se quiere modificar
                    if (idSelect != lblIdCatalogo.Text && idSelect != "-1")
                    {
                        isValid = Actualizar();
                        if (isValid)
                        {
                            SeleccionarPorId(long.Parse(idSelect));
                        }
                    }
                    else
                    {
                        Actualizar();
                    }
                }
            }
            else
            {
                SeleccionarRegistro();
                TipoOperacion = TipoOperacionABM.No_Action;
                ControlarEventosABM();
            }
        }

        private void tglListarInactivos_Click(object sender, EventArgs e)
        {
            if (isPending)
            {
                if (preguntar)
                {
                    var checkDialog = new CheckBoxDialog();
                    DialogResult rp = checkDialog.ShowDialog();

                    if (rp == DialogResult.Yes)
                    {
                        bool isValid = false;
                        TipoOperacion = TipoOperacionABM.Modificar;
                        isValid = ActualizarEnCheck();
                        //Ya se validó y actualizó pero aún no recarga la grilla
                        if (isValid)
                        {
                            if (tglListarInactivos.Checked)
                                tglListarInactivos.Checked = false;
                            else
                                tglListarInactivos.Checked = true;
                            ControlarEventosABM();
                        }
                        preguntar = !checkDialog.check;
                    }
                    else if (rp == DialogResult.No)
                    {
                        isPending = false;
                        LimpiarForm();
                        if (tglListarInactivos.Checked)
                        {
                            tglListarInactivos.Checked = false;
                            ActualizarGrilla(Estado.IdActivo);
                        }
                        else
                        {
                            tglListarInactivos.Checked = true;
                            ActualizarGrilla();
                        }
                    }

                }
                else if (preguntar == false)
                {
                    bool isValid = false;
                    TipoOperacion = TipoOperacionABM.Modificar;
                    isValid = ActualizarEnCheck();
                    //Ya se validó y actualizó pero aún no recarga la grilla
                    if (isValid)
                    {
                        if (tglListarInactivos.Checked)
                            tglListarInactivos.Checked = false;
                        else
                            tglListarInactivos.Checked = true;
                        ControlarEventosABM();
                    }
                }
            }

            else
            {
                LimpiarForm();
                if (tglListarInactivos.Checked)
                {
                    tglListarInactivos.Checked = false;
                    ActualizarGrilla(Estado.IdActivo);
                }
                else
                {
                    tglListarInactivos.Checked = true;
                    ActualizarGrilla();
                }

            }
        }

        private void txtFiltro_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Convert.ToInt32(Keys.Enter))
            {
                btnFilter_Click(null, null);
            }
        }

        private void dgvBordered_Paint(object sender, PaintEventArgs e)
        {
            ControlHelper.DgvSetColorBorder(sender, e);
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            if (isPending)
            {
                if (preguntar)
                {
                    var checkDialog = new CheckBoxDialog();
                    DialogResult rp = checkDialog.ShowDialog();
                    if (rp == DialogResult.Yes)
                    {
                        preguntar = !checkDialog.check;
                        bool isValid = false;
                        TipoOperacion = TipoOperacionABM.Modificar;
                        isValid = Actualizar();
                        if (isValid)
                        {
                            isPending = false;
                            CerrarForm();
                        }

                    }
                    else if (rp == DialogResult.No)
                    {
                        isPending = false;
                        CerrarForm();
                    }

                }
                else if (preguntar == false)
                {
                    bool isValid = false;
                    TipoOperacion = TipoOperacionABM.Modificar;
                    isValid = Actualizar();
                    if (isValid)
                    {
                        isPending = false;
                        CerrarForm();
                    }
                }
            }
            else
            {
                CerrarForm();
            }
        }

        private void btnProveedor_Click(object sender, EventArgs e)
        {
            var form = new FormBuscarProveedor();
            form.ShowDialog();
            if (form.vendor != null)
            {
                errorProv.Clear();
                SetProveedor(form.vendor);
            }
        }

        #region EventosDetalle
        private void btnAddItem_Click(object sender, EventArgs e)
        {
            AddItem();
        }

        private void dgvDetail_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Delete)
            {
                RemoveItem(DeleteDtlAction.Remove);
            }
        }

        private void btnRemoveItem_Click(object sender, EventArgs e)
        {
            RemoveItem(DeleteDtlAction.Remove);
        }

        private void btnBuscarProducto_Click(object sender, EventArgs e)
        {
            SearchAndSetItem();
        }

        private void dgvDetail_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvDetail.CurrentCell != null &&
                dgvDetail.CurrentCell.GetType() == typeof(DataGridViewCheckBoxCell))
            {
                RemoveItem(DeleteDtlAction.ActiveDesactive);
            }
        }

        private void dgvDetail_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //var itemSelected = GetItemSelected();
            //if (itemSelected.Item1 != null)
            //{
            //    var form = new FormComboItem(itemSelected.Item1);
            //    form.ShowDialog();
            //    if (form._itemEdited && form._itemVar != null)
            //    {
            //        if (EditItem(form._itemVar))
            //            CargarGridProd(details, chkMostrarInactivos.Checked);
            //        else
            //            Msg.Ok_Wng("No se pudo editar el item.");
            //    }
            //}
        }

        private void chkMostrarInactivos_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                dgvDetail.DataSourceChanged -= new EventHandler(OnContentChanged);

                CargarGridProd(details, chkMostrarInactivos.Checked);
            }
            catch (Exception ex)
            {
                Msg.Ok_Err("Ocurrió un error al intentar cargar el grid. ERROR:" + ex.Message);
            }
            finally
            {
                dgvDetail.DataSourceChanged += new EventHandler(OnContentChanged);
            }
        }
        private void dgvDetail_Paint(object sender, PaintEventArgs e)
        {
            ControlHelper.DgvSetColorBorder(sender, e);
        }

        #endregion

        #endregion

        
    }
}
