﻿namespace ConfiguradorUI.Inventario
{
    partial class FormUnidadMedida
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormUnidadMedida));
            this.tabMain = new MetroFramework.Controls.MetroTabControl();
            this.tabPagGeneral = new MetroFramework.Controls.MetroTabPage();
            this.chkUMCompra = new MetroFramework.Controls.MetroCheckBox();
            this.chkUMVenta = new MetroFramework.Controls.MetroCheckBox();
            this.cboUnidad = new ConfigUtilitarios.Controls.BorderedCombo();
            this.btnBuscarItem = new MetroFramework.Controls.MetroLink();
            this.chkActivo = new MetroFramework.Controls.MetroCheckBox();
            this.txtCodigo = new MetroFramework.Controls.MetroTextBox();
            this.txtUnidadMedidaBase = new MetroFramework.Controls.MetroTextBox();
            this.lblUnidad = new MetroFramework.Controls.MetroLabel();
            this.lblCodigo = new MetroFramework.Controls.MetroLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblUnidadMedidaBase = new MetroFramework.Controls.MetroLabel();
            this.txtCantidadBase = new MetroFramework.Controls.MetroTextBox();
            this.lblCodBarra = new MetroFramework.Controls.MetroLabel();
            this.txtFactor = new MetroFramework.Controls.MetroTextBox();
            this.lblCodigo01 = new MetroFramework.Controls.MetroLabel();
            this.txtAbrev = new MetroFramework.Controls.MetroTextBox();
            this.lblAbrev = new MetroFramework.Controls.MetroLabel();
            this.txtNombre = new MetroFramework.Controls.MetroTextBox();
            this.lblNombre = new MetroFramework.Controls.MetroLabel();
            this.panelMantenimiento = new MetroFramework.Controls.MetroPanel();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnNuevo = new System.Windows.Forms.Button();
            this.btnRollback = new System.Windows.Forms.Button();
            this.btnCommit = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.tglListarInactivos = new MetroFramework.Controls.MetroToggle();
            this.lblListarInactivos = new MetroFramework.Controls.MetroLabel();
            this.lblNombreForm = new System.Windows.Forms.Label();
            this.errorProv = new System.Windows.Forms.ErrorProvider(this.components);
            this.lblIdUnidad = new System.Windows.Forms.Label();
            this.dgvUnidadMedida = new System.Windows.Forms.DataGridView();
            this.lblNumInactivo = new System.Windows.Forms.Label();
            this.lblNumActivo = new System.Windows.Forms.Label();
            this.lblNumReg = new System.Windows.Forms.Label();
            this.txtFiltro = new MetroFramework.Controls.MetroTextBox();
            this.btnFilter = new System.Windows.Forms.Button();
            this.lblFiltro = new MetroFramework.Controls.MetroLabel();
            this.panelFiltro = new MetroFramework.Controls.MetroPanel();
            this.cboFiltro = new ConfigUtilitarios.Controls.BorderedCombo();
            this.btnCerrar = new MetroFramework.Controls.MetroLink();
            this.panel3 = new System.Windows.Forms.Panel();
            this.cboUndMedidaSNT = new ConfigUtilitarios.Controls.BorderedCombo();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.tabMain.SuspendLayout();
            this.tabPagGeneral.SuspendLayout();
            this.panelMantenimiento.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUnidadMedida)).BeginInit();
            this.panelFiltro.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabMain
            // 
            this.tabMain.Controls.Add(this.tabPagGeneral);
            this.tabMain.Location = new System.Drawing.Point(428, 102);
            this.tabMain.Name = "tabMain";
            this.tabMain.SelectedIndex = 0;
            this.tabMain.Size = new System.Drawing.Size(564, 276);
            this.tabMain.TabIndex = 1;
            this.tabMain.UseSelectable = true;
            // 
            // tabPagGeneral
            // 
            this.tabPagGeneral.Controls.Add(this.chkUMCompra);
            this.tabPagGeneral.Controls.Add(this.chkUMVenta);
            this.tabPagGeneral.Controls.Add(this.cboUnidad);
            this.tabPagGeneral.Controls.Add(this.btnBuscarItem);
            this.tabPagGeneral.Controls.Add(this.chkActivo);
            this.tabPagGeneral.Controls.Add(this.txtCodigo);
            this.tabPagGeneral.Controls.Add(this.txtUnidadMedidaBase);
            this.tabPagGeneral.Controls.Add(this.lblUnidad);
            this.tabPagGeneral.Controls.Add(this.lblCodigo);
            this.tabPagGeneral.Controls.Add(this.panel1);
            this.tabPagGeneral.Controls.Add(this.panel2);
            this.tabPagGeneral.Controls.Add(this.lblUnidadMedidaBase);
            this.tabPagGeneral.Controls.Add(this.txtCantidadBase);
            this.tabPagGeneral.Controls.Add(this.lblCodBarra);
            this.tabPagGeneral.Controls.Add(this.txtFactor);
            this.tabPagGeneral.Controls.Add(this.lblCodigo01);
            this.tabPagGeneral.Controls.Add(this.txtAbrev);
            this.tabPagGeneral.Controls.Add(this.lblAbrev);
            this.tabPagGeneral.Controls.Add(this.txtNombre);
            this.tabPagGeneral.Controls.Add(this.lblNombre);
            this.tabPagGeneral.HorizontalScrollbarBarColor = true;
            this.tabPagGeneral.HorizontalScrollbarHighlightOnWheel = false;
            this.tabPagGeneral.HorizontalScrollbarSize = 10;
            this.tabPagGeneral.Location = new System.Drawing.Point(4, 38);
            this.tabPagGeneral.Name = "tabPagGeneral";
            this.tabPagGeneral.Size = new System.Drawing.Size(556, 234);
            this.tabPagGeneral.TabIndex = 0;
            this.tabPagGeneral.Text = "General";
            this.tabPagGeneral.VerticalScrollbarBarColor = true;
            this.tabPagGeneral.VerticalScrollbarHighlightOnWheel = false;
            this.tabPagGeneral.VerticalScrollbarSize = 10;
            // 
            // chkUMCompra
            // 
            this.chkUMCompra.AutoSize = true;
            this.chkUMCompra.FontSize = MetroFramework.MetroCheckBoxSize.Medium;
            this.chkUMCompra.FontWeight = MetroFramework.MetroCheckBoxWeight.Light;
            this.chkUMCompra.ForeColor = System.Drawing.Color.Navy;
            this.chkUMCompra.Location = new System.Drawing.Point(219, 190);
            this.chkUMCompra.Name = "chkUMCompra";
            this.chkUMCompra.Size = new System.Drawing.Size(74, 19);
            this.chkUMCompra.TabIndex = 118;
            this.chkUMCompra.Text = "Compra";
            this.chkUMCompra.UseCustomForeColor = true;
            this.chkUMCompra.UseSelectable = true;
            this.chkUMCompra.CheckedChanged += new System.EventHandler(this.CambioEnControl);
            // 
            // chkUMVenta
            // 
            this.chkUMVenta.AutoSize = true;
            this.chkUMVenta.FontSize = MetroFramework.MetroCheckBoxSize.Medium;
            this.chkUMVenta.FontWeight = MetroFramework.MetroCheckBoxWeight.Light;
            this.chkUMVenta.ForeColor = System.Drawing.Color.Navy;
            this.chkUMVenta.Location = new System.Drawing.Point(113, 190);
            this.chkUMVenta.Name = "chkUMVenta";
            this.chkUMVenta.Size = new System.Drawing.Size(57, 19);
            this.chkUMVenta.TabIndex = 117;
            this.chkUMVenta.Text = "Venta";
            this.chkUMVenta.UseCustomForeColor = true;
            this.chkUMVenta.UseSelectable = true;
            this.chkUMVenta.CheckedChanged += new System.EventHandler(this.CambioEnControl);
            // 
            // cboUnidad
            // 
            this.cboUnidad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(253)))), ((int)(((byte)(255)))));
            this.cboUnidad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboUnidad.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboUnidad.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboUnidad.FormattingEnabled = true;
            this.cboUnidad.ItemHeight = 15;
            this.cboUnidad.Location = new System.Drawing.Point(353, 119);
            this.cboUnidad.Name = "cboUnidad";
            this.cboUnidad.Size = new System.Drawing.Size(146, 23);
            this.cboUnidad.TabIndex = 5;
            this.cboUnidad.SelectedValueChanged += new System.EventHandler(this.cboUnidadMedida_SelectedValueChanged);
            // 
            // btnBuscarItem
            // 
            this.btnBuscarItem.Image = ((System.Drawing.Image)(resources.GetObject("btnBuscarItem.Image")));
            this.btnBuscarItem.ImageSize = 20;
            this.btnBuscarItem.Location = new System.Drawing.Point(505, 118);
            this.btnBuscarItem.Name = "btnBuscarItem";
            this.btnBuscarItem.Size = new System.Drawing.Size(31, 24);
            this.btnBuscarItem.TabIndex = 0;
            this.btnBuscarItem.UseSelectable = true;
            this.btnBuscarItem.Click += new System.EventHandler(this.btnBuscarItem_Click);
            // 
            // chkActivo
            // 
            this.chkActivo.AutoSize = true;
            this.chkActivo.Checked = true;
            this.chkActivo.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkActivo.FontSize = MetroFramework.MetroCheckBoxSize.Medium;
            this.chkActivo.FontWeight = MetroFramework.MetroCheckBoxWeight.Light;
            this.chkActivo.ForeColor = System.Drawing.Color.Navy;
            this.chkActivo.Location = new System.Drawing.Point(3, 190);
            this.chkActivo.Name = "chkActivo";
            this.chkActivo.Size = new System.Drawing.Size(61, 19);
            this.chkActivo.TabIndex = 8;
            this.chkActivo.Text = "Activo";
            this.chkActivo.UseCustomForeColor = true;
            this.chkActivo.UseSelectable = true;
            this.chkActivo.CheckedChanged += new System.EventHandler(this.CambioEnControl);
            // 
            // txtCodigo
            // 
            this.txtCodigo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(253)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.txtCodigo.CustomButton.Image = null;
            this.txtCodigo.CustomButton.Location = new System.Drawing.Point(162, 1);
            this.txtCodigo.CustomButton.Name = "";
            this.txtCodigo.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtCodigo.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtCodigo.CustomButton.TabIndex = 1;
            this.txtCodigo.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtCodigo.CustomButton.UseSelectable = true;
            this.txtCodigo.CustomButton.Visible = false;
            this.txtCodigo.Lines = new string[0];
            this.txtCodigo.Location = new System.Drawing.Point(353, 11);
            this.txtCodigo.MaxLength = 32767;
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.PasswordChar = '\0';
            this.txtCodigo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCodigo.SelectedText = "";
            this.txtCodigo.SelectionLength = 0;
            this.txtCodigo.SelectionStart = 0;
            this.txtCodigo.ShortcutsEnabled = true;
            this.txtCodigo.Size = new System.Drawing.Size(184, 23);
            this.txtCodigo.TabIndex = 2;
            this.txtCodigo.UseCustomBackColor = true;
            this.txtCodigo.UseSelectable = true;
            this.txtCodigo.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtCodigo.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtCodigo.TextChanged += new System.EventHandler(this.CambioEnControl);
            // 
            // txtUnidadMedidaBase
            // 
            this.txtUnidadMedidaBase.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(253)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.txtUnidadMedidaBase.CustomButton.Image = null;
            this.txtUnidadMedidaBase.CustomButton.Location = new System.Drawing.Point(162, 1);
            this.txtUnidadMedidaBase.CustomButton.Name = "";
            this.txtUnidadMedidaBase.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtUnidadMedidaBase.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtUnidadMedidaBase.CustomButton.TabIndex = 1;
            this.txtUnidadMedidaBase.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtUnidadMedidaBase.CustomButton.UseSelectable = true;
            this.txtUnidadMedidaBase.CustomButton.Visible = false;
            this.txtUnidadMedidaBase.Enabled = false;
            this.txtUnidadMedidaBase.Lines = new string[0];
            this.txtUnidadMedidaBase.Location = new System.Drawing.Point(353, 155);
            this.txtUnidadMedidaBase.MaxLength = 32767;
            this.txtUnidadMedidaBase.Name = "txtUnidadMedidaBase";
            this.txtUnidadMedidaBase.PasswordChar = '\0';
            this.txtUnidadMedidaBase.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtUnidadMedidaBase.SelectedText = "";
            this.txtUnidadMedidaBase.SelectionLength = 0;
            this.txtUnidadMedidaBase.SelectionStart = 0;
            this.txtUnidadMedidaBase.ShortcutsEnabled = true;
            this.txtUnidadMedidaBase.Size = new System.Drawing.Size(184, 23);
            this.txtUnidadMedidaBase.TabIndex = 7;
            this.txtUnidadMedidaBase.UseCustomBackColor = true;
            this.txtUnidadMedidaBase.UseSelectable = true;
            this.txtUnidadMedidaBase.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtUnidadMedidaBase.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // lblUnidad
            // 
            this.lblUnidad.AutoSize = true;
            this.lblUnidad.ForeColor = System.Drawing.Color.Navy;
            this.lblUnidad.Location = new System.Drawing.Point(266, 121);
            this.lblUnidad.Name = "lblUnidad";
            this.lblUnidad.Size = new System.Drawing.Size(54, 19);
            this.lblUnidad.TabIndex = 61;
            this.lblUnidad.Text = "Unidad:";
            this.lblUnidad.UseCustomForeColor = true;
            // 
            // lblCodigo
            // 
            this.lblCodigo.AutoSize = true;
            this.lblCodigo.ForeColor = System.Drawing.Color.Navy;
            this.lblCodigo.Location = new System.Drawing.Point(266, 13);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.Size = new System.Drawing.Size(53, 19);
            this.lblCodigo.TabIndex = 8;
            this.lblCodigo.Text = "Código";
            this.lblCodigo.UseCustomForeColor = true;
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::ConfiguradorUI.Properties.Resources.linea_celeste;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Location = new System.Drawing.Point(3, 223);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(530, 8);
            this.panel1.TabIndex = 57;
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::ConfiguradorUI.Properties.Resources.linea_celeste;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Location = new System.Drawing.Point(3, 88);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(530, 8);
            this.panel2.TabIndex = 57;
            // 
            // lblUnidadMedidaBase
            // 
            this.lblUnidadMedidaBase.AutoSize = true;
            this.lblUnidadMedidaBase.ForeColor = System.Drawing.Color.Navy;
            this.lblUnidadMedidaBase.Location = new System.Drawing.Point(266, 159);
            this.lblUnidadMedidaBase.Name = "lblUnidadMedidaBase";
            this.lblUnidadMedidaBase.Size = new System.Drawing.Size(85, 19);
            this.lblUnidadMedidaBase.TabIndex = 8;
            this.lblUnidadMedidaBase.Text = "Unidad Base:";
            this.lblUnidadMedidaBase.UseCustomForeColor = true;
            // 
            // txtCantidadBase
            // 
            this.txtCantidadBase.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(253)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.txtCantidadBase.CustomButton.Image = null;
            this.txtCantidadBase.CustomButton.Location = new System.Drawing.Point(146, 1);
            this.txtCantidadBase.CustomButton.Name = "";
            this.txtCantidadBase.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtCantidadBase.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtCantidadBase.CustomButton.TabIndex = 1;
            this.txtCantidadBase.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtCantidadBase.CustomButton.UseSelectable = true;
            this.txtCantidadBase.CustomButton.Visible = false;
            this.txtCantidadBase.Enabled = false;
            this.txtCantidadBase.Lines = new string[0];
            this.txtCantidadBase.Location = new System.Drawing.Point(79, 155);
            this.txtCantidadBase.MaxLength = 32767;
            this.txtCantidadBase.Name = "txtCantidadBase";
            this.txtCantidadBase.PasswordChar = '\0';
            this.txtCantidadBase.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCantidadBase.SelectedText = "";
            this.txtCantidadBase.SelectionLength = 0;
            this.txtCantidadBase.SelectionStart = 0;
            this.txtCantidadBase.ShortcutsEnabled = true;
            this.txtCantidadBase.Size = new System.Drawing.Size(168, 23);
            this.txtCantidadBase.TabIndex = 6;
            this.txtCantidadBase.UseCustomBackColor = true;
            this.txtCantidadBase.UseSelectable = true;
            this.txtCantidadBase.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtCantidadBase.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtCantidadBase.TextChanged += new System.EventHandler(this.CambioEnControl);
            // 
            // lblCodBarra
            // 
            this.lblCodBarra.AutoSize = true;
            this.lblCodBarra.ForeColor = System.Drawing.Color.Navy;
            this.lblCodBarra.Location = new System.Drawing.Point(-1, 157);
            this.lblCodBarra.Name = "lblCodBarra";
            this.lblCodBarra.Size = new System.Drawing.Size(73, 19);
            this.lblCodBarra.TabIndex = 6;
            this.lblCodBarra.Text = "Cant. Base:";
            this.lblCodBarra.UseCustomForeColor = true;
            // 
            // txtFactor
            // 
            this.txtFactor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(253)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.txtFactor.CustomButton.Image = null;
            this.txtFactor.CustomButton.Location = new System.Drawing.Point(146, 1);
            this.txtFactor.CustomButton.Name = "";
            this.txtFactor.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtFactor.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtFactor.CustomButton.TabIndex = 1;
            this.txtFactor.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtFactor.CustomButton.UseSelectable = true;
            this.txtFactor.CustomButton.Visible = false;
            this.txtFactor.Lines = new string[0];
            this.txtFactor.Location = new System.Drawing.Point(79, 119);
            this.txtFactor.MaxLength = 32767;
            this.txtFactor.Name = "txtFactor";
            this.txtFactor.PasswordChar = '\0';
            this.txtFactor.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtFactor.SelectedText = "";
            this.txtFactor.SelectionLength = 0;
            this.txtFactor.SelectionStart = 0;
            this.txtFactor.ShortcutsEnabled = true;
            this.txtFactor.Size = new System.Drawing.Size(168, 23);
            this.txtFactor.TabIndex = 4;
            this.txtFactor.UseCustomBackColor = true;
            this.txtFactor.UseSelectable = true;
            this.txtFactor.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtFactor.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtFactor.TextChanged += new System.EventHandler(this.txtCantidad_TextChanged);
            this.txtFactor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ValidarTxtDecimal);
            // 
            // lblCodigo01
            // 
            this.lblCodigo01.AutoSize = true;
            this.lblCodigo01.ForeColor = System.Drawing.Color.Navy;
            this.lblCodigo01.Location = new System.Drawing.Point(1, 121);
            this.lblCodigo01.Name = "lblCodigo01";
            this.lblCodigo01.Size = new System.Drawing.Size(49, 19);
            this.lblCodigo01.TabIndex = 2;
            this.lblCodigo01.Text = "Factor:";
            this.lblCodigo01.UseCustomForeColor = true;
            // 
            // txtAbrev
            // 
            this.txtAbrev.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(253)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.txtAbrev.CustomButton.Image = null;
            this.txtAbrev.CustomButton.Location = new System.Drawing.Point(146, 1);
            this.txtAbrev.CustomButton.Name = "";
            this.txtAbrev.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtAbrev.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtAbrev.CustomButton.TabIndex = 1;
            this.txtAbrev.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtAbrev.CustomButton.UseSelectable = true;
            this.txtAbrev.CustomButton.Visible = false;
            this.txtAbrev.Lines = new string[0];
            this.txtAbrev.Location = new System.Drawing.Point(78, 48);
            this.txtAbrev.MaxLength = 32767;
            this.txtAbrev.Name = "txtAbrev";
            this.txtAbrev.PasswordChar = '\0';
            this.txtAbrev.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtAbrev.SelectedText = "";
            this.txtAbrev.SelectionLength = 0;
            this.txtAbrev.SelectionStart = 0;
            this.txtAbrev.ShortcutsEnabled = true;
            this.txtAbrev.Size = new System.Drawing.Size(168, 23);
            this.txtAbrev.TabIndex = 3;
            this.txtAbrev.UseCustomBackColor = true;
            this.txtAbrev.UseSelectable = true;
            this.txtAbrev.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtAbrev.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtAbrev.TextChanged += new System.EventHandler(this.CambioEnControl);
            // 
            // lblAbrev
            // 
            this.lblAbrev.AutoSize = true;
            this.lblAbrev.ForeColor = System.Drawing.Color.Navy;
            this.lblAbrev.Location = new System.Drawing.Point(0, 50);
            this.lblAbrev.Name = "lblAbrev";
            this.lblAbrev.Size = new System.Drawing.Size(77, 19);
            this.lblAbrev.TabIndex = 0;
            this.lblAbrev.Text = "Abreviatura";
            this.lblAbrev.UseCustomForeColor = true;
            // 
            // txtNombre
            // 
            this.txtNombre.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(253)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.txtNombre.CustomButton.Image = null;
            this.txtNombre.CustomButton.Location = new System.Drawing.Point(146, 1);
            this.txtNombre.CustomButton.Name = "";
            this.txtNombre.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtNombre.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtNombre.CustomButton.TabIndex = 1;
            this.txtNombre.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtNombre.CustomButton.UseSelectable = true;
            this.txtNombre.CustomButton.Visible = false;
            this.txtNombre.Lines = new string[0];
            this.txtNombre.Location = new System.Drawing.Point(79, 11);
            this.txtNombre.MaxLength = 32767;
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.PasswordChar = '\0';
            this.txtNombre.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNombre.SelectedText = "";
            this.txtNombre.SelectionLength = 0;
            this.txtNombre.SelectionStart = 0;
            this.txtNombre.ShortcutsEnabled = true;
            this.txtNombre.Size = new System.Drawing.Size(168, 23);
            this.txtNombre.TabIndex = 1;
            this.txtNombre.UseCustomBackColor = true;
            this.txtNombre.UseSelectable = true;
            this.txtNombre.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtNombre.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtNombre.TextChanged += new System.EventHandler(this.CambioEnControl);
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.ForeColor = System.Drawing.Color.Navy;
            this.lblNombre.Location = new System.Drawing.Point(1, 13);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(59, 19);
            this.lblNombre.TabIndex = 0;
            this.lblNombre.Text = "Nombre";
            this.lblNombre.UseCustomForeColor = true;
            // 
            // panelMantenimiento
            // 
            this.panelMantenimiento.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(253)))), ((int)(((byte)(255)))));
            this.panelMantenimiento.Controls.Add(this.btnSearch);
            this.panelMantenimiento.Controls.Add(this.btnNuevo);
            this.panelMantenimiento.Controls.Add(this.btnRollback);
            this.panelMantenimiento.Controls.Add(this.btnCommit);
            this.panelMantenimiento.Controls.Add(this.btnDelete);
            this.panelMantenimiento.HorizontalScrollbarBarColor = true;
            this.panelMantenimiento.HorizontalScrollbarHighlightOnWheel = false;
            this.panelMantenimiento.HorizontalScrollbarSize = 10;
            this.panelMantenimiento.Location = new System.Drawing.Point(428, 43);
            this.panelMantenimiento.Name = "panelMantenimiento";
            this.panelMantenimiento.Size = new System.Drawing.Size(547, 49);
            this.panelMantenimiento.Style = MetroFramework.MetroColorStyle.Green;
            this.panelMantenimiento.TabIndex = 2;
            this.panelMantenimiento.UseCustomBackColor = true;
            this.panelMantenimiento.UseStyleColors = true;
            this.panelMantenimiento.VerticalScrollbarBarColor = true;
            this.panelMantenimiento.VerticalScrollbarHighlightOnWheel = false;
            this.panelMantenimiento.VerticalScrollbarSize = 10;
            // 
            // btnSearch
            // 
            this.btnSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.btnSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnSearch.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.btnSearch.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.Location = new System.Drawing.Point(3, 5);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(40, 40);
            this.btnSearch.TabIndex = 4;
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnNuevo
            // 
            this.btnNuevo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.btnNuevo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnNuevo.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.btnNuevo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.btnNuevo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNuevo.Font = new System.Drawing.Font("Microsoft Sans Serif", 1.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNuevo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnNuevo.Image = ((System.Drawing.Image)(resources.GetObject("btnNuevo.Image")));
            this.btnNuevo.Location = new System.Drawing.Point(375, 5);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(40, 40);
            this.btnNuevo.TabIndex = 0;
            this.btnNuevo.UseVisualStyleBackColor = false;
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // btnRollback
            // 
            this.btnRollback.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.btnRollback.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnRollback.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.btnRollback.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.btnRollback.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRollback.Image = ((System.Drawing.Image)(resources.GetObject("btnRollback.Image")));
            this.btnRollback.Location = new System.Drawing.Point(501, 5);
            this.btnRollback.Name = "btnRollback";
            this.btnRollback.Size = new System.Drawing.Size(40, 40);
            this.btnRollback.TabIndex = 3;
            this.btnRollback.UseVisualStyleBackColor = false;
            this.btnRollback.Click += new System.EventHandler(this.btnRollback_Click);
            // 
            // btnCommit
            // 
            this.btnCommit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.btnCommit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnCommit.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.btnCommit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.btnCommit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCommit.Image = ((System.Drawing.Image)(resources.GetObject("btnCommit.Image")));
            this.btnCommit.Location = new System.Drawing.Point(459, 5);
            this.btnCommit.Name = "btnCommit";
            this.btnCommit.Size = new System.Drawing.Size(40, 40);
            this.btnCommit.TabIndex = 0;
            this.btnCommit.UseVisualStyleBackColor = false;
            this.btnCommit.Click += new System.EventHandler(this.btnCommit_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnDelete.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.btnDelete.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.Location = new System.Drawing.Point(417, 5);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(40, 40);
            this.btnDelete.TabIndex = 0;
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // tglListarInactivos
            // 
            this.tglListarInactivos.AutoSize = true;
            this.tglListarInactivos.Location = new System.Drawing.Point(137, 117);
            this.tglListarInactivos.Name = "tglListarInactivos";
            this.tglListarInactivos.Size = new System.Drawing.Size(80, 17);
            this.tglListarInactivos.TabIndex = 5;
            this.tglListarInactivos.Text = "Off";
            this.tglListarInactivos.UseSelectable = true;
            this.tglListarInactivos.Click += new System.EventHandler(this.tglListarInactivos_Click);
            // 
            // lblListarInactivos
            // 
            this.lblListarInactivos.AutoSize = true;
            this.lblListarInactivos.Location = new System.Drawing.Point(39, 115);
            this.lblListarInactivos.Name = "lblListarInactivos";
            this.lblListarInactivos.Size = new System.Drawing.Size(92, 19);
            this.lblListarInactivos.TabIndex = 4;
            this.lblListarInactivos.Text = "Listar inactivos";
            // 
            // lblNombreForm
            // 
            this.lblNombreForm.AutoSize = true;
            this.lblNombreForm.Font = new System.Drawing.Font("Segoe UI Semilight", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombreForm.ForeColor = System.Drawing.SystemColors.Highlight;
            this.lblNombreForm.Location = new System.Drawing.Point(100, 43);
            this.lblNombreForm.Name = "lblNombreForm";
            this.lblNombreForm.Size = new System.Drawing.Size(228, 32);
            this.lblNombreForm.TabIndex = 50;
            this.lblNombreForm.Text = "Unidades de medida";
            // 
            // errorProv
            // 
            this.errorProv.ContainerControl = this;
            // 
            // lblIdUnidad
            // 
            this.lblIdUnidad.AutoSize = true;
            this.lblIdUnidad.Location = new System.Drawing.Point(503, 39);
            this.lblIdUnidad.Name = "lblIdUnidad";
            this.lblIdUnidad.Size = new System.Drawing.Size(0, 13);
            this.lblIdUnidad.TabIndex = 51;
            // 
            // dgvUnidadMedida
            // 
            this.dgvUnidadMedida.AllowUserToAddRows = false;
            this.dgvUnidadMedida.AllowUserToResizeColumns = false;
            this.dgvUnidadMedida.AllowUserToResizeRows = false;
            this.dgvUnidadMedida.BackgroundColor = System.Drawing.Color.White;
            this.dgvUnidadMedida.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvUnidadMedida.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvUnidadMedida.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvUnidadMedida.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvUnidadMedida.Location = new System.Drawing.Point(40, 140);
            this.dgvUnidadMedida.MultiSelect = false;
            this.dgvUnidadMedida.Name = "dgvUnidadMedida";
            this.dgvUnidadMedida.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvUnidadMedida.RowHeadersVisible = false;
            this.dgvUnidadMedida.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvUnidadMedida.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvUnidadMedida.Size = new System.Drawing.Size(349, 258);
            this.dgvUnidadMedida.TabIndex = 0;
            this.dgvUnidadMedida.SelectionChanged += new System.EventHandler(this.dgvUnidadMedida_SelectionChanged);
            // 
            // lblNumInactivo
            // 
            this.lblNumInactivo.AutoSize = true;
            this.lblNumInactivo.BackColor = System.Drawing.Color.Transparent;
            this.lblNumInactivo.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumInactivo.ForeColor = System.Drawing.Color.Red;
            this.lblNumInactivo.Location = new System.Drawing.Point(223, 412);
            this.lblNumInactivo.MaximumSize = new System.Drawing.Size(550, 1500);
            this.lblNumInactivo.Name = "lblNumInactivo";
            this.lblNumInactivo.Size = new System.Drawing.Size(65, 17);
            this.lblNumInactivo.TabIndex = 116;
            this.lblNumInactivo.Text = "Inactivos: ";
            // 
            // lblNumActivo
            // 
            this.lblNumActivo.AutoSize = true;
            this.lblNumActivo.BackColor = System.Drawing.Color.Transparent;
            this.lblNumActivo.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumActivo.ForeColor = System.Drawing.Color.LimeGreen;
            this.lblNumActivo.Location = new System.Drawing.Point(143, 412);
            this.lblNumActivo.MaximumSize = new System.Drawing.Size(550, 1500);
            this.lblNumActivo.Name = "lblNumActivo";
            this.lblNumActivo.Size = new System.Drawing.Size(56, 17);
            this.lblNumActivo.TabIndex = 115;
            this.lblNumActivo.Text = "Activos: ";
            // 
            // lblNumReg
            // 
            this.lblNumReg.AutoSize = true;
            this.lblNumReg.BackColor = System.Drawing.Color.Transparent;
            this.lblNumReg.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumReg.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.lblNumReg.Location = new System.Drawing.Point(76, 412);
            this.lblNumReg.MaximumSize = new System.Drawing.Size(550, 1500);
            this.lblNumReg.Name = "lblNumReg";
            this.lblNumReg.Size = new System.Drawing.Size(44, 17);
            this.lblNumReg.TabIndex = 114;
            this.lblNumReg.Text = "Total: ";
            // 
            // txtFiltro
            // 
            // 
            // 
            // 
            this.txtFiltro.CustomButton.Image = null;
            this.txtFiltro.CustomButton.Location = new System.Drawing.Point(183, 1);
            this.txtFiltro.CustomButton.Name = "";
            this.txtFiltro.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtFiltro.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtFiltro.CustomButton.TabIndex = 1;
            this.txtFiltro.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtFiltro.CustomButton.UseSelectable = true;
            this.txtFiltro.CustomButton.Visible = false;
            this.txtFiltro.Lines = new string[0];
            this.txtFiltro.Location = new System.Drawing.Point(291, 11);
            this.txtFiltro.MaxLength = 32767;
            this.txtFiltro.Name = "txtFiltro";
            this.txtFiltro.PasswordChar = '\0';
            this.txtFiltro.PromptText = "Filtro";
            this.txtFiltro.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtFiltro.SelectedText = "";
            this.txtFiltro.SelectionLength = 0;
            this.txtFiltro.SelectionStart = 0;
            this.txtFiltro.ShortcutsEnabled = true;
            this.txtFiltro.Size = new System.Drawing.Size(205, 23);
            this.txtFiltro.TabIndex = 10;
            this.txtFiltro.UseSelectable = true;
            this.txtFiltro.WaterMark = "Filtro";
            this.txtFiltro.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtFiltro.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtFiltro.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFiltro_KeyPress);
            // 
            // btnFilter
            // 
            this.btnFilter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.btnFilter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnFilter.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.btnFilter.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gainsboro;
            this.btnFilter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFilter.Image = ((System.Drawing.Image)(resources.GetObject("btnFilter.Image")));
            this.btnFilter.Location = new System.Drawing.Point(502, 6);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(37, 30);
            this.btnFilter.TabIndex = 11;
            this.btnFilter.UseVisualStyleBackColor = false;
            this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
            // 
            // lblFiltro
            // 
            this.lblFiltro.AutoSize = true;
            this.lblFiltro.BackColor = System.Drawing.Color.Transparent;
            this.lblFiltro.ForeColor = System.Drawing.Color.Navy;
            this.lblFiltro.Location = new System.Drawing.Point(13, 13);
            this.lblFiltro.Name = "lblFiltro";
            this.lblFiltro.Size = new System.Drawing.Size(76, 19);
            this.lblFiltro.TabIndex = 2;
            this.lblFiltro.Text = "Buscar por:";
            this.lblFiltro.UseCustomBackColor = true;
            this.lblFiltro.UseCustomForeColor = true;
            // 
            // panelFiltro
            // 
            this.panelFiltro.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(253)))), ((int)(((byte)(255)))));
            this.panelFiltro.Controls.Add(this.cboFiltro);
            this.panelFiltro.Controls.Add(this.lblFiltro);
            this.panelFiltro.Controls.Add(this.btnFilter);
            this.panelFiltro.Controls.Add(this.txtFiltro);
            this.panelFiltro.HorizontalScrollbarBarColor = true;
            this.panelFiltro.HorizontalScrollbarHighlightOnWheel = false;
            this.panelFiltro.HorizontalScrollbarSize = 10;
            this.panelFiltro.Location = new System.Drawing.Point(431, 431);
            this.panelFiltro.Name = "panelFiltro";
            this.panelFiltro.Size = new System.Drawing.Size(547, 44);
            this.panelFiltro.TabIndex = 3;
            this.panelFiltro.UseCustomBackColor = true;
            this.panelFiltro.VerticalScrollbarBarColor = true;
            this.panelFiltro.VerticalScrollbarHighlightOnWheel = false;
            this.panelFiltro.VerticalScrollbarSize = 10;
            // 
            // cboFiltro
            // 
            this.cboFiltro.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboFiltro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboFiltro.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboFiltro.FormattingEnabled = true;
            this.cboFiltro.Location = new System.Drawing.Point(95, 11);
            this.cboFiltro.Name = "cboFiltro";
            this.cboFiltro.Size = new System.Drawing.Size(172, 23);
            this.cboFiltro.TabIndex = 9;
            // 
            // btnCerrar
            // 
            this.btnCerrar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCerrar.Image = ((System.Drawing.Image)(resources.GetObject("btnCerrar.Image")));
            this.btnCerrar.ImageSize = 48;
            this.btnCerrar.Location = new System.Drawing.Point(38, 32);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(56, 57);
            this.btnCerrar.TabIndex = 6;
            this.btnCerrar.UseSelectable = true;
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // panel3
            // 
            this.panel3.BackgroundImage = global::ConfiguradorUI.Properties.Resources.linea_celeste;
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel3.Location = new System.Drawing.Point(435, 417);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(530, 8);
            this.panel3.TabIndex = 58;
            // 
            // cboUndMedidaSNT
            // 
            this.cboUndMedidaSNT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(253)))), ((int)(((byte)(255)))));
            this.cboUndMedidaSNT.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboUndMedidaSNT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboUndMedidaSNT.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboUndMedidaSNT.FormattingEnabled = true;
            this.cboUndMedidaSNT.ItemHeight = 15;
            this.cboUndMedidaSNT.Location = new System.Drawing.Point(578, 384);
            this.cboUndMedidaSNT.Name = "cboUndMedidaSNT";
            this.cboUndMedidaSNT.Size = new System.Drawing.Size(387, 23);
            this.cboUndMedidaSNT.TabIndex = 117;
            this.cboUndMedidaSNT.SelectedIndexChanged += new System.EventHandler(this.CambioEnControl);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.ForeColor = System.Drawing.Color.Navy;
            this.metroLabel1.Location = new System.Drawing.Point(435, 388);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(132, 19);
            this.metroLabel1.TabIndex = 118;
            this.metroLabel1.Text = "Und. Medida SUNAT:";
            this.metroLabel1.UseCustomForeColor = true;
            // 
            // FormUnidadMedida
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCerrar;
            this.ClientSize = new System.Drawing.Size(1030, 487);
            this.Controls.Add(this.cboUndMedidaSNT);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.lblNumInactivo);
            this.Controls.Add(this.lblNumActivo);
            this.Controls.Add(this.lblNumReg);
            this.Controls.Add(this.dgvUnidadMedida);
            this.Controls.Add(this.lblIdUnidad);
            this.Controls.Add(this.lblNombreForm);
            this.Controls.Add(this.btnCerrar);
            this.Controls.Add(this.panelFiltro);
            this.Controls.Add(this.panelMantenimiento);
            this.Controls.Add(this.tabMain);
            this.Controls.Add(this.tglListarInactivos);
            this.Controls.Add(this.lblListarInactivos);
            this.MaximizeBox = false;
            this.Name = "FormUnidadMedida";
            this.Resizable = false;
            this.Style = MetroFramework.MetroColorStyle.Default;
            this.Load += new System.EventHandler(this.FormProducto_Load);
            this.tabMain.ResumeLayout(false);
            this.tabPagGeneral.ResumeLayout(false);
            this.tabPagGeneral.PerformLayout();
            this.panelMantenimiento.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUnidadMedida)).EndInit();
            this.panelFiltro.ResumeLayout(false);
            this.panelFiltro.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MetroFramework.Controls.MetroTabControl tabMain;
        private MetroFramework.Controls.MetroTabPage tabPagGeneral;
        private MetroFramework.Controls.MetroLabel lblNombre;
        private MetroFramework.Controls.MetroTextBox txtNombre;
        private MetroFramework.Controls.MetroTextBox txtCantidadBase;
        private MetroFramework.Controls.MetroLabel lblCodBarra;
        private MetroFramework.Controls.MetroTextBox txtFactor;
        private MetroFramework.Controls.MetroLabel lblCodigo01;
        private MetroFramework.Controls.MetroPanel panelMantenimiento;
        private MetroFramework.Controls.MetroLink btnCerrar;
        private System.Windows.Forms.Label lblNombreForm;
        private MetroFramework.Controls.MetroToggle tglListarInactivos;
        private MetroFramework.Controls.MetroLabel lblListarInactivos;
        private System.Windows.Forms.Button btnNuevo;
        private System.Windows.Forms.Button btnRollback;
        private System.Windows.Forms.Button btnCommit;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnSearch;
        private MetroFramework.Controls.MetroLabel lblUnidadMedidaBase;
        private System.Windows.Forms.ErrorProvider errorProv;
        private System.Windows.Forms.Label lblIdUnidad;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dgvUnidadMedida;
        private System.Windows.Forms.Label lblNumInactivo;
        private System.Windows.Forms.Label lblNumActivo;
        private System.Windows.Forms.Label lblNumReg;
        private MetroFramework.Controls.MetroLabel lblUnidad;
        private MetroFramework.Controls.MetroPanel panelFiltro;
        private MetroFramework.Controls.MetroLabel lblFiltro;
        private System.Windows.Forms.Button btnFilter;
        private MetroFramework.Controls.MetroTextBox txtFiltro;
        private MetroFramework.Controls.MetroTextBox txtUnidadMedidaBase;
        private MetroFramework.Controls.MetroCheckBox chkActivo;
        private MetroFramework.Controls.MetroLink btnBuscarItem;
        private ConfigUtilitarios.Controls.BorderedCombo cboFiltro;
        private ConfigUtilitarios.Controls.BorderedCombo cboUnidad;
        private MetroFramework.Controls.MetroTextBox txtCodigo;
        private MetroFramework.Controls.MetroLabel lblCodigo;
        private System.Windows.Forms.Panel panel2;
        private MetroFramework.Controls.MetroTextBox txtAbrev;
        private MetroFramework.Controls.MetroLabel lblAbrev;
        private MetroFramework.Controls.MetroCheckBox chkUMCompra;
        private MetroFramework.Controls.MetroCheckBox chkUMVenta;
        private ConfigUtilitarios.Controls.BorderedCombo cboUndMedidaSNT;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private System.Windows.Forms.Panel panel3;
    }
}