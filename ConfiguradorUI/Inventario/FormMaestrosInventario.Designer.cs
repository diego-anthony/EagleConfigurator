﻿namespace ConfiguradorUI.Inventario
{
    partial class FormMaestrosInventario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMaestrosInventario));
            this.lblNombreForm = new System.Windows.Forms.Label();
            this.btnCerrar = new MetroFramework.Controls.MetroLink();
            this.BtnUnidades = new System.Windows.Forms.Button();
            this.metroLink1 = new MetroFramework.Controls.MetroLink();
            this.btnCatalogProv = new System.Windows.Forms.Button();
            this.btnitems = new System.Windows.Forms.Button();
            this.btnNumeracion = new System.Windows.Forms.Button();
            this.btnCostCenter = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblNombreForm
            // 
            this.lblNombreForm.AutoSize = true;
            this.lblNombreForm.Font = new System.Drawing.Font("Segoe UI Semilight", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombreForm.ForeColor = System.Drawing.SystemColors.Highlight;
            this.lblNombreForm.Location = new System.Drawing.Point(85, 40);
            this.lblNombreForm.Name = "lblNombreForm";
            this.lblNombreForm.Size = new System.Drawing.Size(219, 32);
            this.lblNombreForm.TabIndex = 52;
            this.lblNombreForm.Text = "Maestros Inventario";
            // 
            // btnCerrar
            // 
            this.btnCerrar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCerrar.ImageSize = 48;
            this.btnCerrar.Location = new System.Drawing.Point(23, 29);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(56, 57);
            this.btnCerrar.TabIndex = 51;
            this.btnCerrar.UseSelectable = true;
            // 
            // BtnUnidades
            // 
            this.BtnUnidades.BackColor = System.Drawing.Color.DodgerBlue;
            this.BtnUnidades.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.BtnUnidades.FlatAppearance.BorderSize = 0;
            this.BtnUnidades.FlatAppearance.MouseDownBackColor = System.Drawing.Color.SteelBlue;
            this.BtnUnidades.FlatAppearance.MouseOverBackColor = System.Drawing.Color.MidnightBlue;
            this.BtnUnidades.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnUnidades.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnUnidades.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.BtnUnidades.Image = ((System.Drawing.Image)(resources.GetObject("BtnUnidades.Image")));
            this.BtnUnidades.Location = new System.Drawing.Point(45, 114);
            this.BtnUnidades.Margin = new System.Windows.Forms.Padding(2);
            this.BtnUnidades.Name = "BtnUnidades";
            this.BtnUnidades.Size = new System.Drawing.Size(145, 37);
            this.BtnUnidades.TabIndex = 30;
            this.BtnUnidades.Text = "Unidades";
            this.BtnUnidades.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.BtnUnidades.UseVisualStyleBackColor = false;
            this.BtnUnidades.Click += new System.EventHandler(this.BtnUnidades_Click);
            // 
            // metroLink1
            // 
            this.metroLink1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.metroLink1.Image = ((System.Drawing.Image)(resources.GetObject("metroLink1.Image")));
            this.metroLink1.ImageSize = 48;
            this.metroLink1.Location = new System.Drawing.Point(23, 29);
            this.metroLink1.Name = "metroLink1";
            this.metroLink1.Size = new System.Drawing.Size(56, 57);
            this.metroLink1.TabIndex = 54;
            this.metroLink1.UseSelectable = true;
            // 
            // btnCatalogProv
            // 
            this.btnCatalogProv.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnCatalogProv.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnCatalogProv.FlatAppearance.BorderSize = 0;
            this.btnCatalogProv.FlatAppearance.MouseDownBackColor = System.Drawing.Color.SteelBlue;
            this.btnCatalogProv.FlatAppearance.MouseOverBackColor = System.Drawing.Color.MidnightBlue;
            this.btnCatalogProv.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCatalogProv.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCatalogProv.ForeColor = System.Drawing.Color.White;
            this.btnCatalogProv.Image = ((System.Drawing.Image)(resources.GetObject("btnCatalogProv.Image")));
            this.btnCatalogProv.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCatalogProv.Location = new System.Drawing.Point(45, 154);
            this.btnCatalogProv.Margin = new System.Windows.Forms.Padding(2);
            this.btnCatalogProv.Name = "btnCatalogProv";
            this.btnCatalogProv.Size = new System.Drawing.Size(145, 37);
            this.btnCatalogProv.TabIndex = 56;
            this.btnCatalogProv.Text = "Catálog. prov.";
            this.btnCatalogProv.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCatalogProv.UseVisualStyleBackColor = false;
            this.btnCatalogProv.Click += new System.EventHandler(this.BtnCatalogProv_Click);
            // 
            // btnitems
            // 
            this.btnitems.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnitems.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnitems.FlatAppearance.BorderSize = 0;
            this.btnitems.FlatAppearance.MouseDownBackColor = System.Drawing.Color.SteelBlue;
            this.btnitems.FlatAppearance.MouseOverBackColor = System.Drawing.Color.MidnightBlue;
            this.btnitems.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnitems.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnitems.ForeColor = System.Drawing.Color.White;
            this.btnitems.Image = ((System.Drawing.Image)(resources.GetObject("btnitems.Image")));
            this.btnitems.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnitems.Location = new System.Drawing.Point(45, 194);
            this.btnitems.Margin = new System.Windows.Forms.Padding(2);
            this.btnitems.Name = "btnitems";
            this.btnitems.Size = new System.Drawing.Size(145, 37);
            this.btnitems.TabIndex = 56;
            this.btnitems.Text = "Items";
            this.btnitems.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnitems.UseVisualStyleBackColor = false;
            this.btnitems.Click += new System.EventHandler(this.btnitems_Click);
            // 
            // btnNumeracion
            // 
            this.btnNumeracion.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnNumeracion.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnNumeracion.FlatAppearance.BorderSize = 0;
            this.btnNumeracion.FlatAppearance.MouseDownBackColor = System.Drawing.Color.SteelBlue;
            this.btnNumeracion.FlatAppearance.MouseOverBackColor = System.Drawing.Color.MidnightBlue;
            this.btnNumeracion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNumeracion.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNumeracion.ForeColor = System.Drawing.Color.White;
            this.btnNumeracion.Image = ((System.Drawing.Image)(resources.GetObject("btnNumeracion.Image")));
            this.btnNumeracion.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNumeracion.Location = new System.Drawing.Point(211, 114);
            this.btnNumeracion.Margin = new System.Windows.Forms.Padding(2);
            this.btnNumeracion.Name = "btnNumeracion";
            this.btnNumeracion.Size = new System.Drawing.Size(145, 37);
            this.btnNumeracion.TabIndex = 57;
            this.btnNumeracion.Text = "Numeración";
            this.btnNumeracion.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnNumeracion.UseVisualStyleBackColor = false;
            this.btnNumeracion.Click += new System.EventHandler(this.btnNumeracion_Click);
            // 
            // btnCostCenter
            // 
            this.btnCostCenter.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnCostCenter.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnCostCenter.FlatAppearance.BorderSize = 0;
            this.btnCostCenter.FlatAppearance.MouseDownBackColor = System.Drawing.Color.SteelBlue;
            this.btnCostCenter.FlatAppearance.MouseOverBackColor = System.Drawing.Color.MidnightBlue;
            this.btnCostCenter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCostCenter.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCostCenter.ForeColor = System.Drawing.Color.White;
            this.btnCostCenter.Image = ((System.Drawing.Image)(resources.GetObject("btnCostCenter.Image")));
            this.btnCostCenter.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCostCenter.Location = new System.Drawing.Point(45, 234);
            this.btnCostCenter.Margin = new System.Windows.Forms.Padding(2);
            this.btnCostCenter.Name = "btnCostCenter";
            this.btnCostCenter.Size = new System.Drawing.Size(145, 37);
            this.btnCostCenter.TabIndex = 58;
            this.btnCostCenter.Text = "Cost Center";
            this.btnCostCenter.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCostCenter.UseVisualStyleBackColor = false;
            this.btnCostCenter.Click += new System.EventHandler(this.btnCostCenter_Click);
            // 
            // FormMaestrosInventario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(710, 374);
            this.Controls.Add(this.btnCostCenter);
            this.Controls.Add(this.btnNumeracion);
            this.Controls.Add(this.btnitems);
            this.Controls.Add(this.btnCatalogProv);
            this.Controls.Add(this.metroLink1);
            this.Controls.Add(this.BtnUnidades);
            this.Controls.Add(this.lblNombreForm);
            this.Controls.Add(this.btnCerrar);
            this.Name = "FormMaestrosInventario";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNombreForm;
        private MetroFramework.Controls.MetroLink btnCerrar;
        private MetroFramework.Controls.MetroLink metroLink1;
        private System.Windows.Forms.Button BtnUnidades;
        private System.Windows.Forms.Button btnCatalogProv;
        private System.Windows.Forms.Button btnitems;
        private System.Windows.Forms.Button btnNumeracion;
        private System.Windows.Forms.Button btnCostCenter;
    }
}