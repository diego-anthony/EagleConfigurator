﻿using ConfigBusinessEntity;
using ConfigBusinessLogic.Inventario;
using ConfigBusinessLogic.Sunat;
using ConfigUtilitarios;
using MetroFramework.Forms;
using System.Linq;
using ConfigUtilitarios.Extensions;
using ConfigBusinessLogic.Fiscal;
using ConfigUtilitarios.HelperDatabase;
using System;
using System.Windows.Forms;
using System.ComponentModel;
using System.Collections.Generic;
using ConfigUtilitarios.HelperControl;
using ConfigUtilitarios.ViewModels;
using ConfigUtilitarios.KeyValues;
using MetroFramework.Controls;

namespace ConfiguradorUI.Inventario
{
    public partial class FormNumeracion : MetroForm
    {
        #region Constants
        private const string IdColumn = "Id";
        private const string NroSerieColumn = "NroSerie";
        private const string NroInicialColumn = "NroInicial";
        private const string NroFinalColumn = "NroFinal";
        private const string NroActualColumn = "NroActual";
        private const string FechaRegistroColumn = "FechaRegistro";
        private const string NivelColumn = "Nivel";
        private const string TIpoComprobanteColumn = "TipoComp";
        private const string IdNivelColumn = "IdNivel";
        private const string IdTipoCompColumn = "IdTipoComp";
        #endregion

        #region Properties
        public int IdTipoComprobante
        {
            get
            {
                var comp = (SNTt10_tipo_comprobante) cboTipoComprobante.SelectedValue;
                return comp.id_tipo_comp;
            }
        }
        public int IdNivel
        {
            get
            {
                var nivel = (FISt02_nivel)cboNivel.SelectedValue;
                return nivel.id_nivel;
            }
        }
        public string NroSerie
        {
            get { return txtNroSerie.Text?.Trim(); }
            set { txtNroSerie.Text = value; }
        }
        public long? NroInicial
        {
            get { return long.Parse(txtNroInicial.Text?.Trim()); }
            set { txtNroSerie.Text = value?.ToString(); }
        }
        public long? NroFinal
        {
            get { return long.Parse(txtNroFinal.Text.Trim()); }
            set { txtNroSerie.Text = value?.ToString(); }
        }
        #endregion

        #region Attributes
        private BindingSource _mainSource;
        #endregion

        #region Constructors
        public FormNumeracion()
        {
            InitializeComponent();
        } 
        #endregion

        #region Methods
        private InvNumeracionVM GetNumeration()
        {
            var numeration = new InvNumeracionVM
            {
                NroInicial= NroInicial,
                NroFinal= NroFinal,
                NroSerie= NroSerie,
                FechaRegistro = HelperServer.GetCurrentDateTime().Date,
                NroActual= 1,
                IdNivel = IdNivel,
                IdTipoComp = IdTipoComprobante,
                Nivel = ((FISt02_nivel)cboNivel.SelectedItem).txt_desc,
                TipoComp = ((SNTt10_tipo_comprobante)cboTipoComprobante.SelectedItem).txt_desc,
            };
            return numeration;
        }
        private List<InvNumeracionVM> LoadData()
        {
            return new InvControlNumeracionBL().GetAll().Select(
                x => new InvNumeracionVM
                {
                    Id = x.id_control_numeracion,
                    NroSerie= x.txt_nro_serie,
                    NroInicial = x.nro_inicial,
                    NroFinal= x.nro_final,
                    NroActual = x.nro_actual,
                    FechaRegistro= x.fecha_registro,
                    Nivel= x.FISt02_nivel.txt_desc,
                    TipoComp= x.SNTt10_tipo_comprobante.txt_desc
                }).ToList();
        }
        private void ConfigDgv()
        {
            ControlHelper.DgvReadOnly(dgvMain);
            ControlHelper.DgvStyle(dgvMain);
            ConfigHeaderDgv();
        }
        private void ConfigHeaderDgv()
        {
            dgvMain.HideColumns(IdColumn,IdNivelColumn,IdTipoCompColumn);
            dgvMain.DefColumn(NroSerieColumn, "NUMERO SERIE");
            dgvMain.DefColumn(NroInicialColumn, "NUMERO INICIAL");
            dgvMain.DefColumn(NroFinalColumn, "NUMERO FINAL");
            dgvMain.DefColumn(NroActualColumn, "NUMERO ACTUAL");
            dgvMain.DefColumn(FechaRegistroColumn, "FECHA REGISTRO");
            dgvMain.DefColumn(NivelColumn, "NIVEL");
            dgvMain.DefColumn(TIpoComprobanteColumn, "TIPO COMP");
            dgvMain.Columns[NroSerieColumn].Width = 120;
            dgvMain.Columns[NroInicialColumn].Width = 130;
            dgvMain.Columns[FechaRegistroColumn].Width = 140;
            dgvMain.Columns[NroFinalColumn].Width = 140;
            dgvMain.Columns[TIpoComprobanteColumn].Width = 130;
            dgvMain.Columns[NroActualColumn].Width = 130;
        }
        private void LoadCbos()
        {
            cboTipoComprobante.DataSource = new TipoComprobanteBL().FindOrdCompra();
            cboTipoComprobante.DisplayMember = "txt_desc";

            cboNivel.DataSource = new NivelBL().GetAll();
            cboNivel.DisplayMember = "txt_desc";
        }
        private Response IsValid()
        {
            var result = new Response();
            var source = (BindingList<InvNumeracionVM>)_mainSource.DataSource;
            if (!source.Any(x => x.Id > 0))
            {
                result.IsSuccess = true;
            }
            else
            {
                result.Message = "Debe remover todos los elementos anteriores antes de guardar";
            }
            return result;
        }
        private bool IsItemValid()
        {
            var response = true;
            if (string.IsNullOrWhiteSpace(txtNroSerie.Text))
            {
                errorProv.SetError(txtNroSerie, ValidationMsg.Required);
                response = false;
            }
            if (string.IsNullOrWhiteSpace(txtNroInicial.Text))
            {
                errorProv.SetError(txtNroInicial, ValidationMsg.Required);
                response = false;
            }
            if (string.IsNullOrWhiteSpace(txtNroFinal.Text))
            {
                errorProv.SetError(txtNroFinal, ValidationMsg.Required);
                response = false;
            }
            return response;
        }
        private bool IsDocumentValid()
        {
            var response = false; 
            response = new InvControlNumeracionBL().SearchDocumentExist(IdTipoComprobante);
            return response;
        }
        private void CleanForm()
        {
            errorProv.Clear();
            txtNroSerie.Clear();
            txtNroInicial.Clear();
            txtNroFinal.Clear();
            txtInfoExtra01.Clear();
            txtInfoExtra02.Clear();
            DateFormat.SetFormat(dtpExtra01, DateFormat.Blank);
            DateFormat.SetFormat(dtpExtra02, DateFormat.Blank);
        }
        #endregion

        #region Events
        private void FormNumeracion_Load(object sender, System.EventArgs e)
        {
            CleanForm();
            LoadCbos();
            var list = LoadData();
            var bindingList = new BindingList<InvNumeracionVM>(list);
            _mainSource = new BindingSource(bindingList, null);

            dgvMain.DataSource = _mainSource;
            ConfigDgv();
            btnCommit.Enabled = false;
            txtNroInicial.KeyPress += ValidTxtNumeric;
            txtNroFinal.KeyPress += ValidTxtNumeric;
        }
        private void ValidTxtNumeric(object sender, KeyPressEventArgs e)
        {
            var txt = (MetroTextBox)sender;
            if (e.KeyChar >= '0' && e.KeyChar <= '9' || char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
        private void BtnAddItem_Click(object sender, System.EventArgs e)
        {
            if (!IsDocumentValid())
            {
                if (IsItemValid())
                {
                    var numeration = GetNumeration();
                    _mainSource.Add(numeration);
                    CleanForm();
                    btnCommit.Enabled = true;
                }
            }
            else {
                Msg.Ok_Wng("Ya existe una numeración para este tipo de documento");
            }

        }
        private void BtnRemoveItem_Click(object sender, System.EventArgs e)
        {
            if (dgvMain.CurrentRow is DataGridViewRow currentRow)
            {
                dgvMain.Rows.RemoveAt(currentRow.Index);
                btnCommit.Enabled = true;
            }
        }
        private void BtnCommit_Click(object sender, EventArgs e)
        {
            var validation = IsValid();
            if (validation.IsSuccess)
            {
                var source = (BindingList<InvNumeracionVM>)_mainSource.DataSource;
                var lista = new List<INVt11_control_numeracion>();
                foreach (var item in source)
                {
                    lista.Add(new INVt11_control_numeracion
                    {
                        txt_nro_serie = item.NroSerie,
                        fecha_registro = item.FechaRegistro,
                        locked_by = Estado.Unlocked,
                        nro_actual = item.NroActual,
                        id_estado = Estado.IdActivo,
                        txt_estado = Estado.TxtActivo,
                        nro_final = item.NroFinal,
                        id_nivel = item.IdNivel,
                        nro_inicial = item.NroInicial,
                        id_tipo_comp = item.IdTipoComp,
                    });
                }
                if (new InvControlNumeracionBL().Add(lista))
                {
                    btnCommit.Enabled = false;
                }
            }
            else
            {
                Msg.Ok_Err(validation.Message);
            }
        }
        #endregion
    }
}
