﻿using ConfiguradorUI.Producto;
using MetroFramework.Forms;

namespace ConfiguradorUI.Inventario
{
    public partial class FormMaestrosInventario : MetroForm
    {
        public FormMaestrosInventario()
        {
            InitializeComponent();
        }

        private void BtnUnidades_Click(object sender, System.EventArgs e)
        {
            new FormUnidadMedida().ShowDialog();
        }

        private void BtnCatalogProv_Click(object sender, System.EventArgs e)
        {
            new FormCatalogoProveedor().ShowDialog();
        }

        private void btnitems_Click(object sender, System.EventArgs e)
        {
            new FormItemProducto().ShowDialog();
        }

        private void btnNumeracion_Click(object sender, System.EventArgs e)
        {
            new FormNumeracion().ShowDialog();
        }

        private void btnCostCenter_Click(object sender, System.EventArgs e)
        {
            new FormCostCenter().ShowDialog();
        }
    }
}
