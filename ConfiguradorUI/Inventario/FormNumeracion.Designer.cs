﻿namespace ConfiguradorUI.Inventario
{
    partial class FormNumeracion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormNumeracion));
            this.lblNombreForm = new System.Windows.Forms.Label();
            this.btnCerrar = new MetroFramework.Controls.MetroLink();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgvMain = new System.Windows.Forms.DataGridView();
            this.btnAddItem = new System.Windows.Forms.Button();
            this.btnRemoveItem = new System.Windows.Forms.Button();
            this.dtpExtra02 = new MetroFramework.Controls.MetroDateTime();
            this.dtpExtra01 = new MetroFramework.Controls.MetroDateTime();
            this.txtInfoExtra02 = new MetroFramework.Controls.MetroTextBox();
            this.txtInfoExtra01 = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel12 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.txtNroFinal = new MetroFramework.Controls.MetroTextBox();
            this.txtNroInicial = new MetroFramework.Controls.MetroTextBox();
            this.txtNroSerie = new MetroFramework.Controls.MetroTextBox();
            this.cboNivel = new MetroFramework.Controls.MetroComboBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.cboTipoComprobante = new MetroFramework.Controls.MetroComboBox();
            this.lblNombre = new MetroFramework.Controls.MetroLabel();
            this.btnCommit = new System.Windows.Forms.Button();
            this.panelMantenimiento = new MetroFramework.Controls.MetroPanel();
            this.errorProv = new System.Windows.Forms.ErrorProvider(this.components);
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMain)).BeginInit();
            this.panelMantenimiento.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProv)).BeginInit();
            this.SuspendLayout();
            // 
            // lblNombreForm
            // 
            this.lblNombreForm.AutoSize = true;
            this.lblNombreForm.Font = new System.Drawing.Font("Segoe UI Semilight", 16F);
            this.lblNombreForm.ForeColor = System.Drawing.SystemColors.Highlight;
            this.lblNombreForm.Location = new System.Drawing.Point(100, 56);
            this.lblNombreForm.Name = "lblNombreForm";
            this.lblNombreForm.Size = new System.Drawing.Size(236, 30);
            this.lblNombreForm.TabIndex = 126;
            this.lblNombreForm.Text = "Control de Numeración";
            // 
            // btnCerrar
            // 
            this.btnCerrar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCerrar.Image = ((System.Drawing.Image)(resources.GetObject("btnCerrar.Image")));
            this.btnCerrar.ImageSize = 48;
            this.btnCerrar.Location = new System.Drawing.Point(38, 45);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(56, 57);
            this.btnCerrar.TabIndex = 125;
            this.btnCerrar.UseSelectable = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dgvMain);
            this.groupBox1.Controls.Add(this.btnAddItem);
            this.groupBox1.Controls.Add(this.btnRemoveItem);
            this.groupBox1.Controls.Add(this.dtpExtra02);
            this.groupBox1.Controls.Add(this.dtpExtra01);
            this.groupBox1.Controls.Add(this.txtInfoExtra02);
            this.groupBox1.Controls.Add(this.txtInfoExtra01);
            this.groupBox1.Controls.Add(this.metroLabel12);
            this.groupBox1.Controls.Add(this.metroLabel11);
            this.groupBox1.Controls.Add(this.metroLabel10);
            this.groupBox1.Controls.Add(this.metroLabel9);
            this.groupBox1.Controls.Add(this.txtNroFinal);
            this.groupBox1.Controls.Add(this.txtNroInicial);
            this.groupBox1.Controls.Add(this.txtNroSerie);
            this.groupBox1.Controls.Add(this.cboNivel);
            this.groupBox1.Controls.Add(this.panel4);
            this.groupBox1.Controls.Add(this.metroLabel7);
            this.groupBox1.Controls.Add(this.metroLabel6);
            this.groupBox1.Controls.Add(this.metroLabel5);
            this.groupBox1.Controls.Add(this.metroLabel4);
            this.groupBox1.Controls.Add(this.cboTipoComprobante);
            this.groupBox1.Controls.Add(this.lblNombre);
            this.groupBox1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(105, 129);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(794, 535);
            this.groupBox1.TabIndex = 128;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Numeración de Comprobantes Fiscales";
            // 
            // dgvMain
            // 
            this.dgvMain.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dgvMain.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvMain.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMain.Location = new System.Drawing.Point(12, 221);
            this.dgvMain.Name = "dgvMain";
            this.dgvMain.ReadOnly = true;
            this.dgvMain.Size = new System.Drawing.Size(768, 256);
            this.dgvMain.TabIndex = 203;
            // 
            // btnAddItem
            // 
            this.btnAddItem.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.btnAddItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnAddItem.FlatAppearance.BorderColor = System.Drawing.Color.MediumSeaGreen;
            this.btnAddItem.FlatAppearance.MouseOverBackColor = System.Drawing.Color.SeaGreen;
            this.btnAddItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddItem.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddItem.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnAddItem.Image = ((System.Drawing.Image)(resources.GetObject("btnAddItem.Image")));
            this.btnAddItem.Location = new System.Drawing.Point(729, 132);
            this.btnAddItem.Name = "btnAddItem";
            this.btnAddItem.Size = new System.Drawing.Size(33, 29);
            this.btnAddItem.TabIndex = 201;
            this.btnAddItem.UseVisualStyleBackColor = false;
            this.btnAddItem.Click += new System.EventHandler(this.BtnAddItem_Click);
            // 
            // btnRemoveItem
            // 
            this.btnRemoveItem.BackColor = System.Drawing.Color.IndianRed;
            this.btnRemoveItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnRemoveItem.FlatAppearance.BorderColor = System.Drawing.Color.IndianRed;
            this.btnRemoveItem.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnRemoveItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemoveItem.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemoveItem.Image = ((System.Drawing.Image)(resources.GetObject("btnRemoveItem.Image")));
            this.btnRemoveItem.Location = new System.Drawing.Point(729, 171);
            this.btnRemoveItem.Name = "btnRemoveItem";
            this.btnRemoveItem.Size = new System.Drawing.Size(33, 29);
            this.btnRemoveItem.TabIndex = 202;
            this.btnRemoveItem.UseVisualStyleBackColor = false;
            this.btnRemoveItem.Click += new System.EventHandler(this.BtnRemoveItem_Click);
            // 
            // dtpExtra02
            // 
            this.dtpExtra02.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpExtra02.Location = new System.Drawing.Point(485, 171);
            this.dtpExtra02.MinimumSize = new System.Drawing.Size(0, 29);
            this.dtpExtra02.Name = "dtpExtra02";
            this.dtpExtra02.Size = new System.Drawing.Size(225, 29);
            this.dtpExtra02.TabIndex = 200;
            // 
            // dtpExtra01
            // 
            this.dtpExtra01.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpExtra01.Location = new System.Drawing.Point(485, 132);
            this.dtpExtra01.MinimumSize = new System.Drawing.Size(0, 29);
            this.dtpExtra01.Name = "dtpExtra01";
            this.dtpExtra01.Size = new System.Drawing.Size(225, 29);
            this.dtpExtra01.TabIndex = 199;
            // 
            // txtInfoExtra02
            // 
            this.txtInfoExtra02.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(253)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.txtInfoExtra02.CustomButton.Image = null;
            this.txtInfoExtra02.CustomButton.Location = new System.Drawing.Point(245, 1);
            this.txtInfoExtra02.CustomButton.Name = "";
            this.txtInfoExtra02.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtInfoExtra02.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtInfoExtra02.CustomButton.TabIndex = 1;
            this.txtInfoExtra02.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtInfoExtra02.CustomButton.UseSelectable = true;
            this.txtInfoExtra02.CustomButton.Visible = false;
            this.txtInfoExtra02.Lines = new string[0];
            this.txtInfoExtra02.Location = new System.Drawing.Point(100, 174);
            this.txtInfoExtra02.MaxLength = 32767;
            this.txtInfoExtra02.Name = "txtInfoExtra02";
            this.txtInfoExtra02.PasswordChar = '\0';
            this.txtInfoExtra02.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtInfoExtra02.SelectedText = "";
            this.txtInfoExtra02.SelectionLength = 0;
            this.txtInfoExtra02.SelectionStart = 0;
            this.txtInfoExtra02.ShortcutsEnabled = true;
            this.txtInfoExtra02.Size = new System.Drawing.Size(267, 23);
            this.txtInfoExtra02.TabIndex = 198;
            this.txtInfoExtra02.UseCustomBackColor = true;
            this.txtInfoExtra02.UseSelectable = true;
            this.txtInfoExtra02.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtInfoExtra02.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtInfoExtra01
            // 
            this.txtInfoExtra01.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(253)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.txtInfoExtra01.CustomButton.Image = null;
            this.txtInfoExtra01.CustomButton.Location = new System.Drawing.Point(245, 1);
            this.txtInfoExtra01.CustomButton.Name = "";
            this.txtInfoExtra01.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtInfoExtra01.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtInfoExtra01.CustomButton.TabIndex = 1;
            this.txtInfoExtra01.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtInfoExtra01.CustomButton.UseSelectable = true;
            this.txtInfoExtra01.CustomButton.Visible = false;
            this.txtInfoExtra01.Lines = new string[0];
            this.txtInfoExtra01.Location = new System.Drawing.Point(100, 135);
            this.txtInfoExtra01.MaxLength = 32767;
            this.txtInfoExtra01.Name = "txtInfoExtra01";
            this.txtInfoExtra01.PasswordChar = '\0';
            this.txtInfoExtra01.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtInfoExtra01.SelectedText = "";
            this.txtInfoExtra01.SelectionLength = 0;
            this.txtInfoExtra01.SelectionStart = 0;
            this.txtInfoExtra01.ShortcutsEnabled = true;
            this.txtInfoExtra01.Size = new System.Drawing.Size(267, 23);
            this.txtInfoExtra01.TabIndex = 197;
            this.txtInfoExtra01.UseCustomBackColor = true;
            this.txtInfoExtra01.UseSelectable = true;
            this.txtInfoExtra01.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtInfoExtra01.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel12
            // 
            this.metroLabel12.AutoSize = true;
            this.metroLabel12.ForeColor = System.Drawing.Color.Navy;
            this.metroLabel12.Location = new System.Drawing.Point(382, 176);
            this.metroLabel12.Name = "metroLabel12";
            this.metroLabel12.Size = new System.Drawing.Size(94, 19);
            this.metroLabel12.TabIndex = 195;
            this.metroLabel12.Text = "Fecha Extra 02";
            this.metroLabel12.UseCustomForeColor = true;
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.ForeColor = System.Drawing.Color.Navy;
            this.metroLabel11.Location = new System.Drawing.Point(382, 137);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(92, 19);
            this.metroLabel11.TabIndex = 194;
            this.metroLabel11.Text = "Fecha Extra 01";
            this.metroLabel11.UseCustomForeColor = true;
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.ForeColor = System.Drawing.Color.Navy;
            this.metroLabel10.Location = new System.Drawing.Point(12, 176);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(82, 19);
            this.metroLabel10.TabIndex = 193;
            this.metroLabel10.Text = "Info Extra 02";
            this.metroLabel10.UseCustomForeColor = true;
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.ForeColor = System.Drawing.Color.Navy;
            this.metroLabel9.Location = new System.Drawing.Point(14, 137);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(80, 19);
            this.metroLabel9.TabIndex = 192;
            this.metroLabel9.Text = "Info Extra 01";
            this.metroLabel9.UseCustomForeColor = true;
            // 
            // txtNroFinal
            // 
            this.txtNroFinal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(253)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.txtNroFinal.CustomButton.Image = null;
            this.txtNroFinal.CustomButton.Location = new System.Drawing.Point(84, 1);
            this.txtNroFinal.CustomButton.Name = "";
            this.txtNroFinal.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtNroFinal.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtNroFinal.CustomButton.TabIndex = 1;
            this.txtNroFinal.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtNroFinal.CustomButton.UseSelectable = true;
            this.txtNroFinal.CustomButton.Visible = false;
            this.txtNroFinal.Lines = new string[0];
            this.txtNroFinal.Location = new System.Drawing.Point(657, 96);
            this.txtNroFinal.MaxLength = 32767;
            this.txtNroFinal.Name = "txtNroFinal";
            this.txtNroFinal.PasswordChar = '\0';
            this.txtNroFinal.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNroFinal.SelectedText = "";
            this.txtNroFinal.SelectionLength = 0;
            this.txtNroFinal.SelectionStart = 0;
            this.txtNroFinal.ShortcutsEnabled = true;
            this.txtNroFinal.Size = new System.Drawing.Size(106, 23);
            this.txtNroFinal.TabIndex = 190;
            this.txtNroFinal.UseCustomBackColor = true;
            this.txtNroFinal.UseSelectable = true;
            this.txtNroFinal.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtNroFinal.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtNroInicial
            // 
            this.txtNroInicial.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(253)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.txtNroInicial.CustomButton.Image = null;
            this.txtNroInicial.CustomButton.Location = new System.Drawing.Point(73, 1);
            this.txtNroInicial.CustomButton.Name = "";
            this.txtNroInicial.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtNroInicial.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtNroInicial.CustomButton.TabIndex = 1;
            this.txtNroInicial.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtNroInicial.CustomButton.UseSelectable = true;
            this.txtNroInicial.CustomButton.Visible = false;
            this.txtNroInicial.Lines = new string[0];
            this.txtNroInicial.Location = new System.Drawing.Point(485, 96);
            this.txtNroInicial.MaxLength = 32767;
            this.txtNroInicial.Name = "txtNroInicial";
            this.txtNroInicial.PasswordChar = '\0';
            this.txtNroInicial.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNroInicial.SelectedText = "";
            this.txtNroInicial.SelectionLength = 0;
            this.txtNroInicial.SelectionStart = 0;
            this.txtNroInicial.ShortcutsEnabled = true;
            this.txtNroInicial.Size = new System.Drawing.Size(95, 23);
            this.txtNroInicial.TabIndex = 189;
            this.txtNroInicial.UseCustomBackColor = true;
            this.txtNroInicial.UseSelectable = true;
            this.txtNroInicial.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtNroInicial.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // txtNroSerie
            // 
            this.txtNroSerie.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(253)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.txtNroSerie.CustomButton.Image = null;
            this.txtNroSerie.CustomButton.Location = new System.Drawing.Point(245, 1);
            this.txtNroSerie.CustomButton.Name = "";
            this.txtNroSerie.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtNroSerie.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtNroSerie.CustomButton.TabIndex = 1;
            this.txtNroSerie.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtNroSerie.CustomButton.UseSelectable = true;
            this.txtNroSerie.CustomButton.Visible = false;
            this.txtNroSerie.Lines = new string[0];
            this.txtNroSerie.Location = new System.Drawing.Point(100, 96);
            this.txtNroSerie.MaxLength = 32767;
            this.txtNroSerie.Name = "txtNroSerie";
            this.txtNroSerie.PasswordChar = '\0';
            this.txtNroSerie.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNroSerie.SelectedText = "";
            this.txtNroSerie.SelectionLength = 0;
            this.txtNroSerie.SelectionStart = 0;
            this.txtNroSerie.ShortcutsEnabled = true;
            this.txtNroSerie.Size = new System.Drawing.Size(267, 23);
            this.txtNroSerie.TabIndex = 188;
            this.txtNroSerie.UseCustomBackColor = true;
            this.txtNroSerie.UseSelectable = true;
            this.txtNroSerie.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtNroSerie.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // cboNivel
            // 
            this.cboNivel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboNivel.FormattingEnabled = true;
            this.cboNivel.ItemHeight = 23;
            this.cboNivel.Location = new System.Drawing.Point(100, 29);
            this.cboNivel.Name = "cboNivel";
            this.cboNivel.Size = new System.Drawing.Size(267, 29);
            this.cboNivel.TabIndex = 163;
            this.cboNivel.UseSelectable = true;
            // 
            // panel4
            // 
            this.panel4.BackgroundImage = global::ConfiguradorUI.Properties.Resources.linea_celeste;
            this.panel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel4.Location = new System.Drawing.Point(10, 68);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(768, 5);
            this.panel4.TabIndex = 160;
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.ForeColor = System.Drawing.Color.Navy;
            this.metroLabel7.Location = new System.Drawing.Point(586, 98);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(66, 19);
            this.metroLabel7.TabIndex = 9;
            this.metroLabel7.Text = "Nro. Final";
            this.metroLabel7.UseCustomForeColor = true;
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.ForeColor = System.Drawing.Color.Navy;
            this.metroLabel6.Location = new System.Drawing.Point(382, 98);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(71, 19);
            this.metroLabel6.TabIndex = 8;
            this.metroLabel6.Text = "Nro. Inicial";
            this.metroLabel6.UseCustomForeColor = true;
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.ForeColor = System.Drawing.Color.Navy;
            this.metroLabel5.Location = new System.Drawing.Point(12, 98);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(68, 19);
            this.metroLabel5.TabIndex = 7;
            this.metroLabel5.Text = "Nro. Serie";
            this.metroLabel5.UseCustomForeColor = true;
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.ForeColor = System.Drawing.Color.Navy;
            this.metroLabel4.Location = new System.Drawing.Point(382, 34);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(122, 19);
            this.metroLabel4.TabIndex = 6;
            this.metroLabel4.Text = "Tipo Comprobante";
            this.metroLabel4.UseCustomForeColor = true;
            // 
            // cboTipoComprobante
            // 
            this.cboTipoComprobante.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboTipoComprobante.FormattingEnabled = true;
            this.cboTipoComprobante.ItemHeight = 23;
            this.cboTipoComprobante.Location = new System.Drawing.Point(510, 29);
            this.cboTipoComprobante.Name = "cboTipoComprobante";
            this.cboTipoComprobante.Size = new System.Drawing.Size(252, 29);
            this.cboTipoComprobante.TabIndex = 3;
            this.cboTipoComprobante.UseSelectable = true;
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.ForeColor = System.Drawing.Color.Navy;
            this.lblNombre.Location = new System.Drawing.Point(12, 34);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(38, 19);
            this.lblNombre.TabIndex = 1;
            this.lblNombre.Text = "Nivel";
            this.lblNombre.UseCustomForeColor = true;
            // 
            // btnCommit
            // 
            this.btnCommit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.btnCommit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnCommit.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.btnCommit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.btnCommit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCommit.Image = ((System.Drawing.Image)(resources.GetObject("btnCommit.Image")));
            this.btnCommit.Location = new System.Drawing.Point(468, 5);
            this.btnCommit.Name = "btnCommit";
            this.btnCommit.Size = new System.Drawing.Size(40, 40);
            this.btnCommit.TabIndex = 0;
            this.btnCommit.UseVisualStyleBackColor = false;
            this.btnCommit.Click += new System.EventHandler(this.BtnCommit_Click);
            // 
            // panelMantenimiento
            // 
            this.panelMantenimiento.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(253)))), ((int)(((byte)(255)))));
            this.panelMantenimiento.Controls.Add(this.btnCommit);
            this.panelMantenimiento.HorizontalScrollbarBarColor = true;
            this.panelMantenimiento.HorizontalScrollbarHighlightOnWheel = false;
            this.panelMantenimiento.HorizontalScrollbarSize = 10;
            this.panelMantenimiento.Location = new System.Drawing.Point(391, 45);
            this.panelMantenimiento.Name = "panelMantenimiento";
            this.panelMantenimiento.Size = new System.Drawing.Size(516, 49);
            this.panelMantenimiento.Style = MetroFramework.MetroColorStyle.Green;
            this.panelMantenimiento.TabIndex = 127;
            this.panelMantenimiento.UseCustomBackColor = true;
            this.panelMantenimiento.UseStyleColors = true;
            this.panelMantenimiento.VerticalScrollbarBarColor = true;
            this.panelMantenimiento.VerticalScrollbarHighlightOnWheel = false;
            this.panelMantenimiento.VerticalScrollbarSize = 10;
            // 
            // errorProv
            // 
            this.errorProv.ContainerControl = this;
            // 
            // FormNumeracion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(920, 687);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panelMantenimiento);
            this.Controls.Add(this.lblNombreForm);
            this.Controls.Add(this.btnCerrar);
            this.Name = "FormNumeracion";
            this.Load += new System.EventHandler(this.FormNumeracion_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMain)).EndInit();
            this.panelMantenimiento.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNombreForm;
        private MetroFramework.Controls.MetroLink btnCerrar;
        private System.Windows.Forms.GroupBox groupBox1;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroComboBox cboTipoComprobante;
        private MetroFramework.Controls.MetroLabel lblNombre;
        private MetroFramework.Controls.MetroComboBox cboNivel;
        private System.Windows.Forms.Panel panel4;
        private MetroFramework.Controls.MetroDateTime dtpExtra02;
        private MetroFramework.Controls.MetroDateTime dtpExtra01;
        private MetroFramework.Controls.MetroTextBox txtInfoExtra02;
        private MetroFramework.Controls.MetroTextBox txtInfoExtra01;
        private MetroFramework.Controls.MetroLabel metroLabel12;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroTextBox txtNroFinal;
        private MetroFramework.Controls.MetroTextBox txtNroInicial;
        private MetroFramework.Controls.MetroTextBox txtNroSerie;
        private System.Windows.Forms.Button btnAddItem;
        private System.Windows.Forms.Button btnRemoveItem;
        private System.Windows.Forms.DataGridView dgvMain;
        private System.Windows.Forms.Button btnCommit;
        private MetroFramework.Controls.MetroPanel panelMantenimiento;
        private System.Windows.Forms.ErrorProvider errorProv;
    }
}