﻿using ConfigBusinessEntity;
using ConfigBusinessLogic;
using ConfigUtilitarios;
using ConfigUtilitarios.HelperControl;
using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace ConfiguradorUI.Buscadores
{
    public partial class FormBuscarUnidadMedida : MetroForm
    {
        #region Variables
        public INVt10_unidad_medida UnidadMedida = null;
        #endregion

        public FormBuscarUnidadMedida()
        {
            InitializeComponent();
        }
        #region Métodos
        void AddHandled()
        {
            txtCodigo.KeyDown += FocusDgv_KeyDown;
            txtDescripcionProd.KeyDown += FocusDgv_KeyDown;
        }
        private void FocusDgv_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down || e.KeyCode == Keys.Up)
            {
                dgvProd.Focus();
            }
        }
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                Hide();
                Close();
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }
        private void CargarGrid(IEnumerable<INVt10_unidad_medida> list)
        {
            if (list != null)
            {
                dgvProd.DataSource = list.Select(x => new
                {
                    ID = x.id_um,
                    CODIGO = x.cod_um,
                    DESCRIPCION = x.txt_desc,
                    ESTADO = x.txt_estado,
                }).ToList();
            }
            else
            {
                var prodHeader = new List<TNSt05_comp_emitido_dtl>();
                dgvProd.DataSource = prodHeader.Select(x => new
                {
                    ID = string.Empty,
                    CODIGO = string.Empty,
                    DESCRIPCION = string.Empty,
                    ESTADO = string.Empty,
                }).ToList();
            }
            DefinirCabeceraGrid();
        }
        private void Search(bool showAll = false)
        {
            var cod = txtCodigo.Text.Trim();
            var nombre = txtDescripcionProd.Text.Trim();
            var estado = chkIncluirInactivos.Checked ? Estado.Ignorar : Estado.IdActivo;
            if (showAll)
            {
                cod = "";
                nombre = "";
                estado = Estado.Ignorar;
            }
            var list = new ProdUnidadMedidaBL().FindAllUnits(cod,nombre,estado)
                    .OrderBy(x => string.IsNullOrEmpty(x.cod_um))
                    .ThenBy(x => x.cod_um, new AlphaNumericComparer())
                    .ThenBy(x => x.txt_desc).ToList();
            CargarGrid(list);
        }
        private void SetInicio()
        {
            AddHandled();
            ConfigurarControles();
            Search();
        }
        private void DefinirCabeceraGrid()
        {
            try
            {
                dgvProd.Columns["ID"].Visible = false;

                dgvProd.Columns["CODIGO"].HeaderText = "CÓDIGO 01";
                dgvProd.Columns["CODIGO"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvProd.Columns["CODIGO"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                dgvProd.Columns["DESCRIPCION"].HeaderText = "DESCRIPCIÓN";

                dgvProd.Columns["ESTADO"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvProd.Columns["ESTADO"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                dgvProd.Columns["DESCRIPCION"].Width = 387;
            }
            catch (Exception e)
            {
                MessageBox.Show($"No se pudo definir la cabecera de la grilla. Excepción: {e.Message}", "Excepción encontrada", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void ConfigurarControles()
        {
            #region Form
            MaximizeBox = false;
            Resizable = false;
            #endregion

            #region TextBox
            txtCodigo.MaxLength = 50;
            txtDescripcionProd.MaxLength = 350;
            #endregion

            #region Grilla
            var prodHeader = new List<TNSt05_comp_emitido_dtl>();
            dgvProd.DataSource = prodHeader.Select(x => new
            {
                ID = string.Empty,
                CODIGO = string.Empty,
                DESCRIPCION = string.Empty,
                ESTADO = string.Empty,

            }).ToList();
            DefinirCabeceraGrid();
            ControlHelper.DgvReadOnly(dgvProd);
            ControlHelper.DgvStyle(dgvProd);
            #endregion

        }
        private INVt10_unidad_medida GetObject()
        {
            try
            {
                var prod = new INVt10_unidad_medida()
                {
                    // Cambiar a long cuando se corriga el modelo
                    id_um = long.Parse(dgvProd.Rows[dgvProd.CurrentRow.Index].Cells["ID"].Value.ToString()),
                    cod_um = dgvProd.Rows[dgvProd.CurrentRow.Index].Cells["CODIGO"].Value.ToString(),
                    txt_desc = dgvProd.Rows[dgvProd.CurrentRow.Index].Cells["DESCRIPCION"].Value.ToString(),
                    txt_estado = dgvProd.Rows[dgvProd.CurrentRow.Index].Cells["ESTADO"].Value.ToString(),
                };

                return prod;
            }
            catch (Exception ex)
            {
                var log = new Log();
                log.ArchiveLog("Buscar unidad de medida: Error al obtener unidad", ex.Message);
                Msg.Ok_Err("No se pudo obtener el producto.", "Excepción encontrada");
            }
            return null;
        }
        private void Seleccionar()
        {
            if (dgvProd.CurrentRow != null)
            {
                try
                {
                    var estado = dgvProd.Rows[dgvProd.CurrentRow.Index].Cells["ESTADO"].Value.ToString();
                    var desc = dgvProd.Rows[dgvProd.CurrentRow.Index].Cells["DESCRIPCION"].Value.ToString();
                    if (estado == Estado.TxtActivo)
                    {
                        UnidadMedida = GetObject();
                        CerrarForm();
                    }
                    else
                    {
                        if (DialogResult.OK == Msg.OkCancel_Info($@"No puede seleccionar la Unidad de Medida: '{desc}' porque NO ESTÁ ACTIVADO. ¿Desea activarlo y seleccionarlo?."))
                        {
                            if (long.TryParse(ControlHelper.DgvGetCellValueSelected(dgvProd, 0), out long id))
                            {
                                if (new ProdUnidadMedidaBL().Activar(id))
                                {
                                    UnidadMedida = GetObject();
                                    if (UnidadMedida != null)
                                    {
                                        UnidadMedida.id_estado = Estado.IdActivo;
                                        UnidadMedida.txt_estado = Estado.TxtActivo;
                                    }
                                    CerrarForm();
                                }
                                else
                                    Msg.Ok_Err($"1- No se pudo activar la unidad de Medida '{desc}'.");
                            }
                            else
                                Msg.Ok_Err($"2- No se pudo activar la unidad de Medida '{desc}'.");
                        }
                    }
                }
                catch (Exception e)
                {
                    UnidadMedida = null;
                    MessageBox.Show($"No se pudo seleccionar el producto. Excepción: {e.Message}", "Excepción encontrada", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void CerrarForm()
        {
            Hide();
            Close();
        }
        #endregion

        #region Eventos
        private void FormBuscarProducto_Load(object sender, EventArgs e)
        {
            SetInicio();
        }
        private void txtCodigo_TextChanged(object sender, EventArgs e)
        {
            Search();
        }
        private void txtCodigo02_TextChanged(object sender, EventArgs e)
        {
            Search();
        }
        private void txtDescripcionProd_TextChanged(object sender, EventArgs e)
        {
            Search();
        }
        private void dgvProd_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Seleccionar();
        }
        private void chkIncluirInactivos_CheckedChanged(object sender, EventArgs e)
        {
            Search();
        }
        private void chkProdVenta_CheckedChanged(object sender, EventArgs e)
        {
            Search();
        }
        private void chkProdCompra_CheckedChanged(object sender, EventArgs e)
        {
            Search();
        }
        private void btnSeleccionar_Click(object sender, EventArgs e)
        {
            Seleccionar();
        }
        private void btnVerTodos_Click(object sender, EventArgs e)
        {
            Search(true);
        }
        private void dgvProd_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Seleccionar();
            }
        }
        private void dgvProd_Paint(object sender, PaintEventArgs e)
        {
            ControlHelper.DgvSetColorBorder(sender, e);
        }
        

        private void chkIncluirInactivos_CheckedChanged_1(object sender, EventArgs e)
        {
            Search();
        }
        #endregion
    }
}
