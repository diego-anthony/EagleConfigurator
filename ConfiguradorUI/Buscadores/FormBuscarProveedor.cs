﻿using ConfigBusinessEntity;
using ConfigBusinessLogic.Persona;
using ConfigUtilitarios;
using ConfigUtilitarios.HelperControl;
using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace ConfiguradorUI.Buscadores
{
    public partial class FormBuscarProveedor : MetroForm
    {
        #region Variables
        public PERt03_proveedor vendor = null;
        #endregion

        public FormBuscarProveedor()
        {
            InitializeComponent();
        }

        #region Metodos
        void AddHandled()
        {
            txtCodigo.KeyDown += FocusDgv_KeyDown;
            txtnroDoc.KeyDown += FocusDgv_KeyDown;
            txtnroRuc.KeyDown += FocusDgv_KeyDown;
            txtNombre.KeyDown += FocusDgv_KeyDown;
        }
        private void FocusDgv_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down || e.KeyCode == Keys.Up)
            {
                dgvProveedor.Focus();
            }
        }
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                Hide();
                Close();
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }
        private void CargarGrid(IEnumerable<PERt03_proveedor> list)
        {
            if (list != null)
            {
                
                dgvProveedor.DataSource = list.Select(x => new
                {
                    ID = x.id_proveedor,
                    CODIGO = x.cod_proveedor,
                    NRODOC = x.nro_doc,
                    NRORUC = x.nro_ruc,
                    NCOMERCIAL = x.txt_nom_comercial,
                    ESTADO = x.txt_estado,
                    EXCLUSIVO = x.es_exclusivo 
                }).ToList();

            }
            else
            {
                var ccHeader = new List<PERt03_proveedor>();
                dgvProveedor.DataSource = ccHeader.Select(x => new
                {
                    ID = "",
                    CODIGO = "",
                    NRODOC = "",
                    NRORUC = "",
                    NCOMERCIAL = "",
                    ESTADO = "",
                    EXCLUSIVO = ""
                }).ToList();

            }
            
            DefinirCabeceraGrid();
        }
        private void Search(bool showAll = false)
        {
            var codpro = txtCodigo.Text.Trim();
            var nrodoc = txtnroDoc.Text.Trim();
            var nroruc = txtnroRuc.Text.Trim();
            var nombre = txtNombre.Text.Trim();
            int exclusivo= chkSnCat.Checked? Estado.IdInactivo: Estado.IdActivo;
            int? estado = chkIncluirInactivos.Checked ? Estado.Ignorar : Estado.IdActivo;
            if (showAll)
            {
                codpro = "";
                nrodoc = "";
                nroruc = "";
                exclusivo = Estado.IdInactivo;
                nombre = "";
                estado = Estado.Ignorar;
                
            }
            var list = new ProveedorBL().searchVendor(codpro, nrodoc, nroruc, nombre, estado,exclusivo);
            CargarGrid(list);
        }
        private void SetInicio()
        {
            AddHandled();
            ConfigurarControles();
            Search();
        }
        private void DefinirCabeceraGrid()
        {
            try
            {
                
                dgvProveedor.Columns["ID"].Visible = false;
                //dgvProveedor.Columns["EXCLUSIVO"].Visible = false;
                

                dgvProveedor.Columns["CODIGO"].HeaderText = "CÓDIGO";
                dgvProveedor.Columns["CODIGO"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvProveedor.Columns["CODIGO"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                dgvProveedor.Columns["NRODOC"].HeaderText = "NRODOC";
                dgvProveedor.Columns["NRODOC"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvProveedor.Columns["NRODOC"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                dgvProveedor.Columns["NRORUC"].HeaderText = "NRORUC";
                dgvProveedor.Columns["NRORUC"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvProveedor.Columns["NRORUC"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;


                dgvProveedor.Columns["NCOMERCIAL"].HeaderText = "DESCRIPCIÓN";

                dgvProveedor.Columns["ESTADO"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvProveedor.Columns["ESTADO"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                dgvProveedor.Columns["EXCLUSIVO"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvProveedor.Columns["EXCLUSIVO"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                
                

                dgvProveedor.Columns["NCOMERCIAL"].Width = 270;
            }
            catch (Exception e)
            {
                MessageBox.Show($"No se pudo definir la cabecera de la grilla. Excepción: {e.Message}", "Excepción encontrada", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void ConfigurarControles()
        {
            #region Form
            MaximizeBox = false;
            Resizable = false;
            #endregion

            #region TextBox
            txtCodigo.MaxLength = 50;
            txtnroDoc.MaxLength = 20;
            txtnroRuc.MaxLength = 20;
            txtNombre.MaxLength = 350;
            #endregion

            #region Grilla
            var prodHeader = new List<TNSt05_comp_emitido_dtl>();
            dgvProveedor.DataSource = prodHeader.Select(x => new
            {
                ID = string.Empty,
                CODIGO = string.Empty,
                NRODOC = string.Empty,
                NRORUC = string.Empty,
                NCOMERCIAL = string.Empty,
                ESTADO = string.Empty,
                EXCLUSIVO = string.Empty

            }).ToList();
            DefinirCabeceraGrid();
            ControlHelper.DgvReadOnly(dgvProveedor);
            ControlHelper.DgvStyle(dgvProveedor);
            #endregion
        }
        private PERt03_proveedor GetObject()
        {
            try
            {
                var vendorGet = new PERt03_proveedor()
                {
                    id_proveedor = long.Parse(dgvProveedor.Rows[dgvProveedor.CurrentRow.Index].Cells["ID"].Value.ToString()),
                    cod_proveedor = dgvProveedor.Rows[dgvProveedor.CurrentRow.Index].Cells["CODIGO"].Value.ToString(),
                    nro_doc = dgvProveedor.Rows[dgvProveedor.CurrentRow.Index].Cells["NRODOC"].Value.ToString(),
                    nro_ruc = dgvProveedor.Rows[dgvProveedor.CurrentRow.Index].Cells["NRORUC"].Value.ToString(),
                    txt_nom_comercial = dgvProveedor.Rows[dgvProveedor.CurrentRow.Index].Cells["NCOMERCIAL"].Value.ToString(),
                    txt_estado = dgvProveedor.Rows[dgvProveedor.CurrentRow.Index].Cells["ESTADO"].Value.ToString(),
                    es_exclusivo = bool.Parse(dgvProveedor.Rows[dgvProveedor.CurrentRow.Index].Cells["EXCLUSIVO"].Value.ToString())

                };

                return vendorGet;
            }
            catch (Exception ex)
            {
                var log = new Log();
                log.ArchiveLog("Buscar proveedor: Error al obtener el proveedor", ex.Message);
                Msg.Ok_Err("No se pudo obtener el proveedor.", "Excepción encontrada");
            }
            return null;
        }
        private void Seleccionar()
        {
            if (dgvProveedor.CurrentRow != null)
            {
                try
                {
                    var estado = dgvProveedor.Rows[dgvProveedor.CurrentRow.Index].Cells["ESTADO"].Value.ToString();
                    var nombre = dgvProveedor.Rows[dgvProveedor.CurrentRow.Index].Cells["NCOMERCIAL"].Value.ToString();
                    var exclusivo = dgvProveedor.Rows[dgvProveedor.CurrentRow.Index].Cells["EXCLUSIVO"].Value.ToString();

                    if (estado == Estado.TxtActivo &&  exclusivo == "True")
                    {
                        vendor = GetObject();
                        CerrarForm();
                    }
                    else
                    {
                        if (estado == Estado.TxtActivo && exclusivo == "False")
                        {
                            if (DialogResult.OK == Msg.OkCancel_Info($@"No se puede seleccionar el proveedor '{nombre}' porque no es EXCLUSIVO. ¿Desea activarlo y seleccionarlo?"))
                            {
                                if (long.TryParse(ControlHelper.DgvGetCellValueSelected(dgvProveedor, 0), out long id))
                                {
                                    if (new ProveedorBL().activeCatalogoVendor(id))
                                    {
                                        vendor = GetObject();
                                        if (vendor != null)
                                        {
                                            vendor.es_exclusivo = Convert.ToBoolean(Estado.IdActivo);
                                            
                                        }
                                        CerrarForm();
                                    }
                                    else
                                    {
                                        Msg.Ok_Err($"1 - No se pudo activar el catalogo para proveedor activo '{nombre}'.");
                                    }
                                }
                                else
                                {
                                    Msg.Ok_Err($"2 - No se pudo activar el catalogo para proveedor activo '{nombre}'.");
                                }
                            }

                        }
                        else
                        {
                            if (estado != Estado.TxtActivo && exclusivo == "True")
                            {
                                if (DialogResult.OK == Msg.OkCancel_Info($@"No se puede seleccionar el proveedor '{nombre}' porque esta INACTIVO. ¿Desea activarlo y seleccionarlo?"))
                                {
                                    if (long.TryParse(ControlHelper.DgvGetCellValueSelected(dgvProveedor, 0), out long id))
                                    {
                                        if (new ProveedorBL().activeVendor(id))
                                        {
                                            vendor = GetObject();
                                            if (vendor != null)
                                            {
                                                vendor.id_estado = Estado.IdActivo;
                                                vendor.txt_estado = Estado.TxtActivo;
                                            }
                                            CerrarForm();
                                        }
                                        else
                                        {
                                            Msg.Ok_Err($"1 - No se pudo activar el proveedor '{nombre}'.");
                                        }
                                    }
                                    else
                                    {
                                        Msg.Ok_Err($"2 - No se pudo activar el proveedor '{nombre}'.");
                                    }
                                }

                            }
                            else
                            {
                                if (DialogResult.OK == Msg.OkCancel_Info($@"No se puede seleccionar el proveedor '{nombre}' porque no es EXCLUSIVO y no esta ACTIVO. ¿Desea activarlo y seleccionarlo?"))
                                {
                                    if (long.TryParse(ControlHelper.DgvGetCellValueSelected(dgvProveedor, 0), out long id))
                                    {
                                        if (new ProveedorBL().activeCatalogoVendor(id))
                                        {
                                            vendor = GetObject();
                                            if (vendor != null)
                                            {
                                                vendor.es_exclusivo = Convert.ToBoolean(Estado.IdActivo);

                                            }
                                            CerrarForm();
                                        }
                                        else
                                        {
                                            Msg.Ok_Err($"1 - No se pudo activar el catalogo para proveedor inactivo '{nombre}'.");
                                        }

                                        if (new ProveedorBL().activeVendor(id))
                                        {
                                            vendor = GetObject();
                                            if (vendor != null)
                                            {
                                                vendor.id_estado = Estado.IdActivo;
                                                vendor.txt_estado = Estado.TxtActivo;
                                            }
                                            CerrarForm();
                                        }
                                        else
                                        {
                                            Msg.Ok_Err($"1 -  No se pudo activar el catalogo para proveedor inactivo '{nombre}'.");
                                        }
                                    }
                                    else
                                    {
                                        Msg.Ok_Err($"3 - No se pudo activar el catalogo para proveedor inactivo '{nombre}'.");
                                    }
                                }

                               

                            }
                        }
                    }
                }
                catch (Exception ex)
                {

                    vendor = null;
                    MessageBox.Show($"No se pudo seleccionar el proveedor. Excepción: {ex.Message}", "Excepción encontrada", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void CerrarForm()
        {
            Hide();
            Close();
        }

        #endregion

        #region Eventos
        private void FormBuscarProveedor_Load(object sender, EventArgs e)
        {
            SetInicio();
        }
        private void txtCodigo_Click(object sender, EventArgs e)
        {
            Search();
        }

        private void txtnroDoc_Click(object sender, EventArgs e)
        {
            Search();
        }

        private void txtnroRuc_Click(object sender, EventArgs e)
        {
            Search();
        }

        private void txtNombre_Click(object sender, EventArgs e)
        {
            Search();
        }

        private void dgvProveedor_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Seleccionar();
        }

        private void chkIncluirInactivos_CheckedChanged(object sender, EventArgs e)
        {
            Search();
        }

        private void btnSeleccionar_Click(object sender, EventArgs e)
        {
            Seleccionar();
        }

        private void btnVerTodos_Click(object sender, EventArgs e)
        {
            Search(true);
        }

        private void dgvProveedor_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Seleccionar();
            }
        }

        private void dgvProveedor_Paint(object sender, PaintEventArgs e)
        {
            ControlHelper.DgvSetColorBorder(sender, e);
        }
        private void btnCerrar_Click(object sender, EventArgs e)
        {
            CerrarForm();
        }
        

        private void metroCheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            Search();
        }
    #endregion
    }
}
