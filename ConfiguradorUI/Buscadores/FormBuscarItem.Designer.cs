﻿namespace ConfiguradorUI.Buscadores
{
    partial class FormBuscarItem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormBuscarItem));
            this.btnCerrar = new MetroFramework.Controls.MetroLink();
            this.lblNombreForm = new System.Windows.Forms.Label();
            this.gbxFiltro = new System.Windows.Forms.GroupBox();
            this.txtDescripcionProd = new MetroFramework.Controls.MetroTextBox();
            this.btnVerTodos = new System.Windows.Forms.Button();
            this.btnSeleccionar = new System.Windows.Forms.Button();
            this.txtCodigo = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.txtCodigo02 = new MetroFramework.Controls.MetroTextBox();
            this.chkIncluirInactivos = new MetroFramework.Controls.MetroCheckBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.dgvProd = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cboTExist = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.cboType = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.cboModel = new MetroFramework.Controls.MetroComboBox();
            this.cboMar = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.cboSubfam = new MetroFramework.Controls.MetroComboBox();
            this.cboFam = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.cboClass = new MetroFramework.Controls.MetroComboBox();
            this.cboGroup = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.gbxFiltro.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProd)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCerrar
            // 
            this.btnCerrar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCerrar.Image = ((System.Drawing.Image)(resources.GetObject("btnCerrar.Image")));
            this.btnCerrar.ImageSize = 48;
            this.btnCerrar.Location = new System.Drawing.Point(23, 21);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(56, 57);
            this.btnCerrar.TabIndex = 76;
            this.btnCerrar.UseSelectable = true;
            // 
            // lblNombreForm
            // 
            this.lblNombreForm.AutoSize = true;
            this.lblNombreForm.Font = new System.Drawing.Font("Segoe UI Semilight", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombreForm.ForeColor = System.Drawing.SystemColors.Highlight;
            this.lblNombreForm.Location = new System.Drawing.Point(85, 32);
            this.lblNombreForm.Name = "lblNombreForm";
            this.lblNombreForm.Size = new System.Drawing.Size(182, 32);
            this.lblNombreForm.TabIndex = 77;
            this.lblNombreForm.Text = "Seleccionar item";
            // 
            // gbxFiltro
            // 
            this.gbxFiltro.Controls.Add(this.txtDescripcionProd);
            this.gbxFiltro.Controls.Add(this.btnVerTodos);
            this.gbxFiltro.Controls.Add(this.btnSeleccionar);
            this.gbxFiltro.Controls.Add(this.txtCodigo);
            this.gbxFiltro.Controls.Add(this.metroLabel1);
            this.gbxFiltro.Controls.Add(this.txtCodigo02);
            this.gbxFiltro.Controls.Add(this.chkIncluirInactivos);
            this.gbxFiltro.Controls.Add(this.metroLabel2);
            this.gbxFiltro.Controls.Add(this.metroLabel3);
            this.gbxFiltro.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxFiltro.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.gbxFiltro.Location = new System.Drawing.Point(23, 90);
            this.gbxFiltro.Name = "gbxFiltro";
            this.gbxFiltro.Size = new System.Drawing.Size(834, 91);
            this.gbxFiltro.TabIndex = 74;
            this.gbxFiltro.TabStop = false;
            this.gbxFiltro.Text = "Panel de filtros";
            // 
            // txtDescripcionProd
            // 
            this.txtDescripcionProd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(253)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.txtDescripcionProd.CustomButton.Image = null;
            this.txtDescripcionProd.CustomButton.Location = new System.Drawing.Point(618, 1);
            this.txtDescripcionProd.CustomButton.Name = "";
            this.txtDescripcionProd.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtDescripcionProd.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtDescripcionProd.CustomButton.TabIndex = 1;
            this.txtDescripcionProd.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtDescripcionProd.CustomButton.UseSelectable = true;
            this.txtDescripcionProd.CustomButton.Visible = false;
            this.txtDescripcionProd.Lines = new string[0];
            this.txtDescripcionProd.Location = new System.Drawing.Point(91, 53);
            this.txtDescripcionProd.MaxLength = 32767;
            this.txtDescripcionProd.Name = "txtDescripcionProd";
            this.txtDescripcionProd.PasswordChar = '\0';
            this.txtDescripcionProd.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtDescripcionProd.SelectedText = "";
            this.txtDescripcionProd.SelectionLength = 0;
            this.txtDescripcionProd.SelectionStart = 0;
            this.txtDescripcionProd.ShortcutsEnabled = true;
            this.txtDescripcionProd.Size = new System.Drawing.Size(640, 23);
            this.txtDescripcionProd.TabIndex = 3;
            this.txtDescripcionProd.UseCustomBackColor = true;
            this.txtDescripcionProd.UseSelectable = true;
            this.txtDescripcionProd.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtDescripcionProd.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtDescripcionProd.TextChanged += new System.EventHandler(this.txtDescripcionProd_TextChanged);
            // 
            // btnVerTodos
            // 
            this.btnVerTodos.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.btnVerTodos.FlatAppearance.BorderSize = 0;
            this.btnVerTodos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVerTodos.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVerTodos.ForeColor = System.Drawing.Color.White;
            this.btnVerTodos.Location = new System.Drawing.Point(737, 24);
            this.btnVerTodos.Name = "btnVerTodos";
            this.btnVerTodos.Size = new System.Drawing.Size(87, 25);
            this.btnVerTodos.TabIndex = 5;
            this.btnVerTodos.Text = "Ver todos";
            this.btnVerTodos.UseVisualStyleBackColor = false;
            this.btnVerTodos.Click += new System.EventHandler(this.btnVerTodos_Click);
            // 
            // btnSeleccionar
            // 
            this.btnSeleccionar.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnSeleccionar.FlatAppearance.BorderSize = 0;
            this.btnSeleccionar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSeleccionar.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSeleccionar.ForeColor = System.Drawing.Color.White;
            this.btnSeleccionar.Location = new System.Drawing.Point(737, 51);
            this.btnSeleccionar.Name = "btnSeleccionar";
            this.btnSeleccionar.Size = new System.Drawing.Size(87, 25);
            this.btnSeleccionar.TabIndex = 4;
            this.btnSeleccionar.Text = "Seleccionar";
            this.btnSeleccionar.UseVisualStyleBackColor = false;
            this.btnSeleccionar.Click += new System.EventHandler(this.btnSeleccionar_Click);
            // 
            // txtCodigo
            // 
            this.txtCodigo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(253)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.txtCodigo.CustomButton.Image = null;
            this.txtCodigo.CustomButton.Location = new System.Drawing.Point(115, 1);
            this.txtCodigo.CustomButton.Name = "";
            this.txtCodigo.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtCodigo.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtCodigo.CustomButton.TabIndex = 1;
            this.txtCodigo.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtCodigo.CustomButton.UseSelectable = true;
            this.txtCodigo.CustomButton.Visible = false;
            this.txtCodigo.Lines = new string[0];
            this.txtCodigo.Location = new System.Drawing.Point(91, 24);
            this.txtCodigo.MaxLength = 32767;
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.PasswordChar = '\0';
            this.txtCodigo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCodigo.SelectedText = "";
            this.txtCodigo.SelectionLength = 0;
            this.txtCodigo.SelectionStart = 0;
            this.txtCodigo.ShortcutsEnabled = true;
            this.txtCodigo.Size = new System.Drawing.Size(137, 23);
            this.txtCodigo.TabIndex = 0;
            this.txtCodigo.UseCustomBackColor = true;
            this.txtCodigo.UseSelectable = true;
            this.txtCodigo.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtCodigo.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtCodigo.TextChanged += new System.EventHandler(this.txtCodigo_TextChanged);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.ForeColor = System.Drawing.Color.Navy;
            this.metroLabel1.Location = new System.Drawing.Point(6, 26);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(72, 19);
            this.metroLabel1.TabIndex = 2;
            this.metroLabel1.Text = "Código 01:";
            this.metroLabel1.UseCustomForeColor = true;
            // 
            // txtCodigo02
            // 
            this.txtCodigo02.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(253)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.txtCodigo02.CustomButton.Image = null;
            this.txtCodigo02.CustomButton.Location = new System.Drawing.Point(115, 1);
            this.txtCodigo02.CustomButton.Name = "";
            this.txtCodigo02.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtCodigo02.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtCodigo02.CustomButton.TabIndex = 1;
            this.txtCodigo02.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtCodigo02.CustomButton.UseSelectable = true;
            this.txtCodigo02.CustomButton.Visible = false;
            this.txtCodigo02.Lines = new string[0];
            this.txtCodigo02.Location = new System.Drawing.Point(319, 24);
            this.txtCodigo02.MaxLength = 32767;
            this.txtCodigo02.Name = "txtCodigo02";
            this.txtCodigo02.PasswordChar = '\0';
            this.txtCodigo02.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCodigo02.SelectedText = "";
            this.txtCodigo02.SelectionLength = 0;
            this.txtCodigo02.SelectionStart = 0;
            this.txtCodigo02.ShortcutsEnabled = true;
            this.txtCodigo02.Size = new System.Drawing.Size(137, 23);
            this.txtCodigo02.TabIndex = 1;
            this.txtCodigo02.UseCustomBackColor = true;
            this.txtCodigo02.UseSelectable = true;
            this.txtCodigo02.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtCodigo02.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtCodigo02.TextChanged += new System.EventHandler(this.txtCodigo02_TextChanged);
            // 
            // chkIncluirInactivos
            // 
            this.chkIncluirInactivos.AutoSize = true;
            this.chkIncluirInactivos.FontSize = MetroFramework.MetroCheckBoxSize.Medium;
            this.chkIncluirInactivos.FontWeight = MetroFramework.MetroCheckBoxWeight.Light;
            this.chkIncluirInactivos.ForeColor = System.Drawing.Color.Navy;
            this.chkIncluirInactivos.Location = new System.Drawing.Point(619, 28);
            this.chkIncluirInactivos.Name = "chkIncluirInactivos";
            this.chkIncluirInactivos.Size = new System.Drawing.Size(112, 19);
            this.chkIncluirInactivos.TabIndex = 2;
            this.chkIncluirInactivos.Text = "Incluir Inactivos";
            this.chkIncluirInactivos.UseCustomForeColor = true;
            this.chkIncluirInactivos.UseSelectable = true;
            this.chkIncluirInactivos.Click += new System.EventHandler(this.chkIncluirInactivos_CheckedChanged);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.ForeColor = System.Drawing.Color.Navy;
            this.metroLabel2.Location = new System.Drawing.Point(239, 26);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(74, 19);
            this.metroLabel2.TabIndex = 6;
            this.metroLabel2.Text = "Código 02:";
            this.metroLabel2.UseCustomForeColor = true;
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.ForeColor = System.Drawing.Color.Navy;
            this.metroLabel3.Location = new System.Drawing.Point(6, 55);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(79, 19);
            this.metroLabel3.TabIndex = 4;
            this.metroLabel3.Text = "Descripción:";
            this.metroLabel3.UseCustomForeColor = true;
            // 
            // dgvProd
            // 
            this.dgvProd.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProd.Location = new System.Drawing.Point(23, 354);
            this.dgvProd.Name = "dgvProd";
            this.dgvProd.Size = new System.Drawing.Size(834, 331);
            this.dgvProd.TabIndex = 75;
            this.dgvProd.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvProd_CellDoubleClick);
            this.dgvProd.Paint += new System.Windows.Forms.PaintEventHandler(this.dgvProd_Paint);
            this.dgvProd.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvProd_KeyDown);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cboTExist);
            this.groupBox1.Controls.Add(this.metroLabel11);
            this.groupBox1.Controls.Add(this.cboType);
            this.groupBox1.Controls.Add(this.metroLabel10);
            this.groupBox1.Controls.Add(this.cboModel);
            this.groupBox1.Controls.Add(this.cboMar);
            this.groupBox1.Controls.Add(this.metroLabel9);
            this.groupBox1.Controls.Add(this.metroLabel8);
            this.groupBox1.Controls.Add(this.cboSubfam);
            this.groupBox1.Controls.Add(this.cboFam);
            this.groupBox1.Controls.Add(this.metroLabel7);
            this.groupBox1.Controls.Add(this.cboClass);
            this.groupBox1.Controls.Add(this.cboGroup);
            this.groupBox1.Controls.Add(this.metroLabel4);
            this.groupBox1.Controls.Add(this.metroLabel5);
            this.groupBox1.Controls.Add(this.metroLabel6);
            this.groupBox1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.groupBox1.Location = new System.Drawing.Point(23, 187);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(834, 150);
            this.groupBox1.TabIndex = 75;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filtros Por Categorías";
            // 
            // cboTExist
            // 
            this.cboTExist.FormattingEnabled = true;
            this.cboTExist.ItemHeight = 23;
            this.cboTExist.Location = new System.Drawing.Point(544, 111);
            this.cboTExist.Name = "cboTExist";
            this.cboTExist.Size = new System.Drawing.Size(280, 29);
            this.cboTExist.TabIndex = 19;
            this.cboTExist.UseSelectable = true;
            this.cboTExist.SelectedIndexChanged += new System.EventHandler(this.cboTExist_SelectedIndexChanged);
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.ForeColor = System.Drawing.Color.Navy;
            this.metroLabel11.Location = new System.Drawing.Point(418, 116);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(116, 19);
            this.metroLabel11.TabIndex = 18;
            this.metroLabel11.Text = "Tipo de Existencia:";
            this.metroLabel11.UseCustomForeColor = true;
            // 
            // cboType
            // 
            this.cboType.FormattingEnabled = true;
            this.cboType.ItemHeight = 23;
            this.cboType.Location = new System.Drawing.Point(91, 111);
            this.cboType.Name = "cboType";
            this.cboType.Size = new System.Drawing.Size(280, 29);
            this.cboType.TabIndex = 17;
            this.cboType.UseSelectable = true;
            this.cboType.SelectedIndexChanged += new System.EventHandler(this.cboType_SelectedIndexChanged);
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.ForeColor = System.Drawing.Color.Navy;
            this.metroLabel10.Location = new System.Drawing.Point(6, 116);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(38, 19);
            this.metroLabel10.TabIndex = 16;
            this.metroLabel10.Text = "Tipo:";
            this.metroLabel10.UseCustomForeColor = true;
            // 
            // cboModel
            // 
            this.cboModel.FormattingEnabled = true;
            this.cboModel.ItemHeight = 23;
            this.cboModel.Location = new System.Drawing.Point(655, 61);
            this.cboModel.Name = "cboModel";
            this.cboModel.Size = new System.Drawing.Size(169, 29);
            this.cboModel.TabIndex = 15;
            this.cboModel.UseSelectable = true;
            this.cboModel.SelectedIndexChanged += new System.EventHandler(this.cboModel_SelectedIndexChanged);
            // 
            // cboMar
            // 
            this.cboMar.FormattingEnabled = true;
            this.cboMar.ItemHeight = 23;
            this.cboMar.Location = new System.Drawing.Point(655, 26);
            this.cboMar.Name = "cboMar";
            this.cboMar.Size = new System.Drawing.Size(169, 29);
            this.cboMar.TabIndex = 14;
            this.cboMar.UseSelectable = true;
            this.cboMar.SelectedIndexChanged += new System.EventHandler(this.cboMar_SelectedIndexChanged);
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.ForeColor = System.Drawing.Color.Navy;
            this.metroLabel9.Location = new System.Drawing.Point(584, 67);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(58, 19);
            this.metroLabel9.TabIndex = 13;
            this.metroLabel9.Text = "Modelo:";
            this.metroLabel9.UseCustomForeColor = true;
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.ForeColor = System.Drawing.Color.Navy;
            this.metroLabel8.Location = new System.Drawing.Point(584, 32);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(49, 19);
            this.metroLabel8.TabIndex = 12;
            this.metroLabel8.Text = "Marca:";
            this.metroLabel8.UseCustomForeColor = true;
            // 
            // cboSubfam
            // 
            this.cboSubfam.FormattingEnabled = true;
            this.cboSubfam.ItemHeight = 23;
            this.cboSubfam.Location = new System.Drawing.Point(373, 61);
            this.cboSubfam.Name = "cboSubfam";
            this.cboSubfam.Size = new System.Drawing.Size(169, 29);
            this.cboSubfam.TabIndex = 11;
            this.cboSubfam.UseSelectable = true;
            this.cboSubfam.SelectedIndexChanged += new System.EventHandler(this.cboSubfam_SelectedIndexChanged);
            // 
            // cboFam
            // 
            this.cboFam.FormattingEnabled = true;
            this.cboFam.ItemHeight = 23;
            this.cboFam.Location = new System.Drawing.Point(373, 26);
            this.cboFam.Name = "cboFam";
            this.cboFam.Size = new System.Drawing.Size(169, 29);
            this.cboFam.TabIndex = 10;
            this.cboFam.UseSelectable = true;
            this.cboFam.SelectedIndexChanged += new System.EventHandler(this.cboFam_SelectedIndexChanged);
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.ForeColor = System.Drawing.Color.Navy;
            this.metroLabel7.Location = new System.Drawing.Point(291, 67);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(75, 19);
            this.metroLabel7.TabIndex = 9;
            this.metroLabel7.Text = "SubFamilia:";
            this.metroLabel7.UseCustomForeColor = true;
            // 
            // cboClass
            // 
            this.cboClass.FormattingEnabled = true;
            this.cboClass.ItemHeight = 23;
            this.cboClass.Location = new System.Drawing.Point(91, 62);
            this.cboClass.Name = "cboClass";
            this.cboClass.Size = new System.Drawing.Size(169, 29);
            this.cboClass.TabIndex = 8;
            this.cboClass.UseSelectable = true;
            this.cboClass.SelectedIndexChanged += new System.EventHandler(this.cboClass_SelectedIndexChanged);
            // 
            // cboGroup
            // 
            this.cboGroup.FormattingEnabled = true;
            this.cboGroup.ItemHeight = 23;
            this.cboGroup.Location = new System.Drawing.Point(91, 26);
            this.cboGroup.Name = "cboGroup";
            this.cboGroup.Size = new System.Drawing.Size(169, 29);
            this.cboGroup.TabIndex = 7;
            this.cboGroup.UseSelectable = true;
            this.cboGroup.SelectedIndexChanged += new System.EventHandler(this.cboGroup_SelectedIndexChanged);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.ForeColor = System.Drawing.Color.Navy;
            this.metroLabel4.Location = new System.Drawing.Point(6, 32);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(49, 19);
            this.metroLabel4.TabIndex = 2;
            this.metroLabel4.Text = "Grupo:";
            this.metroLabel4.UseCustomForeColor = true;
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.ForeColor = System.Drawing.Color.Navy;
            this.metroLabel5.Location = new System.Drawing.Point(291, 32);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(53, 19);
            this.metroLabel5.TabIndex = 6;
            this.metroLabel5.Text = "Familia:";
            this.metroLabel5.UseCustomForeColor = true;
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.ForeColor = System.Drawing.Color.Navy;
            this.metroLabel6.Location = new System.Drawing.Point(6, 67);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(43, 19);
            this.metroLabel6.TabIndex = 4;
            this.metroLabel6.Text = "Clase:";
            this.metroLabel6.UseCustomForeColor = true;
            // 
            // FormBuscarItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(880, 708);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnCerrar);
            this.Controls.Add(this.lblNombreForm);
            this.Controls.Add(this.gbxFiltro);
            this.Controls.Add(this.dgvProd);
            this.Name = "FormBuscarItem";
            this.Load += new System.EventHandler(this.FormBuscarItem_Load);
            this.gbxFiltro.ResumeLayout(false);
            this.gbxFiltro.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProd)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLink btnCerrar;
        private System.Windows.Forms.Label lblNombreForm;
        private System.Windows.Forms.GroupBox gbxFiltro;
        private MetroFramework.Controls.MetroTextBox txtDescripcionProd;
        private System.Windows.Forms.Button btnVerTodos;
        private System.Windows.Forms.Button btnSeleccionar;
        private MetroFramework.Controls.MetroTextBox txtCodigo;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox txtCodigo02;
        private MetroFramework.Controls.MetroCheckBox chkIncluirInactivos;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private System.Windows.Forms.DataGridView dgvProd;
        private System.Windows.Forms.GroupBox groupBox1;
        private MetroFramework.Controls.MetroComboBox cboTExist;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private MetroFramework.Controls.MetroComboBox cboType;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroComboBox cboModel;
        private MetroFramework.Controls.MetroComboBox cboMar;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroComboBox cboSubfam;
        private MetroFramework.Controls.MetroComboBox cboFam;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroComboBox cboClass;
        private MetroFramework.Controls.MetroComboBox cboGroup;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel6;
    }
}