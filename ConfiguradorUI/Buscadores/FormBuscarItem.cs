﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ConfigBusinessEntity;
using ConfigBusinessLogic;
using ConfigBusinessLogic.Producto;
using ConfigUtilitarios;
using ConfigUtilitarios.HelperControl;
using MetroFramework.Forms;

namespace ConfiguradorUI.Buscadores
{
    public partial class FormBuscarItem : MetroForm
    {

        #region Variables
        public INVt09_item item = null;
        #endregion
        public FormBuscarItem()
        {
            InitializeComponent();
        }

        #region Métodos

        void AddHandled()
        {
            //Form
            KeyPreview = true;
            KeyDown += ControlHelper.FormCloseShiftEsc_KeyDown;

            txtCodigo.KeyDown += FocusDgv_KeyDown;
            txtCodigo02.KeyDown += FocusDgv_KeyDown;
            txtDescripcionProd.KeyDown += FocusDgv_KeyDown;
        }

        private void FocusDgv_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down || e.KeyCode == Keys.Up)
            {
                dgvProd.Focus();
            }
        }

        private void CargarGrid(IEnumerable<INVt09_item> list)
        {
            if (list != null)
            {
                dgvProd.DataSource = list.Select(x => new
                {
                    ID = x.id_item,
                    CODIGO = x.cod_item,
                    CODIGO02 = x.cod_item2,
                    DESCRIPCION = x.txt_desc?.ToUpper(),
                    PVPU_CON_IGV = x.mto_pcpu_con_tax.RemoveTrailingZeros(),
                    PVPU_SIN_IGV = x.mto_pcpu_sin_tax,
                    ESTADO = x.txt_estado?.ToUpper(),
                    POR_IMPTO = x.tax_por_tot,
                    PESO = x.peso_item,
                    UM = x.id_um

                }).ToList();
            }
            else
            {
                var prodHeader = new List<TNSt05_comp_emitido_dtl>();
                dgvProd.DataSource = prodHeader.Select(x => new
                {
                    ID = "",
                    CODIGO = "",
                    CODIGO02 = "",
                    DESCRIPCION = "",
                    PVPU_CON_IGV = "",
                    PVPU_SIN_IGV = "",
                    ESTADO = "",
                    POR_IMPTO = "",
                    PESO = "",
                    UM = ""
                }).ToList();
            }
            DefinirCabeceraGrid();
        }

        private void LoadCbos()
        {
            cboFam.DataSource = null;
            cboFam.DisplayMember = "txt_desc";
            cboFam.ValueMember = "id_familia";
            cboFam.DataSource = new FamiliaBL().ListaFamiliaProd(Estado.IdActivo, false, true);

            cboMar.DataSource = null;
            cboMar.DisplayMember = "txt_desc";
            cboMar.ValueMember = "id_marca";
            cboMar.DataSource = new MarcaBL().ListaMarca(Estado.IdActivo, false, true);

            cboType.DataSource = null;
            cboType.DisplayMember = "txt_desc";
            cboType.ValueMember = "id_tipo_prod";
            cboType.DataSource = new TipoProdBL().ListaTipoProd(Estado.IdActivo, false, true);

            cboTExist.DataSource = null;
            cboTExist.DisplayMember = "txt_desc";
            cboTExist.ValueMember = "id_tipo_existencia";
            cboTExist.DataSource = new TipoExistenciaBL().ListaTipoExistencia(Estado.IdActivo, true);

            cboGroup.DataSource = null;
            cboGroup.DisplayMember = "txt_desc";
            cboGroup.ValueMember = "id_grupo_prod";
            cboGroup.DataSource = new GrupoProdBL().ListaGrupoProd(Estado.IdActivo, false, true);
        }

        private void BuscarItem(bool verTodos = false)
        {
            var cod = txtCodigo.Text.Trim();
            var cod02 = txtCodigo02.Text.Trim();
            var nombre = txtDescripcionProd.Text.Trim();
            int? estado = chkIncluirInactivos.Checked ? Estado.Ignorar : Estado.IdActivo;
            var idClase = (cboClass.SelectedValue != null) ? Convert.ToInt32(cboClass.SelectedValue) : Estado.Ignorar;
            var idSubFam = (cboSubfam.SelectedValue != null) ? Convert.ToInt32(cboSubfam.SelectedValue) : Estado.Ignorar;
            var idModelo = (cboModel.SelectedValue != null) ? Convert.ToInt32(cboModel.SelectedValue) : Estado.Ignorar;
            var idTipo = (cboType.SelectedValue != null) ? Convert.ToInt32(cboType.SelectedValue) : Estado.Ignorar;
            var idTExistencia = (cboTExist.SelectedValue != null) ? Convert.ToInt32(cboTExist.SelectedValue) : Estado.Ignorar;
            
            if (verTodos)
            {
                cod = "";
                cod02 = "";
                nombre = "";
                estado = Estado.Ignorar;
                idClase = Estado.Ignorar;
                idSubFam = Estado.Ignorar;
                idModelo = Estado.Ignorar;
                idTipo = Estado.Ignorar;
                idTExistencia = Estado.Ignorar;
            }

            var list = new ProductoItemBL().BuscarItem(cod, cod02, nombre,idModelo,idTExistencia,idSubFam,idTipo,idClase, estado);
            CargarGrid(list);
        }

        private void SetInicio()
        {
            AddHandled();
            ConfigurarControles();
            LoadCbos();
            LimpiarForm();
            BuscarItem();
            txtDescripcionProd.Focus();
        }
        private void LimpiarForm()
        {
            txtCodigo.Clear();
            txtCodigo02.Clear();
            txtDescripcionProd.Clear();
            chkIncluirInactivos.Checked = false;

            cboGroup.SelectedValue = (cboGroup.Items.Count > 0) ? 0 : -1;
            cboClass.SelectedValue = (cboClass.Items.Count > 0) ? 0 : -1;
            cboFam.SelectedValue = (cboFam.Items.Count > 0) ? 0 : -1;
            cboSubfam.SelectedValue = (cboSubfam.Items.Count > 0) ? 0 : -1;
            cboMar.SelectedValue = (cboMar.Items.Count > 0) ? 0 : -1;
            cboModel.SelectedValue = (cboModel.Items.Count > 0) ? 0 : -1;
            cboType.SelectedValue = (cboType.Items.Count > 0) ? 0 : -1;
            cboTExist.SelectedValue = (cboTExist.Items.Count > 0) ? 0 : -1;
        }

        private void DefinirCabeceraGrid()
        {
            try
            {
                dgvProd.Columns["ID"].Visible = false;
                //dgvProd.Columns["PVPU_CON_IGV"].Visible = false;
                dgvProd.Columns["PVPU_SIN_IGV"].Visible = false;
                dgvProd.Columns["POR_IMPTO"].Visible = false;
                dgvProd.Columns["PESO"].Visible = false;
                dgvProd.Columns["UM"].Visible = false;

                dgvProd.Columns["CODIGO"].HeaderText = "CÓDIGO 01";
                dgvProd.Columns["CODIGO"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvProd.Columns["CODIGO"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                dgvProd.Columns["CODIGO02"].HeaderText = "CÓDIGO 02";
                dgvProd.Columns["CODIGO02"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvProd.Columns["CODIGO02"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                dgvProd.Columns["DESCRIPCION"].HeaderText = "DESCRIPCIÓN";

                dgvProd.Columns["PVPU_CON_IGV"].HeaderText = "PREC. UNIT";
                dgvProd.Columns["PVPU_CON_IGV"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvProd.Columns["PVPU_CON_IGV"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                dgvProd.Columns["ESTADO"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvProd.Columns["ESTADO"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                dgvProd.Columns["DESCRIPCION"].Width = 435;
                dgvProd.Columns["CODIGO"].Width = 100;
                dgvProd.Columns["CODIGO02"].Width = 100;
                dgvProd.Columns["PVPU_CON_IGV"].Width = 100;
                dgvProd.Columns["ESTADO"].Width = 90;
            }
            catch (Exception e)
            {
                MessageBox.Show($"No se pudo definir la cabecera de la grilla. Excepción: {e.Message}", "Excepción encontrada", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ConfigurarControles()
        {
            #region Form
            MaximizeBox = false;
            Resizable = false;
            #endregion

            #region Checks

            chkIncluirInactivos.Checked = false;

            #endregion

            #region TextBox

            txtCodigo.MaxLength = 50;
            txtCodigo02.MaxLength = 50;
            txtDescripcionProd.MaxLength = 350;

            #endregion

            #region Grilla
            var prodHeader = new List<TNSt05_comp_emitido_dtl>();
            dgvProd.DataSource = prodHeader.Select(x => new
            {
                ID = "",
                CODIGO = "",
                CODIGO02 = "",
                DESCRIPCION = "",
                PVPU_CON_IGV = "",
                PVPU_SIN_IGV = "",
                ESTADO = "",
                POR_IMPTO = "",
                PESO = "",
                UM = ""

            }).ToList();
            DefinirCabeceraGrid();
            ControlHelper.DgvReadOnly(dgvProd);
            ControlHelper.DgvStyle(dgvProd);
            #endregion

        }

        private INVt09_item GetItem()
        {
            try
            {
                var prod = new INVt09_item()
                {
                    id_item = long.Parse(dgvProd.Rows[dgvProd.CurrentRow.Index].Cells["ID"].Value.ToString()),
                    cod_item = dgvProd.Rows[dgvProd.CurrentRow.Index].Cells["CODIGO"].Value.ToString(),
                    cod_item2 = dgvProd.Rows[dgvProd.CurrentRow.Index].Cells["CODIGO02"].Value.ToString(),
                    txt_desc = dgvProd.Rows[dgvProd.CurrentRow.Index].Cells["DESCRIPCION"].Value.ToString(),
                    txt_estado = dgvProd.Rows[dgvProd.CurrentRow.Index].Cells["ESTADO"].Value.ToString(),
                    peso_item = dgvProd.Rows[dgvProd.CurrentRow.Index].Cells["PESO"].Value.ToString()
                };

                var pvpu_con_igv = dgvProd.Rows[dgvProd.CurrentRow.Index].Cells["PVPU_CON_IGV"].Value;
                var pvpu_sin_igv = dgvProd.Rows[dgvProd.CurrentRow.Index].Cells["PVPU_SIN_IGV"].Value;
                var por_impto = dgvProd.Rows[dgvProd.CurrentRow.Index].Cells["POR_IMPTO"].Value;
                var id_um = dgvProd.Rows[dgvProd.CurrentRow.Index].Cells["UM"].Value;

                if (pvpu_con_igv == null) prod.mto_pcpu_con_tax = null; else prod.mto_pcpu_con_tax = decimal.Parse(pvpu_con_igv.ToString());
                if (pvpu_sin_igv == null) prod.mto_pcpu_sin_tax = null; else prod.mto_pcpu_sin_tax = decimal.Parse(pvpu_sin_igv.ToString());
                 prod.tax_por_tot = decimal.Parse(por_impto.ToString());
                if (id_um == null) prod.id_um = null; else prod.id_um = int.Parse(id_um.ToString());

                return prod;
            }
            catch (Exception)
            {
                Msg.Ok_Err("No se pudo obtener el item.", "Excepción encontrada");
            }
            return null;
        }

        private void SeleccionarItem()
        {
            if (dgvProd.CurrentRow != null)
            {
                try
                {
                    var estado = dgvProd.Rows[dgvProd.CurrentRow.Index].Cells["ESTADO"].Value.ToString();
                    var desc = dgvProd.Rows[dgvProd.CurrentRow.Index].Cells["DESCRIPCION"].Value.ToString();

                    if (estado == Estado.TxtActivo)
                    {
                        item = GetItem();
                        CerrarForm();
                    }
                    else
                    {
                        if (DialogResult.OK == Msg.OkCancel_Info($@"No puede seleccionar el item '{desc}' porque NO ESTÁ ACTIVADO. ¿Desea activarlo y seleccionarlo?."))
                        {
                            if (long.TryParse(ControlHelper.DgvGetCellValueSelected(dgvProd, 0), out long id))
                            {
                                if (new ProductoItemBL().ActivarItem(id))
                                {
                                    item = GetItem();
                                    if (item != null)
                                    {
                                        item.id_estado = Estado.IdActivo;
                                        item.txt_estado = Estado.TxtActivo;
                                    }
                                    CerrarForm();
                                }
                                else
                                    Msg.Ok_Err($"No se pudo activar el item '{desc}'.");
                            }
                            else
                                Msg.Ok_Err($"No se pudo activar el item '{desc}'.");
                        }
                    }
                }
                catch (Exception e)
                {
                    item = null;
                    MessageBox.Show($"No se pudo seleccionar el item. Excepción: {e.Message}", "Excepción encontrada", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void CerrarForm()
        {
            Hide();
            Close();
        }


        #endregion

        #region Eventos

      
        private void FormBuscarItem_Load(object sender, EventArgs e)
        {
            SetInicio();
        }
        private void txtCodigo_TextChanged(object sender, EventArgs e)
        {
            BuscarItem();
        }

        private void txtCodigo02_TextChanged(object sender, EventArgs e)
        {
            BuscarItem();
        }

        private void txtDescripcionProd_TextChanged(object sender, EventArgs e)
        {
            BuscarItem();
        }

        private void dgvProd_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            SeleccionarItem();
        }

        private void chkIncluirInactivos_CheckedChanged(object sender, EventArgs e)
        {
            BuscarItem();
        }

        private void chkProdVenta_CheckedChanged(object sender, EventArgs e)
        {
            BuscarItem();
        }

        private void chkProdCompra_CheckedChanged(object sender, EventArgs e)
        {
            BuscarItem();
        }

        private void btnSeleccionar_Click(object sender, EventArgs e)
        {
            SeleccionarItem();
        }

        private void btnVerTodos_Click(object sender, EventArgs e)
        {
            BuscarItem(true);
        }

        private void dgvProd_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SeleccionarItem();
            }
        }

        private void dgvProd_Paint(object sender, PaintEventArgs e)
        {
            ControlHelper.DgvSetColorBorder(sender, e);
        }


        private void cboGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboGroup.SelectedValue != null)
            {
                cboClass.Enabled = true;
                cboClass.DataSource = null;
                cboClass.DisplayMember = "txt_desc";
                cboClass.ValueMember = "id_clase_prod";
                cboClass.DataSource = (cboGroup.SelectedValue != null) ? new ClaseProdBL().ListaClaseProdXGrupo(int.Parse(cboGroup.SelectedValue.ToString()), Estado.IdActivo) : null;
            }
            else cboClass.DataSource = null;

            cboClass.DropDownWidth = ControlHelper.DropDownWidth(cboClass);
        }

        private void cboFam_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboFam.SelectedValue != null)
            {
                cboSubfam.Enabled = true;
                cboSubfam.DataSource = null;
                cboSubfam.DisplayMember = "txt_desc";
                cboSubfam.ValueMember = "id_subfamilia";
                cboSubfam.DataSource = (cboFam.SelectedValue != null) ? new SubFamiliaBL().ListaSubFamXFam(int.Parse(cboFam.SelectedValue.ToString()), Estado.IdActivo) : null;
            }
            else cboSubfam.DataSource = null;

            cboSubfam.DropDownWidth = ControlHelper.DropDownWidth(cboSubfam);
        }

        private void cboMar_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboMar.SelectedValue != null)
            {
                cboModel.Enabled = true;
                cboModel.DataSource = null;
                cboModel.DisplayMember = "txt_desc";
                cboModel.ValueMember = "id_modelo";
                cboModel.DataSource = (cboMar.SelectedValue != null) ? new ModeloBL().ListaModeloXMarca(int.Parse(cboMar.SelectedValue.ToString()), Estado.IdActivo) : null;
            }
            else cboModel.DataSource = null;

            cboModel.DropDownWidth = ControlHelper.DropDownWidth(cboModel);
        }
        

        private void cboClass_SelectedIndexChanged(object sender, EventArgs e)
        {
            BuscarItem();
        }

        private void cboSubfam_SelectedIndexChanged(object sender, EventArgs e)
        {
            BuscarItem();
        }

        private void cboModel_SelectedIndexChanged(object sender, EventArgs e)
        {
            BuscarItem();
        }

        private void cboType_SelectedIndexChanged(object sender, EventArgs e)
        {
            BuscarItem();
        }

        private void cboTExist_SelectedIndexChanged(object sender, EventArgs e)
        {
            BuscarItem();
        }
        #endregion
    }
}
