﻿namespace ConfiguradorUI.Buscadores
{
    partial class FormBuscarProveedor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormBuscarProveedor));
            this.btnCerrar = new MetroFramework.Controls.MetroLink();
            this.lblNombreForm = new System.Windows.Forms.Label();
            this.gbxFiltro = new System.Windows.Forms.GroupBox();
            this.txtnroRuc = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.txtNombre = new MetroFramework.Controls.MetroTextBox();
            this.btnVerTodos = new System.Windows.Forms.Button();
            this.btnSeleccionar = new System.Windows.Forms.Button();
            this.txtCodigo = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.txtnroDoc = new MetroFramework.Controls.MetroTextBox();
            this.chkIncluirInactivos = new MetroFramework.Controls.MetroCheckBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.dgvProveedor = new System.Windows.Forms.DataGridView();
            this.chkSnCat = new MetroFramework.Controls.MetroCheckBox();
            this.gbxFiltro.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProveedor)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCerrar
            // 
            this.btnCerrar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCerrar.Image = ((System.Drawing.Image)(resources.GetObject("btnCerrar.Image")));
            this.btnCerrar.ImageSize = 48;
            this.btnCerrar.Location = new System.Drawing.Point(23, 21);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(56, 57);
            this.btnCerrar.TabIndex = 76;
            this.btnCerrar.UseSelectable = true;
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // lblNombreForm
            // 
            this.lblNombreForm.AutoSize = true;
            this.lblNombreForm.Font = new System.Drawing.Font("Segoe UI Semilight", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombreForm.ForeColor = System.Drawing.SystemColors.Highlight;
            this.lblNombreForm.Location = new System.Drawing.Point(85, 32);
            this.lblNombreForm.Name = "lblNombreForm";
            this.lblNombreForm.Size = new System.Drawing.Size(244, 32);
            this.lblNombreForm.TabIndex = 77;
            this.lblNombreForm.Text = "Seleccionar proveedor";
            // 
            // gbxFiltro
            // 
            this.gbxFiltro.Controls.Add(this.chkSnCat);
            this.gbxFiltro.Controls.Add(this.txtnroRuc);
            this.gbxFiltro.Controls.Add(this.metroLabel4);
            this.gbxFiltro.Controls.Add(this.txtNombre);
            this.gbxFiltro.Controls.Add(this.btnVerTodos);
            this.gbxFiltro.Controls.Add(this.btnSeleccionar);
            this.gbxFiltro.Controls.Add(this.txtCodigo);
            this.gbxFiltro.Controls.Add(this.metroLabel1);
            this.gbxFiltro.Controls.Add(this.txtnroDoc);
            this.gbxFiltro.Controls.Add(this.chkIncluirInactivos);
            this.gbxFiltro.Controls.Add(this.metroLabel2);
            this.gbxFiltro.Controls.Add(this.metroLabel3);
            this.gbxFiltro.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxFiltro.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.gbxFiltro.Location = new System.Drawing.Point(23, 84);
            this.gbxFiltro.Name = "gbxFiltro";
            this.gbxFiltro.Size = new System.Drawing.Size(834, 91);
            this.gbxFiltro.TabIndex = 74;
            this.gbxFiltro.TabStop = false;
            this.gbxFiltro.Text = "Panel de filtros";
            // 
            // txtnroRuc
            // 
            this.txtnroRuc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(253)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.txtnroRuc.CustomButton.Image = null;
            this.txtnroRuc.CustomButton.Location = new System.Drawing.Point(88, 1);
            this.txtnroRuc.CustomButton.Name = "";
            this.txtnroRuc.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtnroRuc.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtnroRuc.CustomButton.TabIndex = 1;
            this.txtnroRuc.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtnroRuc.CustomButton.UseSelectable = true;
            this.txtnroRuc.CustomButton.Visible = false;
            this.txtnroRuc.Lines = new string[0];
            this.txtnroRuc.Location = new System.Drawing.Point(494, 24);
            this.txtnroRuc.MaxLength = 32767;
            this.txtnroRuc.Name = "txtnroRuc";
            this.txtnroRuc.PasswordChar = '\0';
            this.txtnroRuc.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtnroRuc.SelectedText = "";
            this.txtnroRuc.SelectionLength = 0;
            this.txtnroRuc.SelectionStart = 0;
            this.txtnroRuc.ShortcutsEnabled = true;
            this.txtnroRuc.Size = new System.Drawing.Size(110, 23);
            this.txtnroRuc.TabIndex = 7;
            this.txtnroRuc.UseCustomBackColor = true;
            this.txtnroRuc.UseSelectable = true;
            this.txtnroRuc.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtnroRuc.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtnroRuc.Click += new System.EventHandler(this.txtnroRuc_Click);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.ForeColor = System.Drawing.Color.Navy;
            this.metroLabel4.Location = new System.Drawing.Point(427, 26);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(68, 19);
            this.metroLabel4.TabIndex = 8;
            this.metroLabel4.Text = "Nro. RUC:";
            this.metroLabel4.UseCustomForeColor = true;
            // 
            // txtNombre
            // 
            this.txtNombre.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(253)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.txtNombre.CustomButton.Image = null;
            this.txtNombre.CustomButton.Location = new System.Drawing.Point(618, 1);
            this.txtNombre.CustomButton.Name = "";
            this.txtNombre.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtNombre.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtNombre.CustomButton.TabIndex = 1;
            this.txtNombre.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtNombre.CustomButton.UseSelectable = true;
            this.txtNombre.CustomButton.Visible = false;
            this.txtNombre.Lines = new string[0];
            this.txtNombre.Location = new System.Drawing.Point(91, 53);
            this.txtNombre.MaxLength = 32767;
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.PasswordChar = '\0';
            this.txtNombre.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNombre.SelectedText = "";
            this.txtNombre.SelectionLength = 0;
            this.txtNombre.SelectionStart = 0;
            this.txtNombre.ShortcutsEnabled = true;
            this.txtNombre.Size = new System.Drawing.Size(640, 23);
            this.txtNombre.TabIndex = 3;
            this.txtNombre.UseCustomBackColor = true;
            this.txtNombre.UseSelectable = true;
            this.txtNombre.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtNombre.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtNombre.Click += new System.EventHandler(this.txtNombre_Click);
            // 
            // btnVerTodos
            // 
            this.btnVerTodos.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.btnVerTodos.FlatAppearance.BorderSize = 0;
            this.btnVerTodos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVerTodos.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVerTodos.ForeColor = System.Drawing.Color.White;
            this.btnVerTodos.Location = new System.Drawing.Point(737, 24);
            this.btnVerTodos.Name = "btnVerTodos";
            this.btnVerTodos.Size = new System.Drawing.Size(87, 25);
            this.btnVerTodos.TabIndex = 5;
            this.btnVerTodos.Text = "Ver todos";
            this.btnVerTodos.UseVisualStyleBackColor = false;
            this.btnVerTodos.Click += new System.EventHandler(this.btnVerTodos_Click);
            // 
            // btnSeleccionar
            // 
            this.btnSeleccionar.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnSeleccionar.FlatAppearance.BorderSize = 0;
            this.btnSeleccionar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSeleccionar.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSeleccionar.ForeColor = System.Drawing.Color.White;
            this.btnSeleccionar.Location = new System.Drawing.Point(737, 51);
            this.btnSeleccionar.Name = "btnSeleccionar";
            this.btnSeleccionar.Size = new System.Drawing.Size(87, 25);
            this.btnSeleccionar.TabIndex = 4;
            this.btnSeleccionar.Text = "Seleccionar";
            this.btnSeleccionar.UseVisualStyleBackColor = false;
            this.btnSeleccionar.Click += new System.EventHandler(this.btnSeleccionar_Click);
            // 
            // txtCodigo
            // 
            this.txtCodigo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(253)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.txtCodigo.CustomButton.Image = null;
            this.txtCodigo.CustomButton.Location = new System.Drawing.Point(115, 1);
            this.txtCodigo.CustomButton.Name = "";
            this.txtCodigo.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtCodigo.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtCodigo.CustomButton.TabIndex = 1;
            this.txtCodigo.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtCodigo.CustomButton.UseSelectable = true;
            this.txtCodigo.CustomButton.Visible = false;
            this.txtCodigo.Lines = new string[0];
            this.txtCodigo.Location = new System.Drawing.Point(91, 24);
            this.txtCodigo.MaxLength = 32767;
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.PasswordChar = '\0';
            this.txtCodigo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCodigo.SelectedText = "";
            this.txtCodigo.SelectionLength = 0;
            this.txtCodigo.SelectionStart = 0;
            this.txtCodigo.ShortcutsEnabled = true;
            this.txtCodigo.Size = new System.Drawing.Size(137, 23);
            this.txtCodigo.TabIndex = 0;
            this.txtCodigo.UseCustomBackColor = true;
            this.txtCodigo.UseSelectable = true;
            this.txtCodigo.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtCodigo.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtCodigo.Click += new System.EventHandler(this.txtCodigo_Click);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.ForeColor = System.Drawing.Color.Navy;
            this.metroLabel1.Location = new System.Drawing.Point(6, 26);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(56, 19);
            this.metroLabel1.TabIndex = 2;
            this.metroLabel1.Text = "Código:";
            this.metroLabel1.UseCustomForeColor = true;
            // 
            // txtnroDoc
            // 
            this.txtnroDoc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(253)))), ((int)(((byte)(255)))));
            // 
            // 
            // 
            this.txtnroDoc.CustomButton.Image = null;
            this.txtnroDoc.CustomButton.Location = new System.Drawing.Point(88, 1);
            this.txtnroDoc.CustomButton.Name = "";
            this.txtnroDoc.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtnroDoc.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtnroDoc.CustomButton.TabIndex = 1;
            this.txtnroDoc.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtnroDoc.CustomButton.UseSelectable = true;
            this.txtnroDoc.CustomButton.Visible = false;
            this.txtnroDoc.Lines = new string[0];
            this.txtnroDoc.Location = new System.Drawing.Point(306, 24);
            this.txtnroDoc.MaxLength = 32767;
            this.txtnroDoc.Name = "txtnroDoc";
            this.txtnroDoc.PasswordChar = '\0';
            this.txtnroDoc.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtnroDoc.SelectedText = "";
            this.txtnroDoc.SelectionLength = 0;
            this.txtnroDoc.SelectionStart = 0;
            this.txtnroDoc.ShortcutsEnabled = true;
            this.txtnroDoc.Size = new System.Drawing.Size(110, 23);
            this.txtnroDoc.TabIndex = 1;
            this.txtnroDoc.UseCustomBackColor = true;
            this.txtnroDoc.UseSelectable = true;
            this.txtnroDoc.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtnroDoc.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtnroDoc.Click += new System.EventHandler(this.txtnroDoc_Click);
            // 
            // chkIncluirInactivos
            // 
            this.chkIncluirInactivos.AutoSize = true;
            this.chkIncluirInactivos.FontSize = MetroFramework.MetroCheckBoxSize.Medium;
            this.chkIncluirInactivos.FontWeight = MetroFramework.MetroCheckBoxWeight.Light;
            this.chkIncluirInactivos.ForeColor = System.Drawing.Color.Navy;
            this.chkIncluirInactivos.Location = new System.Drawing.Point(619, 13);
            this.chkIncluirInactivos.Name = "chkIncluirInactivos";
            this.chkIncluirInactivos.Size = new System.Drawing.Size(112, 19);
            this.chkIncluirInactivos.TabIndex = 2;
            this.chkIncluirInactivos.Text = "Incluir Inactivos";
            this.chkIncluirInactivos.UseCustomForeColor = true;
            this.chkIncluirInactivos.UseSelectable = true;
            this.chkIncluirInactivos.CheckedChanged += new System.EventHandler(this.chkIncluirInactivos_CheckedChanged);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.ForeColor = System.Drawing.Color.Navy;
            this.metroLabel2.Location = new System.Drawing.Point(239, 26);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(65, 19);
            this.metroLabel2.TabIndex = 6;
            this.metroLabel2.Text = "Nro. Doc:";
            this.metroLabel2.UseCustomForeColor = true;
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.ForeColor = System.Drawing.Color.Navy;
            this.metroLabel3.Location = new System.Drawing.Point(6, 55);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(62, 19);
            this.metroLabel3.TabIndex = 4;
            this.metroLabel3.Text = "Nombre:";
            this.metroLabel3.UseCustomForeColor = true;
            // 
            // dgvProveedor
            // 
            this.dgvProveedor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProveedor.Location = new System.Drawing.Point(23, 181);
            this.dgvProveedor.Name = "dgvProveedor";
            this.dgvProveedor.Size = new System.Drawing.Size(834, 342);
            this.dgvProveedor.TabIndex = 75;
            this.dgvProveedor.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvProveedor_CellDoubleClick);
            this.dgvProveedor.Paint += new System.Windows.Forms.PaintEventHandler(this.dgvProveedor_Paint);
            this.dgvProveedor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvProveedor_KeyDown);
            // 
            // chkSnCat
            // 
            this.chkSnCat.AutoSize = true;
            this.chkSnCat.FontSize = MetroFramework.MetroCheckBoxSize.Medium;
            this.chkSnCat.FontWeight = MetroFramework.MetroCheckBoxWeight.Light;
            this.chkSnCat.ForeColor = System.Drawing.Color.Navy;
            this.chkSnCat.Location = new System.Drawing.Point(619, 30);
            this.chkSnCat.Name = "chkSnCat";
            this.chkSnCat.Size = new System.Drawing.Size(117, 19);
            this.chkSnCat.TabIndex = 9;
            this.chkSnCat.Text = "Incluir SN Catál.";
            this.chkSnCat.UseCustomForeColor = true;
            this.chkSnCat.UseSelectable = true;
            this.chkSnCat.CheckedChanged += new System.EventHandler(this.metroCheckBox1_CheckedChanged);
            // 
            // FormBuscarProveedor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(880, 545);
            this.Controls.Add(this.btnCerrar);
            this.Controls.Add(this.lblNombreForm);
            this.Controls.Add(this.gbxFiltro);
            this.Controls.Add(this.dgvProveedor);
            this.Name = "FormBuscarProveedor";
            this.Load += new System.EventHandler(this.FormBuscarProveedor_Load);
            this.gbxFiltro.ResumeLayout(false);
            this.gbxFiltro.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProveedor)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLink btnCerrar;
        private System.Windows.Forms.Label lblNombreForm;
        private System.Windows.Forms.GroupBox gbxFiltro;
        private MetroFramework.Controls.MetroTextBox txtNombre;
        private System.Windows.Forms.Button btnVerTodos;
        private System.Windows.Forms.Button btnSeleccionar;
        private MetroFramework.Controls.MetroTextBox txtCodigo;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox txtnroDoc;
        private MetroFramework.Controls.MetroCheckBox chkIncluirInactivos;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private System.Windows.Forms.DataGridView dgvProveedor;
        private MetroFramework.Controls.MetroTextBox txtnroRuc;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroCheckBox chkSnCat;
    }
}