﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConfigBusinessEntity;
using Dapper;
using System.Data.SqlClient;
using ConfigUtilitarios;
using System.Linq.Expressions;

namespace ConfigDataAccess
{
    public class UnidadMedidaDA
    {
        public List<SNTt06_unidad_medida> ListaUnidadMed(int? id_estado = null)
        {
            var lista = new List<SNTt06_unidad_medida>();
            string sentencia = string.Empty;
            sentencia = (id_estado == null) ?
                @"SELECT * FROM SNTt06_unidad_medida" :
                @"SELECT * FROM SNTt06_unidad_medida WHERE id_estado=@id_estado";
            using (var cnn = new SqlConnection(ConnectionManager.GetConnectionString()))
            {
                try
                {
                    cnn.Open();
                    lista = cnn.Query<SNTt06_unidad_medida>(sentencia, new { id_estado }).ToList();
                }
                catch (Exception e)
                {
                    var log = new Log();
                    log.ArchiveLog("Lista de Unidades de medida: ", e.Message);
                }
            }
            return lista;
        }

        public SNTt06_unidad_medida Add(SNTt06_unidad_medida model)
        {
            SNTt06_unidad_medida result = null;
            using (var ctx = new EagleContext(ConnectionManager.GetConnectionString()))
            {
                try
                {
                    result = ctx.SNTt06_unidad_medida.Add(model);
                    ctx.SaveChanges();
                }
                catch (Exception e)
                {
                    var log = new Log();
                    log.ArchiveLog("Error al agregar unidad: ", e.Message);
                }
            }
            return result;
        }

        public bool Edit(SNTt06_unidad_medida model)
        {
            bool response = false;
            using (var ctx = new EagleContext(ConnectionManager.GetConnectionString()))
            {
                try
                {
                    var original = ctx.SNTt06_unidad_medida.Find(model.id_um);
                    if (original != null)
                    {
                        ctx.Entry(original).CurrentValues.SetValues(model);
                        ctx.SaveChanges();
                        response = true;
                    }
                }
                catch (Exception e)
                {
                    var log = new Log();
                    log.ArchiveLog("Error al editar unidad: ", e.Message);
                }
            }
            return response;
        }

        public bool Activar(long id)
        {
            bool success = false;
            using (var cnn = new SqlConnection(ConnectionManager.GetConnectionString()))
            {
                try
                {
                    int id_estado = Estado.IdActivo;
                    string txt_estado = Estado.TxtActivo;
                    using (SqlCommand cmd = cnn.CreateCommand())
                    {
                        cmd.CommandText = "UPDATE SNTt06_unidad_medida SET id_estado = @id_estado, txt_estado = @txt_estado Where id_um=@id";
                        cmd.Parameters.AddWithValue("@id_estado", id_estado);
                        cmd.Parameters.AddWithValue("@txt_estado", txt_estado);
                        cmd.Parameters.AddWithValue("@id", id);
                        cnn.Open();
                        cmd.ExecuteNonQuery();
                        success = true;
                    }
                }
                catch (Exception e)
                {
                    var log = new Log();
                    log.ArchiveLog("Activar unidad de medida: ", e.Message);
                }
            }
            return success;
        }

        public SNTt06_unidad_medida Get(long id)
        {
            SNTt06_unidad_medida response = null;
            using (var ctx = new EagleContext(ConnectionManager.GetConnectionString()))
            {
                try
                {
                    response = ctx.SNTt06_unidad_medida.Find(id);
                }
                catch (Exception ex)
                {
                    var log = new Log();
                    log.ArchiveLog("Error en obtener producto por id", ex.Message);
                }
            }
            return response;
        }

        public SNTt06_unidad_medida Find(Expression<Func<SNTt06_unidad_medida, bool>> expression)
        {
            SNTt06_unidad_medida response = null;
            using (var ctx = new EagleContext(ConnectionManager.GetConnectionString()))
            {
                try
                {
                    response = ctx.SNTt06_unidad_medida.SingleOrDefault(expression);
                }
                catch (Exception ex)
                {
                    var log = new Log();
                    log.ArchiveLog("Error en buscar una unidad de medida", ex.Message);
                    throw;
                }
            }
            return response;
        }

        public IEnumerable<SNTt06_unidad_medida> FindAll(Expression<Func<SNTt06_unidad_medida, bool>> expression)
        {
            IEnumerable<SNTt06_unidad_medida> response = null;
            using (var ctx = new EagleContext(ConnectionManager.GetConnectionString()))
            {
                try
                {
                    response = ctx.SNTt06_unidad_medida.Where(expression).ToList();
                }
                catch (Exception ex)
                {
                    var log = new Log();
                    log.ArchiveLog("Error en buscar una lista de unidades de medida", ex.Message);
                    throw;
                }
            }
            return response;
        }

        public IEnumerable<SNTt06_unidad_medida> FindAllUnits(string codigo = "", string nombre = "", int? idEstado = null)
        {
            IEnumerable<SNTt06_unidad_medida> response = null;
            try
            {
                using (var ctx = new EagleContext(ConnectionManager.GetConnectionString()))
                {
                    response = ctx.SNTt06_unidad_medida.Where(u =>
                        (string.IsNullOrEmpty(codigo) ? true : u.cod_um.ToLower().Contains(codigo.ToLower())) &&
                        (string.IsNullOrEmpty(nombre) ? true : u.txt_desc.ToLower().Contains(nombre.ToLower())) &&
                        (idEstado == Estado.Ignorar ? true : u.id_estado == Estado.IdActivo))
                        .OrderBy(x => x.txt_desc)
                        .ToList();
                }
            }
            catch (Exception ex)
            {
                var log = new Log();
                log.ArchiveLog("Error en buscar lista de unidades de medida ", ex.Message);
            }
            return response;
        }

        public SNTt06_unidad_medida UnitByCode(string code)
        {
            SNTt06_unidad_medida response = null;
            try
            {
                using (var ctx = new EagleContext(ConnectionManager.GetConnectionString()))
                {
                    response = ctx.SNTt06_unidad_medida.SingleOrDefault(x => x.cod_um == code);
                }
            }
            catch (Exception ex)
            {
                var log = new Log();
                log.ArchiveLog("Error en buscar lista de unidades de medida ", ex.Message);
            }
            return response;
        }
        public int FindByUndbase(long idPadre)
        {
            var result = -1;
            using (var ctx = new EagleContext(ConnectionManager.GetConnectionString()))
            {
                try
                {
                    var idActivo = Estado.IdActivo;
                    result = ctx.SNTt06_unidad_medida
                        .Where(x => x.id_um_base == idPadre && x.id_estado == idActivo)
                        .Count();
                }
                catch (Exception e)
                {
                    var log = new Log();
                    log.ArchiveLog("Run Context: ", e.Message);
                }
            }
            return result;
        }

        public bool Eliminar(long id)
        {
            bool success = false;
            using (var cnn = new SqlConnection(ConnectionManager.GetConnectionString()))
            {
                try
                {
                    int id_estado = Estado.IdInactivo;
                    string txt_estado = Estado.TxtInactivo;
                    using (SqlCommand cmd = cnn.CreateCommand())
                    {
                        cmd.CommandText = "UPDATE SNTt06_unidad_medida SET id_estado = @id_estado, txt_estado = @txt_estado Where id_um=@id";
                        cmd.Parameters.AddWithValue("@id_estado", id_estado);
                        cmd.Parameters.AddWithValue("@txt_estado", txt_estado);
                        cmd.Parameters.AddWithValue("@id", id);
                        cnn.Open();
                        cmd.ExecuteNonQuery();
                        success = true;
                    }
                }
                catch (Exception e)
                {
                    var log = new Log();
                    log.ArchiveLog("Error en eliminar unidad de medida: ", e.Message);
                }
            }
            return success;
        }
    }
}
