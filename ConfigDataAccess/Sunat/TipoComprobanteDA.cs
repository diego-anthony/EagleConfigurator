﻿using ConfigBusinessEntity;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace ConfigDataAccess.Sunat
{
    public class TipoComprobanteDA
    {
        public IEnumerable<SNTt10_tipo_comprobante> FindOrdCompra()
        {
            var response = new List<SNTt10_tipo_comprobante>();
            using (var cnn = new SqlConnection(ConnectionManager.GetConnectionString()))
            {
                cnn.Open();
                string sqlQuery = "SELECT id_tipo_comp, txt_desc FROM SNTt10_tipo_comprobante WHERE cod_tipo_comp IS NULL";
                var cmd = new SqlCommand(sqlQuery, cnn);
                var rd = cmd.ExecuteReader();
                while (rd.Read())
                {
                    response.Add
                        (
                        new SNTt10_tipo_comprobante
                        {
                            id_tipo_comp = int.Parse(rd["id_tipo_comp"].ToString()),
                            txt_desc = rd["txt_desc"].ToString()
                        }
                        );
                }
            }
            return response;
        }
    }
}
