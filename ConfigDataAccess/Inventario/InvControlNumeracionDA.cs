﻿using ConfigBusinessEntity;
using ConfigUtilitarios;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace ConfigDataAccess.Inventario
{
    public class InvControlNumeracionDA
    {
        public IEnumerable<INVt11_control_numeracion> GetAll()
        {
            var response = new List<INVt11_control_numeracion>();
            using (var cnn = new SqlConnection(ConnectionManager.GetConnectionString()))
            {
                cnn.Open();
                string sqlQuery = $@"SELECT id_control_numeracion, txt_nro_serie, cn.nro_inicial, cn.nro_final, cn.
                                    nro_actual, fecha_registro, nv.txt_desc AS txt_nivel,tc.txt_desc AS txt_tipo_comp
                                    FROM INVt11_control_numeracion AS cn 
                                    INNER JOIN FISt02_nivel AS nv ON cn.id_nivel = nv.id_nivel
									INNER JOIN SNTt10_tipo_comprobante AS tc ON tc.id_tipo_comp = cn.id_tipo_comp
                                    WHERE cn.id_estado = {Estado.IdActivo}";
                var cmd = new SqlCommand(sqlQuery, cnn);
                var rd = cmd.ExecuteReader();
                while (rd.Read())
                {
                    response.Add(
                        new INVt11_control_numeracion
                        {
                            id_control_numeracion = long.Parse(rd["id_control_numeracion"].ToString()),
                            txt_nro_serie = rd["txt_nro_serie"].ToString(),
                            nro_inicial = rd["nro_inicial"] as long?,
                            nro_final = rd["nro_final"] as long?,
                            nro_actual = (long)rd["nro_actual"],
                            fecha_registro = (DateTime)rd["fecha_registro"],
                            SNTt10_tipo_comprobante = new SNTt10_tipo_comprobante { txt_desc = rd["txt_tipo_comp"] as string },
                            FISt02_nivel = new FISt02_nivel { txt_desc = rd["txt_nivel"] as string }
                        }
                    );
                }
            }
            return response;
        }

        public bool Add(IEnumerable<INVt11_control_numeracion> list)
        {
            bool result = false;
            try
            {
                using (var ctx = new EagleContext(ConnectionManager.GetConnectionString()))
                {
                    ctx.Database.ExecuteSqlCommand("TRUNCATE TABLE INVt11_control_numeracion");
                    ctx.INVt11_control_numeracion.AddRange(list);
                    ctx.SaveChanges();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                new Log().ArchiveLog("Agregar numeración", ex.Message);
            }
            return result;
        }

        public bool SearchDocumentExist(int idTipoComprobante)
        {
            bool result = false;
            try
            {
                var response = new List<INVt11_control_numeracion>();
                string sqlQuery = $"SELECT id_control_numeracion FROM INVt11_control_numeracion WHERE id_tipo_comp = '{idTipoComprobante}'";
                using (var cnn = new SqlConnection(ConnectionManager.GetConnectionString()))
                {
                    cnn.Open();
                    var cmd = new SqlCommand(sqlQuery, cnn);
                    var rd = cmd.ExecuteReader();
                    while (rd.Read())
                    {
                        response.Add(
                            new INVt11_control_numeracion
                            {
                                id_control_numeracion = long.Parse(rd["id_control_numeracion"].ToString())
                            }
                        );
                    }
                    if (response.Count > 0)
                    {
                        result = true;
                    }
                }
            }
            catch (Exception ex )
            {
                new Log().ArchiveLog("Buscar si existe numeración por  Tipo documento", ex.Message);
            }
            return result;
        }
    }
}
