﻿using ConfigBusinessEntity;
using ConfigUtilitarios;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace ConfigDataAccess.Inventario
{
    public class ParametroInventarioDA
    {
        public List<INVt12_parametro> GetAll()
        {
            var response = new List<INVt12_parametro>();
            string sqlQuery = @"SELECT * FROM INVt12_parametro";
            using (var cnn = new SqlConnection(ConnectionManager.GetConnectionString()))
            {
                try
                {
                    cnn.Open();
                    var cmd = new SqlCommand(sqlQuery, cnn);
                    var rd = cmd.ExecuteReader();
                    while (rd.Read())
                    {
                        response.Add(
                            new INVt12_parametro
                            {
                                id_parametro = (int)rd["id_parametro"],
                                cod_parametro = rd["cod_parametro"].ToString(),
                                txt_desc = rd["txt_desc"].ToString(),
                                dec_valor = rd["dec_valor"] as decimal?,
                                txt_valor = rd["txt_valor"].ToString(),
                                txt_obs = rd["txt_obs"].ToString(),
                                sn_edit = (int)rd["sn_edit"]

                            }
                        );
                    }
                }
                catch (Exception e)
                {
                    var log = new Log();
                    log.ArchiveLog("Lista Parámetro Inventario: ", e.Message);
                }
            }
            return response;
        }

        public bool Update(int val, string code)
        {
            var response = false;
            string sqlQuery = $"UPDATE INVt12_parametro SET dec_valor = {val} WHERE cod_parametro = '{code}'";
            using (var cnn = new SqlConnection(ConnectionManager.GetConnectionString()))
            {
                try
                {
                    cnn.Open();
                    var cmd = new SqlCommand(sqlQuery, cnn);
                    cmd.ExecuteNonQuery();
                    response = true;
                }
                catch (Exception e)
                {
                    var log = new Log();
                    log.ArchiveLog("Obtener parámetro de inventario por código: ", e.Message);
                }
            }
            return response;
        }

        public bool UpdateTxtVal(string val, string code)
        {
            var response = false;
            string sqlQuery = $"UPDATE INVt12_parametro SET txt_valor = '{val}' WHERE cod_parametro = '{code}'";
            using (var cnn = new SqlConnection(ConnectionManager.GetConnectionString()))
            {
                try
                {
                    cnn.Open();
                    var cmd = new SqlCommand(sqlQuery, cnn);
                    cmd.ExecuteNonQuery();
                    response = true;
                }
                catch (Exception e)
                {
                    var log = new Log();
                    log.ArchiveLog("Actualizar txt_valor en el parámetro de inventario por código: ", e.Message);
                }
            }
            return response;
        }

        public INVt12_parametro GetByCode(string code)
        {
            var response = new INVt12_parametro();
            string sqlQuery = $"SELECT id_parametro,dec_valor,txt_valor,txt_valor_default FROM INVt12_parametro WHERE cod_parametro = {code}";
            using (var cnn = new SqlConnection(ConnectionManager.GetConnectionString()))
            {
                try
                {
                    cnn.Open();
                    var cmd = new SqlCommand(sqlQuery, cnn);
                    var rd = cmd.ExecuteReader();
                    while (rd.Read())
                    {
                        response = new INVt12_parametro
                        {
                            id_parametro = (int)rd["id_parametro"],
                            dec_valor = rd["dec_valor"] as decimal?,
                            txt_valor = rd["txt_valor"].ToString(),
                            txt_valor_default = rd["txt_valor_default"].ToString(),
                        };
                    }
                }
                catch (Exception e)
                {
                    var log = new Log();
                    log.ArchiveLog("Obtener parámetro de inventario por código: ", e.Message);
                }
            }
            return response;
        }
    }
}
