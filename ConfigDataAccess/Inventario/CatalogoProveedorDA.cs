﻿using ConfigBusinessEntity;
using ConfigUtilitarios;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Data.Entity;

namespace ConfigDataAccess.Inventario
{
    public class CatalogoProveedorDA
    {
        #region Antes
        //        public List<PROt18_producto_proveedor> ListaCatalogoProveedor(int? id_estado = null)
        //        {
        //            var lista = new List<PROt18_producto_proveedor>();
        //            string sentencia = string.Empty;
        //            sentencia = (id_estado == null) ?
        //                @"SELECT * FROM PROt18_producto_proveedor" :
        //                @"SELECT * FROM PROt18_producto_proveedor WHERE id_estado=@id_estado";
        //            using (var cnn = new SqlConnection(ConnectionManager.GetConnectionString()))
        //            {
        //                try
        //                {
        //                    cnn.Open();
        //                    lista = cnn.Query<PROt18_producto_proveedor>(sentencia, new { id_estado }).ToList();
        //                }
        //                catch (Exception e)
        //                {
        //                    var log = new Log();
        //                    log.ArchiveLog("Lista de Catalogo: ", e.Message);
        //                }
        //            }
        //            return lista;
        //        }

        //        public long Add(PROt18_producto_proveedor model)
        //        {
        //            long id = 0;
        //            using (var ctx = new EagleContext(ConnectionManager.GetConnectionString()))
        //            {
        //                try
        //                {
        //                    ctx.PROt18_producto_proveedor.Add(model);
        //                    ctx.SaveChanges();
        //                    id = model.id_producto_proveedor;
        //                }
        //                catch (Exception e)
        //                {
        //                    var log = new Log();
        //                    log.ArchiveLog("Error al agregar el catalogo: ", e.Message);
        //                }
        //            }
        //            return id;
        //        }

        //        public bool Edit(PROt18_producto_proveedor model)
        //        {
        //            bool response = false;
        //            using (var ctx = new EagleContext(ConnectionManager.GetConnectionString()))
        //            {
        //                try
        //                {
        //                    var original = ctx.PROt18_producto_proveedor.Find(model.id_producto_proveedor);
        //                    if (original != null)
        //                    {
        //                        ctx.Entry(original).CurrentValues.SetValues(model);
        //                        ctx.SaveChanges();
        //                        response = true;
        //                    }
        //                }
        //                catch (Exception e)
        //                {
        //                    var log = new Log();
        //                    log.ArchiveLog("Error al editar el catalogo: ", e.Message);
        //                }
        //            }
        //            return response;
        //        }

        //        public bool Activar(ulong id)
        //        {
        //            bool success = false;
        //            using (var cnn = new SqlConnection(ConnectionManager.GetConnectionString()))
        //            {
        //                try
        //                {
        //                    int id_estado = Estado.IdActivo;
        //                    string txt_estado = Estado.TxtActivo;
        //                    using (SqlCommand cmd = cnn.CreateCommand())
        //                    {
        //                        cmd.CommandText = "UPDATE PROt18_producto_proveedor SET id_estado = @id_estado, txt_estado = @txt_estado Where id_um=@id";
        //                        cmd.Parameters.AddWithValue("@id_estado", id_estado);
        //                        cmd.Parameters.AddWithValue("@txt_estado", txt_estado);
        //                        cmd.Parameters.AddWithValue("@id", id);
        //                        cnn.Open();
        //                        cmd.ExecuteNonQuery();
        //                        success = true;
        //                    }
        //                }
        //                catch (Exception e)
        //                {
        //                    var log = new Log();
        //                    log.ArchiveLog("Activar catalogo: ", e.Message);
        //                }
        //            }
        //            return success;
        //        }

        //        public bool Eliminar(long id)
        //        {
        //            bool success = false;
        //            using (var cnn = new SqlConnection(ConnectionManager.GetConnectionString()))
        //            {
        //                try
        //                {
        //                    int id_estado = Estado.IdInactivo;
        //                    string txt_estado = Estado.TxtInactivo;
        //                    using (SqlCommand cmd = cnn.CreateCommand())
        //                    {
        //                        cmd.CommandText = "UPDATE PROt18_producto_proveedor SET id_estado = @id_estado, txt_estado = @txt_estado Where id_producto_proveedor=@id";
        //                        cmd.Parameters.AddWithValue("@id_estado", id_estado);
        //                        cmd.Parameters.AddWithValue("@txt_estado", txt_estado);
        //                        cmd.Parameters.AddWithValue("@id", id);
        //                        cnn.Open();
        //                        cmd.ExecuteNonQuery();
        //                        success = true;
        //                    }
        //                }
        //                catch (Exception e)
        //                {
        //                    var log = new Log();
        //                    log.ArchiveLog("Error en eliminar el catalogo: ", e.Message);
        //                }
        //            }
        //            return success;
        //        }

        //        public PROt18_producto_proveedor CatalogoProvXCod(string cod)
        //        {
        //            var obj = new PROt18_producto_proveedor();
        //            string sentencia = "SELECT * FROM PROt18_producto_proveedor WHERE cod_catalogo=@cod";
        //            using (var cnn = new SqlConnection(ConnectionManager.GetConnectionString()))
        //            {
        //                try
        //                {
        //                    cnn.Open();
        //                    obj = cnn.Query<PROt18_producto_proveedor>(sentencia, new { cod }).FirstOrDefault();
        //                }
        //                catch (Exception e)
        //                {
        //                    var log = new Log();
        //                    log.ArchiveLog("Búsqueda CatalogoProveedor por COD: ", e.Message);
        //                }
        //            }
        //            return obj;
        //        }
        //        //public PROt18_producto_proveedor Get(ulong id)
        //        //{
        //        //    PROt18_producto_proveedor response = null;
        //        //    using (var ctx = new EagleContext(ConnectionManager.GetConnectionString()))
        //        //    {
        //        //        try
        //        //        {
        //        //            response = ctx.PROt18_producto_proveedor
        //        //                .Include(x=>x.PERt03_proveedor)
        //        //                .Include(x=>x.PROt09_producto)
        //        //                .SingleOrDefault(x=>x.id_producto_proveedor == (long)id);
        //        //        }
        //        //        catch (Exception ex)
        //        //        {
        //        //            var log = new Log();
        //        //            log.ArchiveLog("Error en obtener item por id", ex.Message);
        //        //        }
        //        //    }
        //        //    return response;
        //        //}

        //        //public PROt18_producto_proveedor ListaCatagoloXIdProvIdProd(long idProv, long idProd)
        //        //{
        //        //    PROt18_producto_proveedor response = null;
        //        //    using (var ctx = new EagleContext(ConnectionManager.GetConnectionString()))
        //        //    {
        //        //        try
        //        //        {
        //        //            response = ctx.PROt18_producto_proveedor
        //        //                .Include(x => x.PERt03_proveedor)
        //        //                .Where(x => x.id_provedor == idProv);
        //        //        }
        //        //        catch (Exception ex)
        //        //        {
        //        //            var log = new Log();
        //        //            log.ArchiveLog("Error en obtener item por id", ex.Message);
        //        //        }
        //        //    }
        //        //    return response;
        //        //}

        //        public bool Guardar(List<PROt18_producto_proveedor> model)
        //        {
        //            var result = false;
        //            try
        //            {
        //                using (var ctx = new EagleContext(ConnectionManager.GetConnectionString()))
        //                {
        //                    foreach (var item in model)
        //                    {
        //                        ctx.PROt18_producto_proveedor.Add(item);
        //                    }
        //                    ctx.SaveChanges();
        //                    result = true;
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                new Log().ArchiveLog("Error en guardar catálogo de proveedor", ex.Message)
        //;           }
        //            return result;
        //        }
        #endregion

        public List<INVt06_catalogo_proveedor> ListaCatalogoProveedor(int? id_estado = null)
        {
            var lista = new List<INVt06_catalogo_proveedor>();
            string sentencia = string.Empty;
            sentencia = (id_estado == null) ?
                @"SELECT * FROM INVt06_catalogo_proveedor" :
                @"SELECT * FROM INVt06_catalogo_proveedor WHERE id_estado=@id_estado";
            using (var cnn = new SqlConnection(ConnectionManager.GetConnectionString()))
            {
                try
                {
                    cnn.Open();
                    lista = cnn.Query<INVt06_catalogo_proveedor>(sentencia, new { id_estado }).ToList();
                }
                catch (Exception e)
                {
                    var log = new Log();
                    log.ArchiveLog("Lista Catalogo Proveedor: ", e.Message);
                }
            }
            return lista;
        }
        public List<PERt03_proveedor> ListaProveedorCatActivo(int? id_estado = null)
        {
            var lista = new List<PERt03_proveedor>();
            string sentencia = string.Empty;
            sentencia = (id_estado == null) ?
                @"SELECT * FROM PERt03_proveedor WHERE es_exclusivo=1" :
                @"SELECT * FROM PERt03_proveedor WHERE es_exclusivo=1 and id_estado=@id_estado";
            using (var cnn = new SqlConnection(ConnectionManager.GetConnectionString()))
            {
                try
                {
                    cnn.Open();
                    lista = cnn.Query<PERt03_proveedor>(sentencia, new { id_estado }).ToList();
                }
                catch (Exception e)
                {
                    var log = new Log();
                    log.ArchiveLog("Lista Proveedor Catalogo Activo: ", e.Message);
                }
            }
            return lista;
        }
        public long InsertarCatalogoProveedor(INVt06_catalogo_proveedor obj)
        {
            long id = 0;
            using (var ctx = new EagleContext(ConnectionManager.GetConnectionString()))
            {
                using (var tns = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        ctx.INVt06_catalogo_proveedor.Add(obj);
                        ctx.SaveChanges();
                        tns.Commit();
                        id = obj.id_catalogo;
                    }
                    catch (Exception e)
                    {
                        tns.Rollback();
                        var log = new Log();
                        log.ArchiveLog("Insertar CatalogoProveedor: ", e.Message);
                    }
                }

            }
            return id;
        }
        public void EliminarCatalogoProveedor(long id)
        {
            using (var cnn = new SqlConnection(ConnectionManager.GetConnectionString()))
            {
                try
                {
                    int id_estado = Estado.IdInactivo;
                    string txt_estado = Estado.TxtInactivo;
                    using (SqlCommand cmd = cnn.CreateCommand())
                    {
                        cmd.CommandText = "UPDATE INVt06_catalogo_proveedor SET id_estado = @id_estado, txt_estado = @txt_estado Where id_catalogo=@id";
                        cmd.Parameters.AddWithValue("@id_estado", id_estado);
                        cmd.Parameters.AddWithValue("@txt_estado", txt_estado);
                        cmd.Parameters.AddWithValue("@id", id);
                        cnn.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception e)
                {
                    var log = new Log();
                    log.ArchiveLog("Eliminar Catalogo: ", e.Message);
                }
            }
        }
        public bool ActivarCatalogoProveedor(long id)
        {
            bool success = false;
            using (var cnn = new SqlConnection(ConnectionManager.GetConnectionString()))
            {
                try
                {
                    int id_estado = Estado.IdActivo;
                    string txt_estado = Estado.TxtActivo;
                    using (SqlCommand cmd = cnn.CreateCommand())
                    {
                        cmd.CommandText = "UPDATE INVt06_catalogo_proveedor SET id_estado = @id_estado, txt_estado = @txt_estado Where id_catalogo=@id";
                        cmd.Parameters.AddWithValue("@id_estado", id_estado);
                        cmd.Parameters.AddWithValue("@txt_estado", txt_estado);
                        cmd.Parameters.AddWithValue("@id", id);
                        cnn.Open();
                        cmd.ExecuteNonQuery();
                        success = true;
                    }
                }
                catch (Exception e)
                {
                    var log = new Log();
                    log.ArchiveLog("Activar Catalogo: ", e.Message);
                }
                return success;
            }
        }
        public bool ActualizarCatalogo(INVt06_catalogo_proveedor actualizado)
        {
            bool success = false;
            using (var ctx = new EagleContext(ConnectionManager.GetConnectionString()))
            {
                using (var tns = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        var original = ctx.INVt06_catalogo_proveedor.Find(actualizado.id_catalogo);
                        if (original != null && original.id_catalogo > 0)
                        {
                            bool actualizarPrecioCascada = true;

                            if (actualizarPrecioCascada)
                            {
                                //Actualización del master
                                ctx.Entry(original).CurrentValues.SetValues(actualizado);

                                //Actualización del detail
                                if (actualizado.INVt07_catalogo_proveedor_dtl != null)
                                {
                                    foreach (var newItem in actualizado.INVt07_catalogo_proveedor_dtl)
                                    {
                                        //Caso: Edición 
                                        if (newItem.id_catalogo_proveedor_dtl > 0)
                                        {
                                            var oldItem = ctx.INVt07_catalogo_proveedor_dtl.Find(newItem.id_catalogo_proveedor_dtl);
                                            if (oldItem != null)
                                            {
                                                ctx.Entry(oldItem).CurrentValues.SetValues(newItem);
                                            }
                                        }
                                        //Caso: Inserción
                                        else
                                        {
                                            ctx.INVt07_catalogo_proveedor_dtl.Add(newItem);
                                        }
                                    }
                                }
                                ctx.SaveChanges();
                                tns.Commit();
                                success = true;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        tns.Rollback();
                        var log = new Log();
                        log.ArchiveLog("Actualizar Combo Variable: ", e.Message);
                    }
                }
            }
            return success;
        }

        public INVt06_catalogo_proveedor CatalogoXIdTest(long id)
        {
            var obj = new INVt06_catalogo_proveedor();

            string sentencia = "SELECT * FROM INVt06_catalogo_proveedor WHERE id_catalogo=@id";
            using (var cnn = new SqlConnection(ConnectionManager.GetConnectionString()))
            {
                try
                {
                    cnn.Open();
                    obj = cnn.Query<INVt06_catalogo_proveedor>(sentencia, new { id }).FirstOrDefault();

                    if (obj != null)
                    {

                    }
                }
                catch (Exception e)
                {
                    var log = new Log();
                    log.ArchiveLog("Búsqueda Catalogo por ID Test: ", e.Message);
                }
            }
            return obj;
        }

        public INVt06_catalogo_proveedor CatalogoXId(long id)
        {
            var obj = new INVt06_catalogo_proveedor();

            #region Con Dapper
            //   string sql2 = @"SELECT * FROM PROt15_combo_variable WHERE id_combo_variable=@id; 
            //                      SELECT  dtl.[id_combo_variable_dtl]
            //                         ,dtl.[cod_combo_variable_dtl]
            // ,pro.[txt_desc] as txt_desc
            //                         ,dtl.[cantidad]
            //                         ,dtl.[mto_pvpu_sin_tax]
            //                         ,dtl.[mto_pvpu_con_tax]
            //                         ,dtl.[id_estado]
            //                         ,dtl.[txt_estado]
            //                         ,dtl.[id_combo_variable]
            //                         ,dtl.[id_item]
            //                     FROM [PROt16_combo_variable_dtl] dtl
            //INNER JOIN PROt09_item pro 
            //ON dtl.id_item = pro.id_item
            //WHERE dtl.id_combo_variable = @id;";

            var sql = @"SELECT * FROM INVt06_catalogo_proveedor WHERE id_catalogo=@id; 
                            SELECT[id_catalogo_proveedor_dtl]
                                  
                                  ,[id_estado]
                                  ,[txt_estado]
                                  ,[id_catalogo]
                                  ,[id_item]
                                    FROM [INVt07_catalogo_proveedor_dtl] WHERE id_catalogo = @id;";
            using (var cnn = new SqlConnection(ConnectionManager.GetConnectionString()))
            {
                try
                {
                    cnn.Open();
                    using (var multi = cnn.QueryMultiple(sql, new { id }))
                    {
                        obj = multi.Read<INVt06_catalogo_proveedor>().SingleOrDefault();
                        var combo_var_dtl = multi.Read<INVt07_catalogo_proveedor_dtl>().ToList();

                        if (obj != null && combo_var_dtl != null)
                        {
                            obj.INVt07_catalogo_proveedor_dtl = combo_var_dtl;
                            foreach (var dtl in combo_var_dtl)
                            {
                                dtl.INVt09_item = cnn.Query<INVt09_item>("SELECT txt_desc FROM INVt09_item WHERE id_item = @id_item", new { id_item = dtl.id_item }).SingleOrDefault();
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    var log = new Log();
                    log.ArchiveLog("Búsqueda Catalogo por ID: ", e.Message);
                }
            }
            #endregion

            return obj;
        }

        public INVt06_catalogo_proveedor CatalogoXIdEF(long id)
        {
            var obj = new INVt06_catalogo_proveedor();
            using (var ctx = new EagleContext(ConnectionManager.GetConnectionString()))
            {

                try
                {

                    //Cambiar aaquí.
                    //obj = ctx.PROt16_combo_variable_dtl.SingleOrDefault();

                }
                catch (Exception e)
                {
                    var log = new Log();
                    log.ArchiveLog("Combo var EF Combo Variable: ", e.Message);
                }
            }
            return obj;
        }


        public INVt06_catalogo_proveedor CatalogoXCod(string cod)
        {
            var obj = new INVt06_catalogo_proveedor();
            string sentencia = "SELECT * FROM INVt06_catalogo_proveedor WHERE cod_catalogo=@cod";
            using (var cnn = new SqlConnection(ConnectionManager.GetConnectionString()))
            {
                try
                {
                    cnn.Open();
                    obj = cnn.Query<INVt06_catalogo_proveedor>(sentencia, new { cod }).FirstOrDefault();
                }
                catch (Exception e)
                {
                    var log = new Log();
                    log.ArchiveLog("Búsqueda Catalogo por COD: ", e.Message);
                }
            }
            return obj;
        }

        public IEnumerable<INVt06_catalogo_proveedor> BuscarCatalogo(string cod, string descripcion, int? id_estado)
        {
            var list = new List<INVt06_catalogo_proveedor>();
            using (var ctx = new EagleContext(ConnectionManager.GetConnectionString()))
            {
                try
                {
                    list = ctx.INVt06_catalogo_proveedor
                              .AsNoTracking()
                              .Where(x => x.cod_catalogo.Contains(cod) &&
                                    x.txt_proveedor.Contains(descripcion) &&
                                    x.id_estado == (id_estado == null ? x.id_estado : id_estado))
                              .ToList();
                }
                catch (Exception e)
                {
                    var log = new Log();
                    log.ArchiveLog("Búsqueda Catalogo por código, nombre y estado: ", e.Message);
                }
            }
            return list;
        }

        public string ObtenerIdCatalogo(long idProv )
        {
            string idCatalogo = string.Empty ;

            using (var cnn = new SqlConnection(ConnectionManager.GetConnectionString()))
            {
                cnn.Open();
                string sqlQuery = @"SELECT id_catalogo FROM INVt06_catalogo_proveedor WHERE id_proveedor="+idProv;
                var cmd = new SqlCommand(sqlQuery, cnn);
                var rd = cmd.ExecuteReader();
                while (rd.Read())
                {
                    idCatalogo = rd["id_catalogo"].ToString();
                   
                }
            }
            
            return idCatalogo;
        }
    }
}
