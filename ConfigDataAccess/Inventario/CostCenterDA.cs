﻿using ConfigBusinessEntity;
using ConfigUtilitarios;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace ConfigDataAccess.Inventario
{
    public class CostCenterDA
    {
        public List<INVt05_cost_center> ListaCostCenter(int? id_estado = null)
        {
            var lista = new List<INVt05_cost_center>();
            string sentencia = string.Empty;
            sentencia = (id_estado == null) ?
                @"SELECT * FROM INVt05_cost_center" :
                @"SELECT * FROM INVt05_cost_center WHERE id_estado=@id_estado";
            using (var cnn = new SqlConnection(ConnectionManager.GetConnectionString()))
            {
                try
                {
                    cnn.Open();
                    lista = cnn.Query<INVt05_cost_center>(sentencia, new { id_estado }).ToList();
                }
                catch (Exception e)
                {
                    var log = new Log();
                    log.ArchiveLog("Lista CostCenter: ", e.Message);
                }
            }
            return lista;
        }
        public int InsertarCostCenter(INVt05_cost_center obj)
        {
            int id = 0;
            using (var ctx = new EagleContext(ConnectionManager.GetConnectionString()))
            {
                try
                {
                    ctx.INVt05_cost_center.Add(obj);
                    ctx.SaveChanges();
                    id = obj.id_location;
                }
                catch (Exception e)
                {
                    var log = new Log();
                    log.ArchiveLog("Insertar CostCenter: ", e.Message);
                }
            }
            return id;
        }
        public void EliminarCostCenter(int id)
        {
            using (var cnn = new SqlConnection(ConnectionManager.GetConnectionString()))
            {
                try
                {
                    int id_estado = Estado.IdInactivo;
                    string txt_estado = Estado.TxtInactivo;
                    using (SqlCommand cmd = cnn.CreateCommand())
                    {
                        cmd.CommandText = "UPDATE INVt05_cost_center SET id_estado = @id_estado, txt_estado = @txt_estado Where id_cost_center=@id";
                        cmd.Parameters.AddWithValue("@id_estado", id_estado);
                        cmd.Parameters.AddWithValue("@txt_estado", txt_estado);
                        cmd.Parameters.AddWithValue("@id", id);
                        cnn.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception e)
                {
                    var log = new Log();
                    log.ArchiveLog("Eliminar CostCenter: ", e.Message);
                }
            }
        }
        public void ActualizarCostCenter(INVt05_cost_center actualizado)
        {
            using (var ctx = new EagleContext(ConnectionManager.GetConnectionString()))
            {
                try
                {
                    var original = ctx.INVt05_cost_center.Find(actualizado.id_cost_center);
                    if (original != null && original.id_location > 0)
                    {
                        ctx.Entry(original).CurrentValues.SetValues(actualizado);
                        ctx.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    var log = new Log();
                    log.ArchiveLog("Actualizar CostCenter: ", e.Message);
                }
            }
        }
        public INVt05_cost_center CostCenterXId(int id)
        {
            var obj = new INVt05_cost_center();
            string sentencia = "SELECT * FROM INVt05_cost_center WHERE id_cost_center=@id";
            using (var cnn = new SqlConnection(ConnectionManager.GetConnectionString()))
            {
                try
                {
                    cnn.Open();
                    obj = cnn.Query<INVt05_cost_center>(sentencia, new { id }).FirstOrDefault();
                }
                catch (Exception e)
                {
                    var log = new Log();
                    log.ArchiveLog("Búsqueda CostCenter por ID: ", e.Message);
                }
            }
            return obj;
        }
        //public INVt05_cost_center CostCenterXIdMM(int id)
        //{
        //    var obj = new INVt05_cost_center();
        //    //obteniendo
        //    obj = CostCenterXId(id);
        //    const string sentencia =
        //            @"SELECT * FROM SNTt33_distrito WHERE id_dist=@id_dist";
        //    using (var cnn = new SqlConnection(ConnectionManager.GetConnectionString()))
        //    {
        //        try
        //        {
        //            cnn.Open();
        //            var multi = cnn.QueryMultiple(sentencia, new
        //            {
        //                id_dist = obj.id_dist
        //            });
        //            var distrito = multi.Read<SNTt33_distrito>().FirstOrDefault();
        //            obj.SNTt33_distrito = distrito;

        //        }
        //        catch (Exception e)
        //        {
        //            var log = new Log();
        //            log.ArchiveLog("Búsqueda CostCenter MM por ID: ", e.Message);
        //        }
        //    }
        //    return obj;
        //}


        public INVt05_cost_center CostCenterXCod(string cod)
        {
            var obj = new INVt05_cost_center();
            string sentencia = "SELECT * FROM INVt05_cost_center WHERE cod_cost_center=@cod";
            using (var cnn = new SqlConnection(ConnectionManager.GetConnectionString()))
            {
                try
                {
                    cnn.Open();
                    obj = cnn.Query<INVt05_cost_center>(sentencia, new { cod }).FirstOrDefault();
                }
                catch (Exception e)
                {
                    var log = new Log();
                    log.ArchiveLog("Búsqueda CostCenter por COD: ", e.Message);
                }
            }
            return obj;
        }
    }
}
