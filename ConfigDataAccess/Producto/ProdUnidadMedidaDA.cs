﻿using ConfigBusinessEntity;
using ConfigUtilitarios;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;

namespace ConfigDataAccess
{
    public class ProdUnidadMedidaDA
    {
        public List<INVt10_unidad_medida> ListaUnidMedCompra(int? id_estado = null)
        {
            var lista = new List<INVt10_unidad_medida>();
            string sentencia = string.Empty;
            sentencia = (id_estado == null) ?
                @"SELECT * FROM INVt10_unidad_medida WHERE sn_compra = 1" :
                @"SELECT * FROM INVt10_unidad_medida WHERE id_estado = @id_estado AND sn_compra = 1";
            using (var cnn = new SqlConnection(ConnectionManager.GetConnectionString()))
            {
                try
                {
                    cnn.Open();
                    lista = cnn.Query<INVt10_unidad_medida>(sentencia, new { id_estado }).ToList();
                }
                catch (Exception e)
                {
                    var log = new Log();
                    log.ArchiveLog("Lista de Unidades de medida: ", e.Message);
                }
            }
            return lista;
        }
        public List<INVt10_unidad_medida> ListaUnidadMed(int? id_estado = null)
        {
            var lista = new List<INVt10_unidad_medida>();
            string sentencia = string.Empty;
            sentencia = (id_estado == null) ?
                @"SELECT * FROM INVt10_unidad_medida" :
                @"SELECT * FROM INVt10_unidad_medida WHERE id_estado=@id_estado";
            using (var cnn = new SqlConnection(ConnectionManager.GetConnectionString()))
            {
                try
                {
                    cnn.Open();
                    lista = cnn.Query<INVt10_unidad_medida>(sentencia, new { id_estado }).ToList();
                }
                catch (Exception e)
                {
                    var log = new Log();
                    log.ArchiveLog("Lista de Unidades de medida: ", e.Message);
                }
            }
            return lista;
        }

        public INVt10_unidad_medida Add(INVt10_unidad_medida model)
        {
            INVt10_unidad_medida result = null;
            using (var ctx = new EagleContext(ConnectionManager.GetConnectionString()))
            {
                try
                {
                    result = ctx.INVt10_unidad_medida.Add(model);
                    ctx.SaveChanges();
                }
                catch (Exception e)
                {
                    var log = new Log();
                    log.ArchiveLog("Error al agregar unidad: ", e.Message);
                    //foreach (var validationErrors in dbEx.EntityValidationErrors)
                    //{
                    //    foreach (var validationError in validationErrors.ValidationErrors)
                    //    {
                    //        System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    //    }
                    //}

                }
            }
            return result;
        }

        public bool Edit(INVt10_unidad_medida model)
        {
            bool response = false;
            using (var ctx = new EagleContext(ConnectionManager.GetConnectionString()))
            {
                try
                {
                    var original = ctx.INVt10_unidad_medida.Find(model.id_um);
                    if (original != null)
                    {
                        ctx.Entry(original).CurrentValues.SetValues(model);
                        ctx.SaveChanges();
                        response = true;
                    }
                }
                catch (Exception e)
                {
                    var log = new Log();
                    log.ArchiveLog("Error al editar unidad: ", e.Message);
                }
            }
            return response;
        }

        public bool Activar(long id)
        {
            bool success = false;
            using (var cnn = new SqlConnection(ConnectionManager.GetConnectionString()))
            {
                try
                {
                    int id_estado = Estado.IdActivo;
                    string txt_estado = Estado.TxtActivo;
                    using (SqlCommand cmd = cnn.CreateCommand())
                    {
                        cmd.CommandText = "UPDATE INVt10_unidad_medida SET id_estado = @id_estado, txt_estado = @txt_estado Where id_um=@id";
                        cmd.Parameters.AddWithValue("@id_estado", id_estado);
                        cmd.Parameters.AddWithValue("@txt_estado", txt_estado);
                        cmd.Parameters.AddWithValue("@id", id);
                        cnn.Open();
                        cmd.ExecuteNonQuery();
                        success = true;
                    }
                }
                catch (Exception e)
                {
                    var log = new Log();
                    log.ArchiveLog("Activar unidad de medida: ", e.Message);
                }
            }
            return success;
        }

        public INVt10_unidad_medida Get(ulong id)
        {
            INVt10_unidad_medida response = null;
            using (var ctx = new EagleContext(ConnectionManager.GetConnectionString()))
            {
                try
                {
                    response = ctx.INVt10_unidad_medida.Find((long)id);
                }
                catch (Exception ex)
                {
                    var log = new Log();
                    log.ArchiveLog("Error en obtener producto por id", ex.Message);
                }
            }
            return response;
        }

        public INVt10_unidad_medida Find(Expression<Func<INVt10_unidad_medida, bool>> expression)
        {
            INVt10_unidad_medida response = null;
            using (var ctx = new EagleContext(ConnectionManager.GetConnectionString()))
            {
                try
                {
                    response = ctx.INVt10_unidad_medida.SingleOrDefault(expression);
                }
                catch (Exception ex)
                {
                    var log = new Log();
                    log.ArchiveLog("Error en buscar una unidad de medida", ex.Message);
                    throw;
                }
            }
            return response;
        }

        public IEnumerable<INVt10_unidad_medida> FindAll(Expression<Func<INVt10_unidad_medida, bool>> expression)
        {
            IEnumerable<INVt10_unidad_medida> response = null;
            using (var ctx = new EagleContext(ConnectionManager.GetConnectionString()))
            {
                try
                {
                    response = ctx.INVt10_unidad_medida.Where(expression).ToList();
                }
                catch (Exception ex)
                {
                    var log = new Log();
                    log.ArchiveLog("Error en buscar una lista de unidades de medida", ex.Message);
                    throw;
                }
            }
            return response;
        }

        public IEnumerable<INVt10_unidad_medida> FindAllUnits(string codigo = "", string nombre = "", int? idEstado = null)
        {
            IEnumerable<INVt10_unidad_medida> response = null;
            try
            {
                using (var ctx = new EagleContext(ConnectionManager.GetConnectionString()))
                {
                    response = ctx.INVt10_unidad_medida.Where(u =>
                        (string.IsNullOrEmpty(codigo) ? true : u.cod_um.ToLower().Contains(codigo.ToLower())) &&
                        (string.IsNullOrEmpty(nombre) ? true : u.txt_desc.ToLower().Contains(nombre.ToLower())) &&
                        (idEstado == Estado.Ignorar ? true : u.id_estado == Estado.IdActivo))
                        .OrderBy(x => x.txt_desc)
                        .ToList();
                }
            }
            catch (Exception ex)
            {
                var log = new Log();
                log.ArchiveLog("Error en buscar lista de unidades de medida ", ex.Message);
            }
            return response;
        }

        public INVt10_unidad_medida UnitByCode(string code)
        {
            INVt10_unidad_medida response = null;
            try
            {
                using (var ctx = new EagleContext(ConnectionManager.GetConnectionString()))
                {
                    response = ctx.INVt10_unidad_medida.SingleOrDefault(x => x.cod_um == code);
                }
            }
            catch (Exception ex)
            {
                var log = new Log();
                log.ArchiveLog("Error en buscar lista de unidades de medida ", ex.Message);
            }
            return response;
        }

        public int FindByUndbase(ulong idPadre)
        {
            var result = -1;
            using (var ctx = new EagleContext(ConnectionManager.GetConnectionString()))
            {
                try
                {
                    var idActivo = Estado.IdActivo;
                    result = ctx.INVt10_unidad_medida
                        .Where(x => x.id_um_base == (long)idPadre && x.id_estado == idActivo)
                        .Count();
                }
                catch (Exception e)
                {
                    var log = new Log();
                    log.ArchiveLog("Run Context: ", e.Message);
                }
            }
            return result;
        }

        public bool Eliminar(ulong id)
        {
            bool success = false;
            using (var cnn = new SqlConnection(ConnectionManager.GetConnectionString()))
            {
                try
                {
                    int id_estado = Estado.IdInactivo;
                    string txt_estado = Estado.TxtInactivo;
                    using (SqlCommand cmd = cnn.CreateCommand())
                    {
                        cmd.CommandText = "UPDATE INVt10_unidad_medida SET id_estado = @id_estado, txt_estado = @txt_estado Where id_um=@id";
                        cmd.Parameters.AddWithValue("@id_estado", id_estado);
                        cmd.Parameters.AddWithValue("@txt_estado", txt_estado);
                        cmd.Parameters.AddWithValue("@id", id);
                        cnn.Open();
                        cmd.ExecuteNonQuery();
                        success = true;
                    }
                }
                catch (Exception e)
                {
                    var log = new Log();
                    log.ArchiveLog("Error en eliminar unidad de medida: ", e.Message);
                }
            }
            return success;
        }
    }

}
