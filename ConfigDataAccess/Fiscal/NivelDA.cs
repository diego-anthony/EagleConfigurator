﻿using ConfigBusinessEntity;
using ConfigUtilitarios;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace ConfigDataAccess.Fiscal
{
    public class NivelDA
    {
        public IEnumerable<FISt02_nivel> GetAll()
        {
            var response = new List<FISt02_nivel>();
            using (var cnn = new SqlConnection(ConnectionManager.GetConnectionString()))
            {
                cnn.Open();
                string sqlQuery = $@"SELECT id_nivel,txt_desc FROM FISt02_nivel
                                    WHERE id_estado = {Estado.IdActivo}";
                var cmd = new SqlCommand(sqlQuery, cnn);
                var rd = cmd.ExecuteReader();
                while (rd.Read())
                {
                    response.Add(
                        new FISt02_nivel
                        {
                            id_nivel = int.Parse(rd["id_nivel"].ToString()),
                            txt_desc = rd["txt_desc"] as string,
                        }
                    );
                }
            }
            return response;
        }
    }
}
