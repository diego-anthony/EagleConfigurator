﻿namespace ConfigBusinessEntity
{
    public class Response
    {
        public string Code { get; set; }
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public object Result { get; set; }
    }
}
