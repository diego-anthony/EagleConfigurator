namespace ConfigBusinessEntity
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class INVt05_cost_center
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public INVt05_cost_center()
        {
            INVt01_orden_compra = new HashSet<INVt01_orden_compra>();
            INVt13_transferencia = new HashSet<INVt13_transferencia>();
            INVt13_transferencia1 = new HashSet<INVt13_transferencia>();
            INVt02_recepcion = new HashSet<INVt02_recepcion>();
        }

        [Key]
        public long id_cost_center { get; set; }

        [StringLength(20)]
        public string cod_cost_center { get; set; }

        [Required]
        [StringLength(50)]
        public string txt_nombre { get; set; }

        public int? id_estado { get; set; }

        [StringLength(20)]
        public string txt_estado { get; set; }

        [StringLength(300)]
        public string txt_direccion1 { get; set; }

        [StringLength(300)]
        public string txt_direccion2 { get; set; }

        [StringLength(300)]
        public string txt_abrev1 { get; set; }

        [StringLength(300)]
        public string txt_abrev2 { get; set; }

        [StringLength(200)]
        public string nro_ruc { get; set; }

        [StringLength(20)]
        public string fono1 { get; set; }

        [StringLength(20)]
        public string fono2 { get; set; }

        [StringLength(150)]
        public string txt_datos2 { get; set; }

        [StringLength(150)]
        public string txt_datos1 { get; set; }

        [StringLength(150)]
        public string txt_datos3 { get; set; }

        [StringLength(150)]
        public string txt_datos4 { get; set; }

        public decimal? latitud { get; set; }

        public decimal? longitud { get; set; }

        public int? sn_almacen { get; set; }

        public int? sn_location_current { get; set; }

        public int id_location { get; set; }

        public virtual MSTt08_location MSTt08_location { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<INVt01_orden_compra> INVt01_orden_compra { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<INVt02_recepcion> INVt02_recepcion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<INVt13_transferencia> INVt13_transferencia { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<INVt13_transferencia> INVt13_transferencia1 { get; set; }
    }
}
