namespace ConfigBusinessEntity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class INVt07_catalogo_proveedor_dtl
    {
        [Key]
        public long id_catalogo_proveedor_dtl { get; set; }

        public long id_catalogo { get; set; }

        public long id_item { get; set; }

        public int? id_estado { get; set; }

        [StringLength(20)]
        public string txt_estado { get; set; }

        public virtual INVt06_catalogo_proveedor INVt06_catalogo_proveedor { get; set; }

        public virtual INVt09_item INVt09_item { get; set; }
    }
}
