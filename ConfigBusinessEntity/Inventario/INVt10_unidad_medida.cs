namespace ConfigBusinessEntity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class INVt10_unidad_medida
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public INVt10_unidad_medida()
        {
            INVt09_item = new HashSet<INVt09_item>();
            INVt10_unidad_medida1 = new HashSet<INVt10_unidad_medida>();
        }

        [Key]
        public long id_um { get; set; }

        [StringLength(10)]
        public string cod_um { get; set; }

        [StringLength(10)]
        public string txt_abrv { get; set; }

        [Required]
        [StringLength(250)]
        public string txt_desc { get; set; }

        [StringLength(250)]
        public string txt_unid_base { get; set; }

        public long? id_um_base { get; set; }

        public decimal? dec_factor { get; set; }

        public bool sn_compra { get; set; }

        public bool sn_venta { get; set; }

        public decimal? qty_base { get; set; }

        public int? id_estado { get; set; }

        [StringLength(20)]
        public string txt_estado { get; set; }

        public int? id_um_snt { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<INVt09_item> INVt09_item { get; set; }

        public virtual SNTt06_unidad_medida SNTt06_unidad_medida { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<INVt10_unidad_medida> INVt10_unidad_medida1 { get; set; }

        public virtual INVt10_unidad_medida INVt10_unidad_medida2 { get; set; }
    }
}
