namespace ConfigBusinessEntity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class INVt11_control_numeracion
    {
        [Key]
        public long id_control_numeracion { get; set; }

        [StringLength(20)]
        public string txt_nro_serie { get; set; }

        public long? nro_inicial { get; set; }

        public long? nro_final { get; set; }

        public long nro_actual { get; set; }

        public int? locked_by { get; set; }

        [StringLength(100)]
        public string prefijo { get; set; }

        [StringLength(100)]
        public string sufijo { get; set; }

        public DateTime fecha_registro { get; set; }

        public int id_estado { get; set; }

        [Required]
        [StringLength(20)]
        public string txt_estado { get; set; }

        public int id_nivel { get; set; }

        public int id_tipo_comp { get; set; }

        public virtual FISt02_nivel FISt02_nivel { get; set; }

        public virtual SNTt10_tipo_comprobante SNTt10_tipo_comprobante { get; set; }
    }
}
