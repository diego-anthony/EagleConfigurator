﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ConfigBusinessEntity
{
    public partial class INVt15_transferencia_estado
    {
        [Key]
        public long id_transferencia_estado_dtl { get; set; }

        public int id_estado { get; set; }

        [Required]
        [StringLength(20)]
        public string txt_estado { get; set; }

        public DateTime fecha_registro { get; set; }

        public long id_usuario { get; set; }

        [Required]
        [StringLength(50)]
        public string txt_usuario { get; set; }

        public int? id_razon { get; set; }

        [StringLength(500)]
        public string txt_razon { get; set; }

        [StringLength(600)]
        public string txt_observ { get; set; }

        public long id_transferencia { get; set; }

        public virtual INVt13_transferencia INVt13_transferencia { get; set; }
    }
}
