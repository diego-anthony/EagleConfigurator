namespace ConfigBusinessEntity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class INVt02_recepcion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public INVt02_recepcion()
        {
            INVt04_recepcion_dtl = new HashSet<INVt04_recepcion_dtl>();
            INVt16_recepcion_estado = new HashSet<INVt16_recepcion_estado>();
        }

        [Key]
        public long id_recepcion { get; set; }

        [StringLength(100)]
        [Required]
        public string cod_recepcion { get; set; }

        [StringLength(255)]
        public string nro_correlativo { get; set; }

        public DateTime fecha_creacion { get; set; }

        public DateTime? fecha_book { get; set; }

        public DateTime? fecha_modificacion { get; set; }

        [Column(TypeName = "date")]
        public DateTime fecha_recepcion { get; set; }

        [StringLength(100)]
        public string referencia { get; set; }

        [StringLength(500)]
        public string info01 { get; set; }

        [StringLength(500)]
        public string info02 { get; set; }

        [StringLength(500)]
        public string info03 { get; set; }

        [StringLength(500)]
        public string info04 { get; set; }

        [StringLength(500)]
        public string info05 { get; set; }

        [StringLength(500)]
        public string info06 { get; set; }

        [StringLength(500)]
        public string info07 { get; set; }

        [StringLength(500)]
        public string info08 { get; set; }

        [StringLength(500)]
        public string info09 { get; set; }

        [StringLength(500)]
        public string info10 { get; set; }

        [StringLength(150)]
        public string txt_tipo_moneda { get; set; }

        public decimal? mto_tipo_cambio { get; set; }

        public decimal mto_con_tax_tot { get; set; }

        public decimal mto_sin_tax_tot { get; set; }

        public decimal tax_por_tot { get; set; }

        public decimal mto_tax_tot { get; set; }

        public decimal mto_tot_rec { get; set; }

        public int id_estado { get; set; }

        [Required]
        [StringLength(20)]
        public string txt_estado { get; set; }

        [Required]
        [StringLength(50)]
        public string txt_usuario { get; set; }

        public long? id_usuario_modificador { get; set; }

        [StringLength(50)]
        public string txt_usuario_modificador { get; set; }

        [StringLength(500)]
        public string txt_razon { get; set; }

        public long? id_orden_compra { get; set; }

        public int id_tipo_moneda { get; set; }

        public long id_usuario { get; set; }

        public long id_cost_center { get; set; }

        public long id_proveedor { get; set; }

        public int? id_razon { get; set; }

        public virtual INVt01_orden_compra INVt01_orden_compra { get; set; }

        public virtual INVt05_cost_center INVt05_cost_center { get; set; }

        public virtual PERt01_usuario PERt01_usuario { get; set; }

        public virtual SNTt04_tipo_moneda SNTt04_tipo_moneda { get; set; }

        public virtual PERt03_proveedor PERt03_proveedor { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<INVt04_recepcion_dtl> INVt04_recepcion_dtl { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<INVt16_recepcion_estado> INVt16_recepcion_estado { get; set; }
    }
}
