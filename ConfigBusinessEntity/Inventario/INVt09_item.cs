namespace ConfigBusinessEntity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class INVt09_item
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public INVt09_item()
        {
            INVt03_orden_compra_dtl = new HashSet<INVt03_orden_compra_dtl>();
            INVt07_catalogo_proveedor_dtl = new HashSet<INVt07_catalogo_proveedor_dtl>();
            INVt14_transferencia_dtl = new HashSet<INVt14_transferencia_dtl>();
        }

        [Key]
        public long id_item { get; set; }

        [StringLength(50)]
        public string cod_item { get; set; }

        [StringLength(50)]
        public string cod_item2 { get; set; }

        [StringLength(20)]
        public string cod_barra { get; set; }

        [StringLength(350)]
        public string txt_desc { get; set; }

        public decimal? mto_pcpu_con_tax { get; set; }

        public decimal? mto_pcmi_con_tax { get; set; }

        public decimal? mto_pcma_con_tax { get; set; }

        public decimal? mto_pcpu_sin_tax { get; set; }

        public decimal? mto_pcmi_sin_tax { get; set; }

        public decimal? mto_pcma_sin_tax { get; set; }

        public decimal? tax_por01 { get; set; }

        public decimal? tax_por02 { get; set; }

        public decimal? tax_por03 { get; set; }

        public decimal? tax_por04 { get; set; }

        public decimal? tax_por05 { get; set; }

        public decimal? tax_por06 { get; set; }

        public decimal? tax_por07 { get; set; }

        public decimal? tax_por08 { get; set; }

        public decimal tax_por_tot { get; set; }

        public decimal? tax_mto01 { get; set; }

        public decimal? tax_mto02 { get; set; }

        public decimal? tax_mto03 { get; set; }

        public decimal? tax_mto04 { get; set; }

        public decimal? tax_mto05 { get; set; }

        public decimal? tax_mto06 { get; set; }

        public decimal? tax_mto07 { get; set; }

        public decimal? tax_mto08 { get; set; }

        [StringLength(10)]
        public string peso_item { get; set; }

        [StringLength(10)]
        public string largo_item { get; set; }

        [StringLength(10)]
        public string ancho_item { get; set; }

        [StringLength(10)]
        public string altura_item { get; set; }

        [StringLength(10)]
        public string diametro_item { get; set; }

        [StringLength(300)]
        public string url_img_item { get; set; }

        [StringLength(300)]
        public string txt_referencia { get; set; }

        public int sn_incluye_tax { get; set; }

        public int? id_tipo_existencia { get; set; }

        public int? id_subfamilia { get; set; }

        public int? id_tipo_prod { get; set; }

        public int? id_clase_prod { get; set; }

        public int? id_estado { get; set; }

        [StringLength(20)]
        public string txt_estado { get; set; }

        public int? id_impuesto { get; set; }

        public long? id_um { get; set; }

        public int? id_modelo { get; set; }

        public int? id_tipo_moneda { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<INVt03_orden_compra_dtl> INVt03_orden_compra_dtl { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<INVt07_catalogo_proveedor_dtl> INVt07_catalogo_proveedor_dtl { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<INVt14_transferencia_dtl> INVt14_transferencia_dtl { get; set; }

        public virtual PROt07_tipo_prod PROt07_tipo_prod { get; set; }

        public virtual PROt02_modelo PROt02_modelo { get; set; }

        public virtual INVt10_unidad_medida INVt10_unidad_medida { get; set; }

        public virtual SNTt04_tipo_moneda SNTt04_tipo_moneda { get; set; }

        public virtual SNTt05_tipo_existencia SNTt05_tipo_existencia { get; set; }

        public virtual MSTt06_impuesto MSTt06_impuesto { get; set; }

        public virtual PROt04_subfamilia PROt04_subfamilia { get; set; }

        public virtual PROt06_clase_prod PROt06_clase_prod { get; set; }
    }
}
