namespace ConfigBusinessEntity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class INVt06_catalogo_proveedor
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public INVt06_catalogo_proveedor()
        {
            INVt07_catalogo_proveedor_dtl = new HashSet<INVt07_catalogo_proveedor_dtl>();
        }

        [Key]
        public long id_catalogo { get; set; }

        [StringLength(25)]
        public string cod_catalogo { get; set; }

        [StringLength(350)]
        public string txt_proveedor { get; set; }

        [Column(TypeName = "date")]
        public DateTime fecha_creacion { get; set; }

        public int? id_estado { get; set; }

        [StringLength(20)]
        public string txt_estado { get; set; }

        public long id_proveedor { get; set; }

        public virtual PERt03_proveedor PERt03_proveedor { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<INVt07_catalogo_proveedor_dtl> INVt07_catalogo_proveedor_dtl { get; set; }
    }
}
