﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ConfigBusinessEntity
{
    public partial class INVt13_transferencia
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public INVt13_transferencia()
        {
            INVt14_transferencia_dtl = new HashSet<INVt14_transferencia_dtl>();
            INVt15_transferencia_estado = new HashSet<INVt15_transferencia_estado>();
        }

        [Key]
        public long id_transferencia { get; set; }

        [StringLength(100)]
        public string cod_transferencia { get; set; }

        [Column(TypeName = "date")]
        public DateTime fecha_requerida { get; set; }

        public DateTime fecha_creacion { get; set; }

        [Column(TypeName = "date")]
        public DateTime? fecha_book { get; set; }

        [Column(TypeName = "date")]
        public DateTime? fecha_delivered { get; set; }

        public DateTime? fecha_modificacion { get; set; }

        [StringLength(500)]
        public string referencia { get; set; }

        [StringLength(500)]
        public string info01 { get; set; }

        [StringLength(500)]
        public string info02 { get; set; }

        [StringLength(500)]
        public string info03 { get; set; }

        [StringLength(500)]
        public string info04 { get; set; }

        [StringLength(500)]
        public string info05 { get; set; }

        [StringLength(500)]
        public string info06 { get; set; }

        [StringLength(500)]
        public string info07 { get; set; }

        [StringLength(500)]
        public string info08 { get; set; }

        [StringLength(500)]
        public string info09 { get; set; }

        [StringLength(500)]
        public string info10 { get; set; }

        public int id_tipo_moneda { get; set; }

        [Required]
        [StringLength(150)]
        public string txt_tipo_moneda { get; set; }

        public decimal? mto_tipo_cambio { get; set; }

        public decimal mto_con_tax_tot { get; set; }

        public decimal mto_sin_tax_tot { get; set; }

        public decimal tax_por_tot { get; set; }

        public decimal mto_tax_tot { get; set; }

        public decimal mto_tot_ord { get; set; }

        public int id_estado { get; set; }

        [Required]
        [StringLength(20)]
        public string txt_estado { get; set; }

        public long id_usuario { get; set; }

        [Required]
        [StringLength(50)]
        public string txt_usuario { get; set; }

        public long? id_usuario_modificador { get; set; }

        [StringLength(50)]
        public string txt_usuario_modificador { get; set; }

        public int? id_razon { get; set; }

        [StringLength(500)]
        public string txt_razon { get; set; }

        public long id_cost_center_origen { get; set; }

        public long id_cost_center_destino { get; set; }

        public virtual INVt05_cost_center INVt05_cost_center { get; set; }

        public virtual INVt05_cost_center INVt05_cost_center1 { get; set; }

        public virtual SNTt04_tipo_moneda SNTt04_tipo_moneda { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<INVt14_transferencia_dtl> INVt14_transferencia_dtl { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<INVt15_transferencia_estado> INVt15_transferencia_estado { get; set; }

        public virtual MSTt05_razon MSTt05_razon { get; set; }

        public virtual PERt01_usuario PERt01_usuario { get; set; }
    }
}
