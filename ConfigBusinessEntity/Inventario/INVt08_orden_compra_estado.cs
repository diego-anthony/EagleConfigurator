namespace ConfigBusinessEntity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class INVt08_orden_compra_estado
    {
        [Key]
        public long id_orden_compra_estado_dtl { get; set; }

        public int id_estado { get; set; }

        [Required]
        [StringLength(20)]
        public string txt_estado { get; set; }

        public DateTime fecha_registro { get; set; }

        public long id_usuario { get; set; }

        [Required]
        [StringLength(50)]
        public string txt_usuario { get; set; }

        public int? id_razon { get; set; }

        [StringLength(500)]
        public string txt_razon { get; set; }

        [StringLength(600)]
        public string txt_observ { get; set; }

        public long id_orden_compra { get; set; }

        public virtual INVt01_orden_compra INVt01_orden_compra { get; set; }
    }
}
