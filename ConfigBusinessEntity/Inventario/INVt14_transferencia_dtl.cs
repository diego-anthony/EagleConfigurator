﻿using System.ComponentModel.DataAnnotations;

namespace ConfigBusinessEntity
{
    public partial class INVt14_transferencia_dtl
    {
        [Key]
        public long id_transferencia_dtl { get; set; }

        public long id_transferencia { get; set; }

        [StringLength(250)]
        public string txt_unid_med { get; set; }

        public decimal cantidad_transferida { get; set; }

        [StringLength(250)]
        public string txt_unid_med_base { get; set; }

        public decimal? cantidad_und_base { get; set; }

        public decimal punit_sin_tax { get; set; }

        public decimal punit_con_tax { get; set; }

        public decimal? tax_por01 { get; set; }

        public decimal? tax_por02 { get; set; }

        public decimal? tax_por03 { get; set; }

        public decimal? tax_por04 { get; set; }

        public decimal? tax_por05 { get; set; }

        public decimal? tax_por06 { get; set; }

        public decimal? tax_por07 { get; set; }

        public decimal? tax_por08 { get; set; }

        public decimal? tax_mto01 { get; set; }

        public decimal? tax_mto02 { get; set; }

        public decimal? tax_mto03 { get; set; }

        public decimal? tax_mto04 { get; set; }

        public decimal? tax_mto05 { get; set; }

        public decimal? tax_mto06 { get; set; }

        public decimal? tax_mto07 { get; set; }

        public decimal? tax_mto08 { get; set; }

        public decimal? tax_por_tot { get; set; }

        public decimal? tax_mto_tot { get; set; }

        public decimal? mto_tax_total { get; set; }

        public decimal? mto_pc_sin_tax { get; set; }

        public decimal? mto_pc_con_tax { get; set; }

        public long id_item { get; set; }

        [Required]
        [StringLength(500)]
        public string txt_item { get; set; }

        public int id_estado { get; set; }

        [Required]
        [StringLength(20)]
        public string txt_estado { get; set; }

        public virtual INVt09_item INVt09_item { get; set; }

        public virtual INVt13_transferencia INVt13_transferencia { get; set; }
    }
}
