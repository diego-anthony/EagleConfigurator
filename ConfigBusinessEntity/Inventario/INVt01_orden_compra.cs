namespace ConfigBusinessEntity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class INVt01_orden_compra
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public INVt01_orden_compra()
        {
            INVt04_recepcion_dtl = new HashSet<INVt04_recepcion_dtl>();
            INVt03_orden_compra_dtl = new HashSet<INVt03_orden_compra_dtl>();
            INVt02_recepcion = new HashSet<INVt02_recepcion>();
            INVt08_orden_compra_estado = new HashSet<INVt08_orden_compra_estado>();
        }

        [Key]
        public long id_orden_compra { get; set; }

        [StringLength(100)]
        public string cod_orden_compra { get; set; }

        [StringLength(100)]
        public string nombre_template { get; set; }

        [StringLength(255)]
        public string nro_correlativo { get; set; }

        public DateTime fecha_creacion { get; set; }

        public DateTime? fecha_book { get; set; }

        public DateTime? fecha_modificacion { get; set; }

        [Column(TypeName = "date")]
        public DateTime fecha_requerida { get; set; }

        [StringLength(500)]
        public string referencia { get; set; }

        [StringLength(500)]
        public string info01 { get; set; }

        [StringLength(500)]
        public string info02 { get; set; }

        [StringLength(500)]
        public string info03 { get; set; }

        [StringLength(500)]
        public string info04 { get; set; }

        [StringLength(500)]
        public string info05 { get; set; }

        [StringLength(500)]
        public string info06 { get; set; }

        [StringLength(500)]
        public string info07 { get; set; }

        [StringLength(500)]
        public string info08 { get; set; }

        [StringLength(500)]
        public string info09 { get; set; }

        [StringLength(500)]
        public string info10 { get; set; }

        [Required]
        [StringLength(150)]
        public string txt_tipo_moneda { get; set; }

        public decimal? mto_tipo_cambio { get; set; }

        public decimal mto_con_tax_tot { get; set; }

        public decimal mto_sin_tax_tot { get; set; }

        public decimal tax_por_tot { get; set; }

        public decimal mto_tax_tot { get; set; }

        public decimal mto_tot_ord { get; set; }

        public decimal? por_dscto { get; set; }

        public decimal? mto_dscto_sin_tax_tot { get; set; }

        public decimal? mto_dscto_con_tax_tot { get; set; }

        public int id_estado { get; set; }

        [Required]
        [StringLength(20)]
        public string txt_estado { get; set; }

        [Required]
        [StringLength(50)]
        public string txt_usuario { get; set; }

        public long? id_usuario_modificador { get; set; }

        [StringLength(50)]
        public string txt_usuario_modificador { get; set; }

        public bool? sn_template { get; set; }

        public bool sn_open { get; set; }

        [StringLength(500)]
        public string txt_razon { get; set; }

        public int id_tipo_moneda { get; set; }

        public long id_usuario { get; set; }

        public long id_cost_center { get; set; }

        public long id_proveedor { get; set; }

        public int? id_razon { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<INVt04_recepcion_dtl> INVt04_recepcion_dtl { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<INVt03_orden_compra_dtl> INVt03_orden_compra_dtl { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<INVt02_recepcion> INVt02_recepcion { get; set; }

        public virtual SNTt04_tipo_moneda SNTt04_tipo_moneda { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<INVt08_orden_compra_estado> INVt08_orden_compra_estado { get; set; }


        public virtual INVt05_cost_center INVt05_cost_center { get; set; }

        public virtual PERt01_usuario PERt01_usuario { get; set; }

        public virtual PERt03_proveedor PERt03_proveedor { get; set; }
    }
}
