namespace ConfigBusinessEntity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class INVt16_recepcion_estado
    {
        [Key]
        public long id_recepcion_estado { get; set; }

        public int id_estado { get; set; }

        [Required]
        [StringLength(50)]
        public string txt_estado { get; set; }

        public DateTime fecha_registro { get; set; }

        public long id_usuario { get; set; }

        [Required]
        [StringLength(50)]
        public string txt_usuario { get; set; }

        [StringLength(600)]
        public string txt_observ { get; set; }

        public long id_recepcion { get; set; }

        public virtual INVt02_recepcion INVt02_recepcion { get; set; }
    }
}
