namespace ConfigBusinessEntity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SEGt03_pantalla_dtl
    {
        [Key]
        public int id_pantalla_dtl { get; set; }

        public bool? sn_none { get; set; }

        public bool? sn_full { get; set; }

        public bool? sn_add { get; set; }

        public bool? sn_upd { get; set; }

        public bool? sn_read { get; set; }

        public bool? sn_del { get; set; }

        public int id_pantalla { get; set; }

        public int? id_categoria_emp { get; set; }

        public virtual PERt05_categoria_emp PERt05_categoria_emp { get; set; }

        public virtual SEGt02_pantalla SEGt02_pantalla { get; set; }
    }
}
