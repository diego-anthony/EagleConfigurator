namespace ConfigBusinessEntity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SEGt01_tipo_sistema
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SEGt01_tipo_sistema()
        {
            SEGt02_pantalla = new HashSet<SEGt02_pantalla>();
            SEGt04_proceso = new HashSet<SEGt04_proceso>();
        }

        [Key]
        public int id_tipo_sistema { get; set; }

        [Required]
        [StringLength(10)]
        public string cod_tipo_sistema { get; set; }

        [StringLength(250)]
        public string desc_tipo_sistema { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SEGt02_pantalla> SEGt02_pantalla { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SEGt04_proceso> SEGt04_proceso { get; set; }
    }
}
