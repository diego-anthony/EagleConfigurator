namespace ConfigBusinessEntity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SEGt02_pantalla
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SEGt02_pantalla()
        {
            SEGt03_pantalla_dtl = new HashSet<SEGt03_pantalla_dtl>();
        }

        [Key]
        public int id_pantalla { get; set; }

        [Required]
        [StringLength(10)]
        public string cod_pantalla { get; set; }

        [StringLength(250)]
        public string desc_pantalla { get; set; }

        public int? id_tipo_sistema { get; set; }

        public virtual SEGt01_tipo_sistema SEGt01_tipo_sistema { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SEGt03_pantalla_dtl> SEGt03_pantalla_dtl { get; set; }
    }
}
