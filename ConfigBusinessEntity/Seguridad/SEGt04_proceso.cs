namespace ConfigBusinessEntity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SEGt04_proceso
    {
        [Key]
        public int id_proceso { get; set; }

        [StringLength(10)]
        public string cod_proceso { get; set; }

        [StringLength(250)]
        public string desc_proceso { get; set; }

        public int nivel_privilegio_requerido { get; set; }

        public int? id_tipo_sistema { get; set; }

        public int? id_clase_emp { get; set; }

        public virtual PERt06_clase_emp PERt06_clase_emp { get; set; }

        public virtual SEGt01_tipo_sistema SEGt01_tipo_sistema { get; set; }
    }
}
