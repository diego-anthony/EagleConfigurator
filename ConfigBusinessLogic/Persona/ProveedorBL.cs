﻿using ConfigBusinessEntity;
using ConfigBusinessLogic.Sunat;
using ConfigDataAccess.Persona;
using System.Collections.Generic;
using System.Linq;

namespace ConfigBusinessLogic.Persona
{
    public class ProveedorBL
    {
        public long InsertarProveedor(PERt03_proveedor obj)
        {
            return new ProveedorDA().InsertarProveedor(obj);
        }

        public void EliminarProveedor(long id)
        {
            new ProveedorDA().EliminarProveedor(id);
        }

        public void ActualizarProveedor(PERt03_proveedor actualizado)
        {
            new ProveedorDA().ActualizarProveedor(actualizado);
        }

        public PERt03_proveedor ProveedorXIdMM(long id)
        {
            var proveedor = new ProveedorDA().ProveedorXIdMM(id);

            if (proveedor.SNTt33_distrito != null && proveedor.SNTt33_distrito.id_dist > 0)
            {
                var provincia = new ProvinciaBL().ProvinciaXId(proveedor.SNTt33_distrito.id_prov);
                proveedor.SNTt33_distrito.SNTt32_provincia = provincia;
            }

            return proveedor;
        }

        public PERt03_proveedor ProveedorViewXId(long id)
        {
            var proveedor = new ProveedorDA().ProveedorViewXId(id);

            if (proveedor.SNTt33_distrito != null && proveedor.SNTt33_distrito.id_dist > 0)
            {
                var provincia = new ProvinciaBL().ProvinciaXId(proveedor.SNTt33_distrito.id_prov);
                proveedor.SNTt33_distrito.SNTt32_provincia = provincia;
            }

            return proveedor;
        }

        public PERt03_proveedor ProveedorXCod(string cod)
        {
            return new ProveedorDA().ProveedorXCod(cod);
        }

        public List<PERt03_proveedor> ListaProveedor(int? id_estado = null, bool ocultarBlankReg = false)
        {
            var lista = new ProveedorDA().ListaProveedor(id_estado);
            if (ocultarBlankReg && lista != null && lista.Count > 0)
            {
                lista.RemoveAll(x => x.cod_proveedor == Parameter.BlankRegister);
            }
            return lista;
        }

        public List<PERt03_proveedor> ListaProveedorExclusivo(int? id_estado = null, bool ocultarBlankReg = false)
        {
            var lista = new ProveedorDA().ListaProveedorExclusivo(id_estado);
            if (ocultarBlankReg && lista != null && lista.Count > 0)
            {
                lista.RemoveAll(x => x.cod_proveedor == Parameter.BlankRegister);
            }
            return lista;
        }

        public PERt03_proveedor ProveedorXEmail(string email)
        {
            return new ProveedorDA().ProveedorXEmail(email);
        }

        public PERt03_proveedor ProveedorXId(long id)
        {
            return new ProveedorDA().ProveedorXId(id);
        }
        //
        public IEnumerable<PERt03_proveedor> searchVendor(string codpro, string nrodoc, string nroruc, string nombreCom, int? idEstado, int exclusivo,bool ocultarBlankReg = true)
        {
            var lista = new ProveedorDA().searchVendor(codpro, nrodoc, nroruc, nombreCom, idEstado,exclusivo);
            if (ocultarBlankReg && lista != null && lista.Count() > 0)
            {
                return lista.Where(x => x.cod_proveedor != Parameter.BlankRegister);
            }
            return lista;
        }

        public bool activeVendor(long id)
        {
            return new ProveedorDA().activeVendor(id);
        }

        public bool activeCatalogoVendor(long id)
        {
            return new ProveedorDA().activeCatalogoVendor(id);
        }
    }
}
