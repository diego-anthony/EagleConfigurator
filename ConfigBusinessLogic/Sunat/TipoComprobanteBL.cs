﻿using ConfigBusinessEntity;
using ConfigDataAccess.Sunat;
using System.Collections.Generic;

namespace ConfigBusinessLogic.Sunat
{
    public class TipoComprobanteBL
    {
        public IEnumerable<SNTt10_tipo_comprobante> FindOrdCompra()
        {
            return new TipoComprobanteDA().FindOrdCompra();
        }
    }
}
