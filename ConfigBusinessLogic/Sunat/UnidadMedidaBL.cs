﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using ConfigBusinessEntity;
using ConfigDataAccess;
using ConfigUtilitarios;

namespace ConfigBusinessLogic
{
    public class UnidadMedidaBL
    {
        public List<SNTt06_unidad_medida> ListaUnidadMed(int? id_estado, bool enableTopList = false, bool ocultarBlankReg = false)
        {
            var list =  new UnidadMedidaDA().ListaUnidadMed(id_estado);

            if (ocultarBlankReg && list != null && list.Count > 0)
            {
                list.RemoveAll(x => x.cod_um == TopList.UnidadMedida);
            }

            if (enableTopList && list != null)
                return list.OrderBy(x => x.cod_um != TopList.UnidadMedida).ThenBy(x => x.txt_desc).ToList();

            return list;
        }

        public SNTt06_unidad_medida Add(SNTt06_unidad_medida model)
        {
            return new UnidadMedidaDA().Add(model);
        }

        public bool Edit(SNTt06_unidad_medida model)
        {
            return new UnidadMedidaDA().Edit(model);
        }

        public SNTt06_unidad_medida Get(long id)
        {
            return new UnidadMedidaDA().Get(id);
        }

        public SNTt06_unidad_medida GetUnidadMedidaBase(long id)
        {
            SNTt06_unidad_medida model = null;
            // corregir a long
            long? idPadre = id;
            while (idPadre != null)
            {
                model = new UnidadMedidaDA().Get((long)idPadre);
                idPadre = model.id_um_base;
            }
            return model;
        }

        public SNTt06_unidad_medida Find(Expression<Func<SNTt06_unidad_medida, bool>> expression)
        {
            return new UnidadMedidaDA().Find(expression);
        }

        public IEnumerable<SNTt06_unidad_medida> FindAll(Expression<Func<SNTt06_unidad_medida, bool>> expression)
        {
            return new UnidadMedidaDA().FindAll(expression);
        }

        public IEnumerable<SNTt06_unidad_medida> FindAllUnits(string codigo = "", string nombre = "", int? idEstado = null)
        {
            return new UnidadMedidaDA().FindAllUnits(codigo, nombre, idEstado);
        }

        public SNTt06_unidad_medida UnitByCode(string code)
        {
            return new UnidadMedidaDA().UnitByCode(code);
        }

        public bool Activar(long id)
        {
            return new UnidadMedidaDA().Activar(id);
        }

        public int FindByUndbase(long idBase)
        {
            return new UnidadMedidaDA().FindByUndbase(idBase);
        }

        public bool Eliminar(long id)
        {
            return new UnidadMedidaDA().Eliminar(id);
        }
    }
}
