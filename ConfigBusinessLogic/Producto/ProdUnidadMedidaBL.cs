﻿using ConfigBusinessEntity;
using ConfigDataAccess;
using ConfigUtilitarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace ConfigBusinessLogic
{
    public class ProdUnidadMedidaBL
    {
        public List<INVt10_unidad_medida> ListaUndMedCompra(int? id_estado, bool enableTopList = false, bool ocultarBlankReg = false)
        {
            var list = new ProdUnidadMedidaDA().ListaUnidMedCompra(id_estado);

            if (ocultarBlankReg && list != null && list.Count > 0)
            {
                list.RemoveAll(x => x.cod_um == TopList.UnidadMedida);
            }

            if (enableTopList && list != null)
                return list.OrderBy(x => x.cod_um != TopList.UnidadMedida).ThenBy(x => x.txt_desc).ToList();

            return list;
        }

        public List<INVt10_unidad_medida> ListaUndMed(int? id_estado, bool enableTopList = false, bool ocultarBlankReg = false)
        {
            var list = new ProdUnidadMedidaDA().ListaUnidadMed(id_estado);

            if (ocultarBlankReg && list != null && list.Count > 0)
            {
                list.RemoveAll(x => x.cod_um == TopList.UnidadMedida);
            }

            if (enableTopList && list != null)
                return list.OrderBy(x => x.cod_um != TopList.UnidadMedida).ThenBy(x => x.txt_desc).ToList();

            return list;
        }

        public INVt10_unidad_medida Add(INVt10_unidad_medida model)
        {
            return new ProdUnidadMedidaDA().Add(model);
        }

        public bool Edit(INVt10_unidad_medida model)
        {
            return new ProdUnidadMedidaDA().Edit(model);
        }

        public INVt10_unidad_medida Get(ulong id)
        {
            return new ProdUnidadMedidaDA().Get(id);
        }

        public INVt10_unidad_medida GetUnidadMedidaBase(ulong id)
        {
            INVt10_unidad_medida model = null;
            // corregir a long
            ulong? idPadre = id;
            while (idPadre != null)
            {
                model = new ProdUnidadMedidaDA().Get((ulong)idPadre);
                idPadre = (ulong?)model.id_um_base;
            }
            return model;
        }

        public INVt10_unidad_medida Find(Expression<Func<INVt10_unidad_medida, bool>> expression)
        {
            return new ProdUnidadMedidaDA().Find(expression);
        }

        public IEnumerable<INVt10_unidad_medida> FindAll(Expression<Func<INVt10_unidad_medida, bool>> expression)
        {
            return new ProdUnidadMedidaDA().FindAll(expression);
        }

        public IEnumerable<INVt10_unidad_medida> FindAllUnits(string codigo = "", string nombre = "", int? idEstado = null)
        {
            return new ProdUnidadMedidaDA().FindAllUnits(codigo, nombre, idEstado);
        }

        public INVt10_unidad_medida UnitByCode(string code)
        {
            return new ProdUnidadMedidaDA().UnitByCode(code);
        }

        public bool Activar(long id)
        {
            return new ProdUnidadMedidaDA().Activar(id);
        }

        public int FindByUndbase(ulong idBase)
        {
            return new ProdUnidadMedidaDA().FindByUndbase(idBase);
        }

        public bool Eliminar(ulong id)
        {
            return new ProdUnidadMedidaDA().Eliminar(id);
        }
    }
}
