﻿using ConfigBusinessEntity;
using ConfigDataAccess.Inventario;
using System;
using System.Collections.Generic;

namespace ConfigBusinessLogic.Inventario
{
    public class ParametroInventarioBL
    {
        public List<INVt12_parametro> GetAll()
        {
            return new ParametroInventarioDA().GetAll();
        }

        public INVt12_parametro GetByCode(string code)
        {
            return new ParametroInventarioDA().GetByCode(code);
        }

        public bool Update(int id, string code)
        {
            return new ParametroInventarioDA().Update(id, code);
        }

        public bool UpdateTxtVal(string txt, string code)
        {
            return new ParametroInventarioDA().UpdateTxtVal(txt, code);
        }
    }
}
