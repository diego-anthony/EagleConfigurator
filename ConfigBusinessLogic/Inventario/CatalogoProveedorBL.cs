﻿using ConfigBusinessEntity;
using ConfigDataAccess.Inventario;
using ConfigUtilitarios;
using System.Collections.Generic;
using System.Linq;

namespace ConfigBusinessLogic.Inventario
{
    public class CatalogoProveedorBL
    {
        #region Antes
        //public List<PROt18_producto_proveedor> ListaCatalogoProveedor(int? id_estado, bool enableTopList = false, bool ocultarBlankReg = false)
        //{
        //    var list = new CatalogoProveedorDA().ListaCatalogoProveedor(id_estado);

        //    if (ocultarBlankReg && list != null && list.Count > 0)
        //    {
        //        list.RemoveAll(x => x.cod_catalogo == TopList.CatalogoProveedor);
        //    }

        //    if (enableTopList && list != null)
        //        return list.OrderBy(x => x.cod_catalogo != TopList.CatalogoProveedor).ThenBy(x => x.txt_desc).ToList();

        //    return list;
        //}

        //public long Add(PROt18_producto_proveedor model)
        //{
        //    return new CatalogoProveedorDA().Add(model);
        //}

        //public bool Edit(PROt18_producto_proveedor model)
        //{
        //    return new CatalogoProveedorDA().Edit(model);
        //}

        //public bool Activar(ulong id)
        //{
        //    return new CatalogoProveedorDA().Activar(id);
        //}

        //public bool Eliminar(long id)
        //{
        //    return new CatalogoProveedorDA().Eliminar(id);
        //}

        //public PROt18_producto_proveedor CatalogoProvXCod(string cod)
        //{
        //    return new CatalogoProveedorDA().CatalogoProvXCod(cod);
        //}

        //public PROt18_producto_proveedor Get(ulong id)
        //{
        //    return new CatalogoProveedorDA().Get(id);
        //}
        #endregion

        public List<INVt06_catalogo_proveedor> ListaCatalogoProveedor(int? id_estado = null)
        {
            return new CatalogoProveedorDA().ListaCatalogoProveedor(id_estado);
        }
        public List<PERt03_proveedor> ListaProveedorCatActivo(int? id_estado = null)
        {
            return new CatalogoProveedorDA().ListaProveedorCatActivo(id_estado);
        }
        public long InsertarCatalogoProveedor(INVt06_catalogo_proveedor obj)
        {
            return new CatalogoProveedorDA().InsertarCatalogoProveedor(obj);
        }
        public void EliminarCatalogoProveedor(long id)
        {
            new CatalogoProveedorDA().EliminarCatalogoProveedor(id);
        }
        public bool ActivarCatalogoProveedor(long id)
        {
            return new CatalogoProveedorDA().ActivarCatalogoProveedor(id);
        }
        public bool ActualizarCatalogo(INVt06_catalogo_proveedor actualizado)
        {
            return new CatalogoProveedorDA().ActualizarCatalogo(actualizado);
        }
        public INVt06_catalogo_proveedor CatalogoXId(long id)
        {
            return new CatalogoProveedorDA().CatalogoXId(id);
        }
        public INVt06_catalogo_proveedor CatalogoXCod(string cod)
        {
            return new CatalogoProveedorDA().CatalogoXCod(cod);
        }

        public IEnumerable<INVt06_catalogo_proveedor> BuscarCatalogo(string cod, string descripcion, int? id_estado)
        {
            return new CatalogoProveedorDA().BuscarCatalogo(cod, descripcion, id_estado);
        }
        public string ObtenerIdCatalogo(long idProv )
        {
            
            return new CatalogoProveedorDA().ObtenerIdCatalogo(idProv);
            
        }
    }
}
