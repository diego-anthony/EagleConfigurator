﻿using ConfigBusinessEntity;
using ConfigDataAccess.Inventario;
using System.Collections.Generic;

namespace ConfigBusinessLogic.Inventario
{
    public class CostCenterBL
    {
        public int InsertarCostCenter(INVt05_cost_center obj)
        {
            return new CostCenterDA().InsertarCostCenter(obj);
        }

        public void EliminarCostCenter(int id)
        {
            new CostCenterDA().EliminarCostCenter(id);
        }

        public void ActualizarCostCenter(INVt05_cost_center obj)
        {
            new CostCenterDA().ActualizarCostCenter(obj);
        }

        public INVt05_cost_center CostCenterXId(int id)
        {
            return new CostCenterDA().CostCenterXId(id);
        }

        public INVt05_cost_center CostCenterXCod(string cod)
        {
            return new CostCenterDA().CostCenterXCod(cod);
        }

        public List<INVt05_cost_center> ListaCostCenter(int? id_estado = null, bool ocultarBlankReg = false)
        {
            var lista = new CostCenterDA().ListaCostCenter(id_estado);
            if (ocultarBlankReg && lista != null && lista.Count > 0)
            {
                lista.RemoveAll(x => x.cod_cost_center == Parameter.BlankRegister);
            }

            return lista;
        }
    }
}
