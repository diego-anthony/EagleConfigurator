﻿using ConfigBusinessEntity;
using ConfigDataAccess.Inventario;
using System.Collections.Generic;

namespace ConfigBusinessLogic.Inventario
{
    public class InvControlNumeracionBL
    {
        public IEnumerable<INVt11_control_numeracion> GetAll()
        {
            return new InvControlNumeracionDA().GetAll();
        }

        public bool Add(IEnumerable<INVt11_control_numeracion> list)
        {
            return new InvControlNumeracionDA().Add(list);
        }

        public bool SearchDocumentExist(int idTipoComprobante)
        {
            return new InvControlNumeracionDA().SearchDocumentExist(idTipoComprobante);
        }
    }
}
