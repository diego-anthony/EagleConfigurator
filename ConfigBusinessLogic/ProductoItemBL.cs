﻿using ConfigBusinessEntity;
using ConfigDataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigBusinessLogic
{
    public class ProductoItemBL
    {
        public long InsertarProducto(INVt09_item obj)
        {
            return new ProductoItemDA().InsertarProducto(obj);
        }

        public void EliminarProducto(long id)
        {
            new ProductoItemDA().EliminarProducto(id);
        }

        public bool ActivarItem(long id)
        {
            return new ProductoItemDA().ActivarItem(id);
        }

        public bool ActualizarProducto(INVt09_item obj)
        {
            return new ProductoItemDA().ActualizarProducto(obj);
        }

        public List<INVt09_item> ListaProducto(int? id_estado = null, bool ocultarBlankReg = false)
        {
            var lista = new ProductoItemDA().ListaProducto(id_estado);
            if (ocultarBlankReg && lista != null && lista.Count > 0)
            {
                lista.RemoveAll(x => (x.cod_item == Parameter.BlankRegister) || (x.cod_item2 == Parameter.BlankRegister));
            }
            return lista;
        }

        public INVt09_item ProductoXId(long id)
        {
            return new ProductoItemDA().ProductoXId(id);
        }

        public INVt09_item ProductoXIdMM(long id)
        {
            return new ProductoItemDA().ProductoXIdMM(id);
        }

        public INVt09_item ProductoViewXId(long id)
        {
            return new ProductoItemDA().ProductoViewXId(id);
        }

        public INVt09_item ProductoXCod(string cod)
        {
            return new ProductoItemDA().ProductoXCod(cod);
        }

        public INVt09_item ProductoXCod2(string cod)
        {
            return new ProductoItemDA().ProductoXCod2(cod);
        }

        public INVt09_item ProductoXCodBarra(string cod)
        {
            return new ProductoItemDA().ProductoXCodBarra(cod);
        }

        public List<INVt09_item> ListaProductoXNom(string nombre, int? id_estado = null)
        {
            return new ProductoItemDA().ListaProductoXNom(nombre, id_estado);
        }

        public List<INVt09_item> ListaProductoXMod(int id_modelo, int? id_estado = null)
        {
            return new ProductoItemDA().ListaProductoXMod(id_modelo, id_estado);
        }

        public IEnumerable<INVt09_item> BuscarItem(string cod, string cod02, string nombre,int? idModelo, int? idTipoExist,int? idSubFamilia,int? idTipoProd,int?idClase, int? idEstado, bool ocultarBlankReg = true)
        {
            var lista = new ProductoItemDA().BuscarItem(cod, cod02, nombre, idModelo, idTipoExist, idSubFamilia, idTipoProd, idClase, idEstado);
            if (ocultarBlankReg && lista != null && lista.Count() > 0)
            {
                return lista.Where(x => x.cod_item != Parameter.BlankRegister);
            }
            return lista;
            
        }

    }
}
