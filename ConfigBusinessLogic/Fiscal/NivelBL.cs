﻿using ConfigBusinessEntity;
using ConfigDataAccess.Fiscal;
using System.Collections.Generic;

namespace ConfigBusinessLogic.Fiscal
{
    public class NivelBL
    {
        public IEnumerable<FISt02_nivel> GetAll()
        {
            return new NivelDA().GetAll();
        }
    }
}
