﻿using System.Windows.Forms;

namespace ConfigUtilitarios.Extensions
{
    public static class DgvExt
    {
        public static DataGridView DefColumn(this DataGridView dgv, string columnName, string headerText)
        {
            dgv.Columns[columnName].HeaderText = headerText;
            dgv.Columns[columnName].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgv.Columns[columnName].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            return dgv;
        }

        public static DataGridView SetWidth(this DataGridView dgv, string columnName, int width)
        {
            dgv.Columns[columnName].Width = width;
            return dgv;
        }

        public static DataGridView HideColumns(this DataGridView dgv, params string[] columnsName)
        {
            foreach (var columnName in columnsName)
            {
                dgv.Columns[columnName].Visible = false;
            }
            return dgv;
        }

        public static decimal GetDecimal(this DataGridViewRow dgvRow, string columnName)
        {
            return decimal.Parse(dgvRow.Cells[columnName].Value.ToString());
        }

        public static object GetObject(this DataGridViewRow dgv, string columnName)
        {
            return dgv.Cells[columnName].Value;
        }

        public static string GetString(this DataGridViewRow dgvRow, string columnName)
        {
            return dgvRow.Cells[columnName].Value.ToString();
        }

        public static long GetLong(this DataGridView dgv, string columnName, int i)
        {
            return long.Parse(dgv.Rows[i].Cells[columnName].Value.ToString());
        }

        public static string GetString(this DataGridView dgv, string columnName, int i)
        {
            return dgv.Rows[i].Cells[columnName].Value.ToString();
        }

        public static object GetObject(this DataGridView dgv, string columnName, int i)
        {
            return dgv.Rows[i].Cells[columnName].Value;
        }

        public static void AddColumn(this DataGridView dgv, DataGridViewColumn dataGridViewColumn, string columnName, int index = 0)
        {
            dataGridViewColumn.HeaderText = columnName;
            dataGridViewColumn.Name = columnName;

            dgv.Columns.Add(dataGridViewColumn);
            dgv.Columns[columnName].DisplayIndex = index;
        }

        public static int GetIndex(this DataGridView dgv, string columnName)
        {
            return dgv.Columns[columnName].Index;
        }
    }
}
