﻿using System;

namespace ConfigUtilitarios.ViewModels
{
    public class InvNumeracionVM
    {
        public long Id { get; set; }

        public string NroSerie { get; set; }

        public long? NroInicial { get; set; }

        public long? NroFinal { get; set; }

        public long NroActual { get; set; }

        public DateTime FechaRegistro { get; set; }

        public string Nivel { get; set; }

        public string TipoComp { get; set; }

        public int IdNivel { get; set; }

        public int IdTipoComp { get; set; }
    }
}
